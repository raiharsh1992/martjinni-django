# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.http import JsonResponse
import logging
from layer2 import calculatedistance,generateAddList,insertAddress,insertingPocketInfo,generatePocketList,generateKeyWords,generateShopData,calculateTrending,generateDeliveryResponse
from login.layer2 import validateKey,vlaidatesessiontype,getAdmId

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/address.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

class validAddress():
    lattitude = ""
    longitude = ""
    def __init__(self, lattitude,longitude):
        self.lattitude = lattitude
        self.longitude = longitude

class insertOfAddress():
    addLine1 = ""
    addLine2 = ""
    city = ""
    state = ""
    country = ""
    pincode = ""
    dmLat = ""
    dmLng = ""
    addName = ""
    uId = ""
    def __init__(self, addLine1, addLine2, city, state, country, pincode, dmLat, dmLng, addName, uId):
        self.addLine1 = addLine1
        self.addLine2 = addLine2
        self.city = city
        self.state = state
        self.country = country
        self.pincode = pincode
        self.dmLat = dmLat
        self.dmLng = dmLng
        self.addName = addName
        self.uId = uId

def calculateDistanceExtractor(request):
    try:
        print request.body
        string = json.loads(request.body)
        lattitude = string['lattitude']
        longitude = string['longitude']
        isValidAddress = validAddress(lattitude,longitude)
        response = calculatedistance(isValidAddress)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    return response

def calculateTrendingExtractor(request):
    try:
        print request.body
        string = json.loads(request.body)
        lattitude = string['lattitude']
        longitude = string['longitude']
        isValidAddress = validAddress(lattitude,longitude)
        response = calculateTrending(isValidAddress)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    return response

def getAddList(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if (uId==0):
            rData = json
            rData = {"Data":"Incorrect session value"}
            response = JsonResponse(rData)
            response.status_code=401
        else:
            iData = json.loads(request.body)
            userId = iData['userId']
            if(uId == userId):
                userType = vlaidatesessiontype(uId)
                if (userType=="CUST"):
                    response = generateAddList(uId)
                else:
                    rData = json
                    rData = {"Data":"Not authorized"}
                    response = JsonResponse(rData)
                    response.status_code = 401
            else:
                rData = json
                rData = {"Data":"Not authorized"}
                response = JsonResponse(rData)
                response.status_code = 401

    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except:
        x = json
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def validateInsertAddress(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if (uId==0):
            rData = json
            rData = {"Data":"Incorrect session value"}
            response = JsonResponse(rData)
            response.status_code=401
        else:
            iData = json.loads(request.body)
            userId = iData['userId']
            if(uId == userId):
                userType = vlaidatesessiontype(uId)
                if (userType=="CUST"):
                    addLine1 = iData['addLine1']
                    addLine2 = iData['addLine2']
                    city = iData['city']
                    state = iData['state']
                    country = iData['country']
                    pincode = iData['pincode']
                    dmLat = iData['lat']
                    dmLng = iData['lng']
                    addName = iData['addName']
                    addressObject = insertOfAddress(addLine1, addLine2, city, state, country, pincode, dmLat, dmLng, addName, uId)
                    response = insertAddress(addressObject)
                    logger.info(response)
                else:
                    rData = json
                    rData = {"Data":"Not authorized"}
                    response = JsonResponse(rData)
                    response.status_code = 401
            else:
                rData = json
                rData = {"Data":"Not authorized"}
                response = JsonResponse(rData)
                response.status_code = 401

    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except UnboundLocalError:
        x = json
        logger.exception(UnboundLocalError)
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def insertPocketDetails(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if (uId==0):
            rData = json
            rData = {"Data":"Incorrect session value"}
            response = JsonResponse(rData)
            response.status_code=401
        else:
            iData = json.loads(request.body)
            userId = iData['userId']
            if(uId == userId):
                userType = vlaidatesessiontype(uId)
                if ((userType=="ADM") and (userType==iData['userType'])):
                    admId = getAdmId(userId)
                    if(admId==iData['typeId']):
                        pocketName = iData['pocketName']
                        pocketCity = iData['pocketCity']
                        pocketState = iData['pocketState']
                        pocketCountry = iData['pocketCountry']
                        pocketPin = iData['pocketPin']
                        pocketKeywords = iData['pocketKey']
                        pocketLat = iData['pocketLat']
                        pocketLng = iData['pocketLng']
                        radius = iData['radius']
                        insertPocketInfo = insertingPocketInfo(pocketName, pocketCity, pocketState, pocketCountry, pocketPin, pocketLat, pocketLng, pocketKeywords, radius)
                        if(insertPocketInfo==1):
                            x = json
                            x = {"Data":"Pocket created"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while creating pocket"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect typeId passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    rData = json
                    rData = {"Data":"Not authorized"}
                    response = JsonResponse(rData)
                    response.status_code = 401
            else:
                rData = json
                rData = {"Data":"Not authorized"}
                response = JsonResponse(rData)
                response.status_code = 401

    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except UnboundLocalError:
        x = json
        logger.exception(UnboundLocalError)
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def viewPocket1(request):
    try:
        response = generatePocketList()
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except UnboundLocalError:
        x = json
        logger.exception(UnboundLocalError)
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def viewQueryResult(request):
    try:
        response = generateKeyWords()
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except UnboundLocalError:
        x = json
        logger.exception(UnboundLocalError)
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def getShopInfoLayer(request):
    try:
        rString = json.loads(request.body)
        shopList = rString['shops']
        response = generateShopData(shopList)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except UnboundLocalError:
        x = json
        logger.exception(UnboundLocalError)
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def validatedeliverypoint(request):
    try:
        if(request.body):
            rString = json.loads(request.body)
            lat = rString['lat']
            lng = rString['lng']
            shopList = rString['shopList']
            response = generateDeliveryResponse(lat,lng,shopList)
        else:
            x = json
            x = {"Data":"No body"}
            response = JsonResponse(x)
            response.status_code = 404
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
    except UnboundLocalError:
        x = json
        logger.exception(UnboundLocalError)
        x = {"Data":"Kuch to locha hai daya"}
        response = JsonResponse(x)
        response.status_code = 409
    return response
