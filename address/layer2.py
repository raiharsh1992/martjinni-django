from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.http import JsonResponse
from django.db import connection
from haversine import haversine
from decimal import Decimal
from datetime import datetime
from login.layer2 import checkShopProperty,evaluatePropertyShop

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/address.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def generateDeliveryResponse(lat,lng,shopList):
    with connection.cursor() as cursor:
        result = 0
        x = json
        for each in shopList:
            cursor.execute("SELECT * from login_shopmaster where shopId=%s",[each])
            row = cursor.fetchone()
            uPassed = (lat,lng)
            centerPoint = (row[4], row[5])
            x = haversine(centerPoint,uPassed)
            if (x<=row[10]):
                result = 0
            else:
                result = 1
                break
        if(result==0):
            x = {"Data":"Address deliverable"}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = {"Data":"Address not deliverable"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def calculatedistance(self):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_shopmaster where isActiveFl=1 and isVerified=1 order by rating desc")
        row = cursor.fetchone()
        uPassed = (self.lattitude, self.longitude)
        shopListOpen = []
        serviceListOpen = []
        subsListOpen = []
        shopListClose = []
        serviceListClose = []
        subsListClose = []
        shopsCat = ""
        serviceCat = ""
        subsCat = ""
        serviceCount = 0
        shopCount = 0
        subsCount = 0
        while row is not None:
            centerPoint = (row[4], row[5])
            x = haversine(centerPoint,uPassed)
            if (x<=row[10]):
                if(row[9]==2):
                    if(row[3]==1):
                        serviceListOpen.append(row[0])
                    elif(row[3]==0):
                        serviceListClose.append(row[0])
                    serviceCat = serviceCat+","+row[16]
                    serviceCount = serviceCount+1
                else:
                    isSubs = checkShopProperty(2, row[0])
                    if(isSubs==1):
                        if(row[3]==1):
                            subsListOpen.append(row[0])
                            shopListOpen.append(row[0])
                        elif(row[3]==0):
                            subsListClose.append(row[0])
                            shopListClose.append(row[0])
                        subsCat = subsCat+","+row[16]
                        subsCount = subsCount+1
                        shopsCat = shopsCat+","+row[16]
                        shopCount = shopCount+1
                    else:
                        if(row[3]==1):
                            shopListOpen.append(row[0])
                        elif(row[3]==0):
                            shopListClose.append(row[0])
                        shopsCat = shopsCat+","+row[16]
                        shopCount = shopCount+1
            row = cursor.fetchone()
        container = {}
        cateogaryInfoService = getCateoInfo(serviceCat, serviceListOpen)
        serviceData = {
            "count" : serviceCount,
            "filterData" : cateogaryInfoService,
            "shopList" : {
                "open":serviceListOpen,
                "close":serviceListClose
            }
        }
        cateogaryInfoShop = getCateoInfo(shopsCat, shopListOpen)
        shopData = {
            "count" : shopCount,
            "filterData" : cateogaryInfoShop,
            "shopList" : {
                "open":shopListOpen,
                "close":shopListClose
            }
        }
        cateogaryInfoSubs = getCateoInfo(subsCat, subsListOpen)
        subsData = {
            "count" : subsCount,
            "filterData" : cateogaryInfoSubs,
            "shopList" : {
                "open" : subsListOpen,
                "close" : subsListClose
            }
        }
        container['tabs'] = ["shops","service","subs"]
        container['shops']=shopData
        container['service']=serviceData
        container['subs'] = subsData
        response=JsonResponse(container)
        response.status_code=200
        return response

def calculateTrending(self):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_shopmaster where isActiveFl=1 and isVerified=1 order by totalOrders desc")
        row = cursor.fetchone()
        uPassed = (self.lattitude, self.longitude)
        shopListOpen = []
        serviceListOpen = []
        subsListOpen = []
        shopListClose = []
        serviceListClose = []
        subsListClose = []
        shopsCat = ""
        serviceCat = ""
        subsCat = ""
        serviceCount = 0
        shopCount = 0
        subsCount = 0
        totalCount = 0
        openCount = 0
        while row is not None:
            centerPoint = (row[4], row[5])
            x = haversine(centerPoint,uPassed)
            if (x<=row[10]):
                if(row[9]==2):
                    if(row[3]==1):
                        serviceListOpen.append(row[0])
                        openCount = openCount + 1
                    elif(row[3]==0):
                        serviceListClose.append(row[0])
                    serviceCount = serviceCount+1
                    serviceCat = serviceCat+","+row[16]
                else:
                    isSubs = checkShopProperty(2, row[0])
                    if(isSubs==1):
                        if(row[3]==1):
                            subsListOpen.append(row[0])
                            shopListOpen.append(row[0])
                            openCount = openCount + 1
                        elif(row[3]==0):
                            subsListClose.append(row[0])
                            shopListClose.append(row[0])
                        subsCount = subsCount+1
                        shopCount = shopCount+1
                        subsCat = subsCat+","+row[16]
                        shopsCat = shopsCat+","+row[16]
                    else:
                        if(row[3]==1):
                            shopListOpen.append(row[0])
                            openCount = openCount + 1
                        elif(row[3]==0):
                            shopListClose.append(row[0])
                        shopCount = shopCount+1
                        shopsCat = shopsCat+","+row[16]
                totalCount = totalCount + 1
            row = cursor.fetchone()
        container = {}
        cateogaryInfoService = getCateoInfo(serviceCat, serviceListOpen)
        serviceData = {
            "count" : serviceCount,
            "filterData" : cateogaryInfoService,
            "shopList" : {
                "open":serviceListOpen,
                "close":serviceListClose
            }
        }
        cateogaryInfoShop = getCateoInfo(shopsCat, shopListOpen)
        shopData = {
            "count" : shopCount,
            "filterData" : cateogaryInfoShop,
            "shopList" : {
                "open":shopListOpen,
                "close":shopListClose
            }
        }
        cateogaryInfoSubs = getCateoInfo(subsCat, subsListOpen)
        subsData = {
            "count" : subsCount,
            "filterData" : cateogaryInfoSubs,
            "shopList" : {
                "open" : subsListOpen,
                "close" : subsListClose
            }
        }
        container['tabs'] = ["shops","service","subs"]
        container['shops']=shopData
        container['service']=serviceData
        container['subs'] = subsData
        container['totalCount'] = totalCount
        container['openCount'] = openCount
        response=JsonResponse(container)
        response.status_code=200
        return response

def isShopCat(catId, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT catId from login_shopmaster where shopId=%s",[shopId])
        if(row_count>0):
            row = cursor.fetchone()
            catIdUse = row[0].split(",")
            for each in catIdUse:
                each = str(each)
                catId = str(catId)
                if(each==catId):
                    return 1
        else:
            return 0

def getCateoInfo(catId, shopList):
    if catId:
        catList = list(set(catId.split(',')))
        logger.exception(catList)
        typeCat = []
        typeSubCat = []
        mjInfo = []
        mj = ""
        for x in catList:
            with connection.cursor() as cursor:
                row_coun = cursor.execute("SELECT * from login_cateogaryshops where catId=%s",[x])
                if(row_coun==1):
                    row = cursor.fetchone()
                    shopUseList = []
                    for each in shopList:
                        isPart = isShopCat(row[0], each)
                        if(isPart==1):
                            shopUseList.append(each)
                    typeCat.append({
                        "catName":row[1],
                        "catId":row[0],
                        "catMJ":row[3],
                        "shopList":shopUseList
                    })
                    mj = mj+","+str(row[3])
        mjList = list(set(mj))
        for x in mjList:
            with connection.cursor() as cursor:
                row_coun = cursor.execute("SELECT * from login_mjavailable where mjId=%s",[x])
                if(row_coun==1):
                    row = cursor.fetchone()
                    mjInfo.append({
                        "mjName":row[1],
                        "mjId":row[0]
                    })
        container = {}
        container['mjAvailable'] = mjInfo
        container['catAvailable'] = typeCat
        container['Data'] = 1
        return container
    else:
        return({"Data":0})

def generateAddList(uId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from address_addressmaster where isActiveFl=1 and uId_id=%s",[uId])
        row = cursor.fetchone()
        addressList = []
        count = 0
        while row is not None:
            addressList.append({
                "addressId":row[1],
                "addressLine1":row[2],
                "addressLine2":row[3],
                "city":row[4],
                "state":row[5],
                "country":row[6],
                "pincode":row[7],
                "addLat":row[8],
                "addLng":row[9],
                "addName":row[0]
            })
            count = count + 1
            row = cursor.fetchone()
        container = {}
        container['addressCount']=count
        container['address']=addressList
        response=JsonResponse(container)
        response.status_code=200
        return response

def generateAddListNew(aDid):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from address_addressmaster where addId=%s",[aDid])
        row = cursor.fetchone()
        container = {'addressId':row[1],'addressLine1':row[2],'addressLine2':row[3],'city':row[4],'state':row[5],'country':row[6],'pincode':row[7],'addLat':row[8],'addLng':row[9],'addName':row[0]}
        response=JsonResponse(container)
        response.status_code=200
        return response

def insertAddress(self):
    with connection.cursor() as cursor:
        try:
            cursor.execute("SELECT count(*) from address_addressmaster;")
            row = cursor.fetchone()
            aDid = row[0]+1
            createdOnDate =  datetime.now()
            logger.info(aDid)
            cursor.execute("INSERT INTO address_addressmaster (addId, addLine1, addLine2, city, state, country, pincode, dmLat, dmLng, createdOn, isActiveFl, uId_id, addName) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '1' , %s, %s);",[aDid,self.addLine1,self.addLine2,self.city,self.state,self.country,self.pincode,self.dmLat,self.dmLng,createdOnDate,self.uId,self.addName])
            connection.commit()
            response = generateAddListNew(aDid)
        except:
            connection.rollback()
            logger.exception("DEKH")
            rData = {"Data":"Failure",}
            response = JsonResponse(rData)
            response.status_code = 400
        return response

def insertingPocketInfo(pocketName, pocketCity, pocketState, pocketCountry, pocketPin, pocketLat, pocketLng, pocketKeywords, radius):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO address_pocketmaster (pocketName, keywords, city, state, country, pincode, lat, lng, isActiveFl, radius ) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",[pocketName, pocketKeywords, pocketCity, pocketState, pocketCountry, pocketPin, pocketLat, pocketLng, 1, radius])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def generatePocketList():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from address_pocketmaster")
        if row_count>0 :
            container = []
            count = 0
            row = cursor.fetchone()
            while row is not None:
                count = count + 1
                container.append({
                    "pocketId":row[1],
                    "pocketName":row[0],
                    "city":row[3],
                    "state":row[4],
                    "country":row[5],
                    "pincode":row[6],
                    "lat":row[7],
                    "lng":row[8],
                    "keywords":row[2],
                    "isActiveFl":row[9]
                })
                row = cursor.fetchone()
            myObj = {}
            myObj['Data']=container
            myObj['totalCount']=count
            response = JsonResponse(myObj)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[], "totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def generateKeyWords():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * FROM address_querymaster")
        if row_count>0:
            row=cursor.fetchone()
            container=[]
            count=0
            while row is not None:
                container.append(row[1])
                count=count+1
                row=cursor.fetchone()
            x=json
            x={"totalCount":count, "Data":container}
            response=JsonResponse(x)
            response.status_code=200
        else:
            x=json
            x={"totalCount":0,"Data":[]}
            response=JsonResponse(x)
            response.status_code=200
    return response

def generateShopData(shopList):
    if len(shopList)<=25:
        shopData = []
        count = 0
        shopList = list(set(shopList))
        with connection.cursor() as cursor:
            for each in shopList:
                row_count=cursor.execute("SELECT * from login_shopmaster where shopId=%s",[each])
                if(row_count==1):
                    row = cursor.fetchone()
                    viewType = ""
                    if(row[9]==2):
                        viewType="service"
                    else:
                        viewType="shop"
                    shopPhoneNumber = 0
                    if(evaluatePropertyShop("showNumber",each)>0):
                        shopPhoneNumber = row[2]
                    shopData.append({
                        "shopName":row[1],
                        "shopPhoneNumber":shopPhoneNumber,
                        "shopId":row[0],
                        "isOpen":row[3],
                        "shopDesc":row[6],
                        "minOrderValue":row[14],
                        "delCharges":row[13],
                        "catId":row[16],
                        "imageUploaded":row[17],
                        "subCatId":row[19],
                        "address":row[12],
                        "rating":row[21],
                        "viewType":viewType
                    })
                    count = count + 1
        x = {
            "Data":shopData,
            "count":count
        }
        response = JsonResponse(x)
        response.status_code = 200
    else:
        x = json
        x = {"Data":"Lenght Exceeded","count":0}
        response = JsonResponse(x)
        response.status_code = 409
    return response
