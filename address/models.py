# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from login.models import loginMaster
from django.db import models

# Create your models here.
class addressMaster(models.Model):
    """docstring for sessionMaster."""
    uId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    addName = models.CharField(blank=True, max_length=32)
    addId = models.AutoField(max_length=128, primary_key=True)
    addLine1 = models.CharField(max_length=128, blank=False)
    addLine2 = models.CharField(max_length=128, blank=False)
    city = models.CharField(max_length=64, blank=False)
    state = models.CharField(max_length=32, blank=False)
    country = models.CharField(max_length=32, blank=False)
    pincode = models.IntegerField(blank=False)
    dmLat = models.FloatField(blank=False)
    dmLng = models.FloatField(blank=False)
    createdOn = models.DateTimeField(auto_now_add=False)
    isActiveFl = models.BooleanField(blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

# Create your models here.
class PocketMaster(models.Model):
    """docstring for sessionMaster."""
    pocketName = models.CharField(blank=True, max_length=32)
    pocketId = models.AutoField(max_length=32, primary_key=True)
    keywords = models.CharField(max_length=200, blank=True)
    city = models.CharField(max_length=20, blank=False)
    state = models.CharField(max_length=15, blank=False)
    country = models.CharField(max_length=20, blank=False)
    pincode = models.IntegerField(blank=False)
    lat = models.FloatField(blank=False)
    lng = models.FloatField(blank=False)
    isActiveFl = models.BooleanField(blank=False)
    radius = models.IntegerField(blank=False, default=1)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class queryMaster(models.Model):
    """docstring for sessionMaster."""
    locationName = models.CharField(max_length=50, blank=False, unique=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg
