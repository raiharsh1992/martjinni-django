# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import json
from django.http import JsonResponse
import logging
from layer1 import calculateDistanceExtractor,getAddList,validateInsertAddress,insertPocketDetails,viewPocket1,viewQueryResult,getShopInfoLayer,calculateTrendingExtractor,validatedeliverypoint
# Create your views here.

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/address.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def shoplist(request):
    try:
        if (request.method=="POST"):
            if (request.body):
                response = calculateDistanceExtractor(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def trendlist(request):
    try:
        if (request.method=="POST"):
            if (request.body):
                response = calculateTrendingExtractor(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def custaddresslist(request):
    try:
        if (request.method=="POST"):
            if (request.body and request.META['HTTP_BASICAUTHENTICATE']):
                response = getAddList(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def addaddress(request):
    try:
        if (request.method=="POST"):
            if (request.body and request.META['HTTP_BASICAUTHENTICATE']):
                response = validateInsertAddress(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def addpocket(request):
    try:
        if (request.method=="POST"):
            if (request.body and request.META['HTTP_BASICAUTHENTICATE']):
                response = insertPocketDetails(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def viewpocket(request):
    try:
        if (request.method=="POST"):
            response = viewPocket1(request)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def keywordsearch(request):
    try:
        if (request.method=="POST"):
            response = viewQueryResult(request)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def getshopinfo(request):
    try:
        if (request.method=="POST"):
            response = getShopInfoLayer(request)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def validatedelivery(request):
    try:
        if (request.method=="POST"):
            response = validatedeliverypoint(request)
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response
