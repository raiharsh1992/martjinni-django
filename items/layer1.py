# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import json
from django.http import JsonResponse
import logging
from layer2 import returnItemList,insertMainItem,validateShopMainItem,insertSubItem,validateSubItem,updateMItem,updateSubItem,validateGstId,returnGstTable,insertValidGst,updateValidGst,validateGstName,validateCatId,viewItemsCateogary,viewShopsCateogary,insertShopCat,insertItemCat,validateShopCatId,insertMjAvailable,creatingNewAddOn,checkIfExistAddOn,updatingAddOn,checkIfExistSub,insertSub,doUpdateSubs,checkIfAddOnItemExist,insertFinalItemAddOn,validateAddOnId,updateFinalItemAddOn,createAddOnListShop,createSubsListShop,createShopItemCat,validateItemCat,updateShopmaste,isValidGstId,isValidAddOnInfo,isValidSubsInfo,validateSubItemShop,insertquantityinfo,validateQuantityInsertion,validateQuantEntry,updateQuantInfo,validateUpload,saveItemImage,validateTimePropInsert,checkIfPresentTime,createTimeInfo,validateIfUpdatePossible,updateTimeInformation,getMjListAdmin
from login.layer2 import validateKey,vlaidatesessiontype,getShpId,getAdmId,getCustId,evaluatePropertyShop

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/address.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def fetchItem(request):
    try:
        rString = json.loads(request.body)
        shopId = rString['shopId']
        if shopId:
            response = returnItemList(shopId)
        else:
            x = json
            x = {"Data":"Not all variable values passed"}
            response = JsonResponse(x)
            response.status_code = 404
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def addItemMainLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    mainItemName = rString['mItemName']
                    itemCatId = rString['itemCatId']
                    isValidCatId = validateItemCat(shpId,itemCatId)
                    if(isValidCatId==1):
                        itemAdded = insertMainItem(mainItemName, rString['typeId'], itemCatId)
                        if(itemAdded==1):
                            x = json
                            x = {"Data":"Item added successfuly"}
                            updateShopmaste(itemCatId, rString['typeId'])
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while inserting items"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect catId selected"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect user Id"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def viewCateogaryList(request):
    try:
        rString = json.loads(request.body)
        viewType = rString['viewType']
        if(viewType=="items"):
            response = viewItemsCateogary()
        elif(viewType=="shop"):
            response = viewShopsCateogary()
        else:
            x = json
            x = {"Data":"Incorrect view type selected"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def addItemSubLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                print(uId)
                if(shpId==rString['typeId']):
                    mItemId = rString['mItemId']
                    validMItem = validateShopMainItem(mItemId, shpId)
                    if(validMItem==1):
                        subItemName = rString['subItemName']
                        itemDesc = rString['itemDesc']
                        if(rString['gstId']):
                            usesAddon = evaluatePropertyShop("useGst",shpId)
                            if(usesAddon==0):
                                gstId = 1
                            else:
                                gstIdDummy = rString['gstId']
                                validGst=isValidGstId(gstIdDummy)
                                if(validGst==1):
                                    gstId=gstIdDummy
                                else:
                                    gstId=1
                        else:
                            gstId = 1
                        if(rString['addOnInfo']):
                            usesAddon = evaluatePropertyShop("useAddon",shpId)
                            if(usesAddon==0):
                                addOnInfo = ""
                            else:
                                addOnInfoDummy = rString['addOnInfo']
                                validAddOn = isValidAddOnInfo(addOnInfoDummy,shpId)
                                if(validAddOn==1):
                                    y = addOnInfoDummy
                                    addOnInfo = ",".join(map(str, y))
                                else:
                                    addOnInfo = ""
                        else:
                            addOnInfo=""
                        if(rString['subsInfo']):
                            usesAddon = evaluatePropertyShop("subscription",shpId)
                            if(usesAddon==0):
                                susbInfo = ""
                            else:
                                subsInfoDummy = rString['subsInfo']
                                validAddOn = isValidSubsInfo(subsInfoDummy,shpId)
                                if(validAddOn==1):
                                    x = subsInfoDummy
                                    subsInfo = ",".join(map(str, x))
                                else:
                                    subsInfo = ""
                        else:
                            subsInfo=""
                        if(rString['spclCat']):
                            if((rString['spclCat'])=="none")or((rString['spclCat'])=="veg")or((rString['spclCat'])=="nonveg"):
                                spclCat = rString['spclCat']
                            else:
                                spclCat = "none"
                        else:
                            spclCat = "none"
                        subItemInserted = insertSubItem(subItemName, gstId, mItemId, shpId, addOnInfo, subsInfo, itemDesc, spclCat)
                        if(subItemInserted==1):
                            x = json
                            x = {"Data":"Item inserted successfuly"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while inserting item"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Main item Id doesn't belong to the store"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect user Id"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def updateItemSubLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                print(uId)
                if(shpId==rString['typeId']):
                    mItemId = rString['mItemId']
                    validMItem = validateShopMainItem(mItemId, rString['typeId'])
                    if(validMItem==1):
                        if(rString['updateType']=="mItem"):
                            mainItemName = rString['mItemName']
                            isActiveFl = rString['isActiveFl']
                            updatedMItem = updateMItem(mainItemName,mItemId,isActiveFl)
                            if(updatedMItem==1):
                                x = json
                                x = {"Data":"Update successful"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Error occured while updating"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        elif(rString['updateType']=="subItem"):
                            subItemId = rString['subItemId']
                            validSubItem = validateSubItem(subItemId,mItemId)
                            if(validSubItem==1):
                                subItemName = rString['subItemName']
                                isActiveFl = rString['isActiveFl']
                                updatedSubItem = updateSubItem(subItemName, subItemId, isActiveFl)
                                if(updatedSubItem==1):
                                    x = json
                                    x = {"Data":"Item update successful"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Error occured while updatinf subItem"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Sell price can't be more than MRP"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        elif(rString['updateType']=="quantity"):
                            subItemId = rString['subItemId']
                            quantityId = rString['quantId']
                            isValidQuant = validateQuantEntry(subItemId,quantityId)
                            if(isValidQuant==1):
                                quantity = rString['quantity']
                                value = rString['value']
                                mrp = rString['mrp']
                                if(value<=mrp):
                                    isUpdatedQuant = updateQuantInfo(quantity, value, mrp, quantityId)
                                    if (isUpdatedQuant == 1):
                                        x = json
                                        x = {"Data":"Quantity updated"}
                                        response = JsonResponse(x)
                                        response.status_code = 200
                                    else:
                                        x = json
                                        x = {"Data":"Issue while updating quantity"}
                                        response = JsonResponse(x)
                                        response.status_code = 409
                                else:
                                    x = json
                                    x = {"Data":"Incorrect prices"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Incorrect quantity info"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Action not alloed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Main item Id doesn't belong to the store"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect user Id"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def fetchGstList(request):
    try:
        response = returnGstTable()
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def createGstRate(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="ADM")):
                adminId = getAdmId(uId)
                if(adminId==rString['typeId']):
                    gstName = rString['gstName']
                    gstRate = rString['gstRate']
                    validGstName = validateGstName(gstName)
                    if(validGstName==0):
                        insertGst = insertValidGst(gstName, gstRate)
                        if(insertGst==1):
                            x = json
                            x = {"Data":"GST value added"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while inserting GST values"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"GST name already taken"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect user Id"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def updateGstRate(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="ADM")):
                adminId = getAdmId(uId)
                if(adminId==rString['typeId']):
                    gstName = rString['gstName']
                    gstRate = rString['gstRate']
                    gstId = rString['gstId']
                    validGstName = validateGstName(gstName)
                    if(validGstName==0):
                        updateGst = updateValidGst(gstName, gstRate, gstId)
                        if(updateGst==1):
                            x = json
                            x = {"Data":"GST value added"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while inserting GST values"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        if(validGstName==gstId):
                            updateGst = updateValidGst(gstName, gstRate, gstId)
                            if(updateGst==1):
                                x = json
                                x = {"Data":"GST value added"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while inserting GST values"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"GST Name already taken"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect user Id"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def createCateogary(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="ADM")):
                adminId = getAdmId(uId)
                if(adminId==rString['typeId']):
                    insertType = rString['insertType']
                    if(insertType=="items"):
                        catName = rString['catName']
                        catKey = rString['catKeyword']
                        shopCatId = rString['shopCatId']
                        validShopCatId = validateShopCatId(shopCatId)
                        if(validShopCatId==1):
                            insertType = insertItemCat(catName, catKey, shopCatId)
                            if(insertType==1):
                                x = json
                                x = {"Data":"new cat inserted"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating new cateogary"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect Shop Cateogary"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    elif(insertType=="mjAvailable"):
                        mjName = rString['mjName']
                        inserType = insertMjAvailable(mjName)
                        if(inserType==1):
                            x = json
                            x = {"Data":"new cat inserted"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while creating new cateogary"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    elif(insertType=="shop"):
                        mjId = rString['mjId']
                        catName = rString['catName']
                        catKey = rString['catKeyword']
                        insertType = insertShopCat(catName, catKey, mjId)
                        if(insertType==1):
                            x = json
                            x = {"Data":"new cat inserted"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while creating new cateogary"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect inserType"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect user Id"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def createAddOn(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesAddon = evaluatePropertyShop("useAddon",shpId)
                    if(usesAddon==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        addOnName = rString['addOnName']
                        addOnCharge = rString['addOnCharge']
                        isMultiSelect = rString['isMultiSelect']
                        doesExist = checkIfExistAddOn(addOnName, shpId)
                        if(doesExist==0):
                            createNewAddOn = creatingNewAddOn(addOnCharge, addOnName, shpId, isMultiSelect)
                            if(createNewAddOn==1):
                                x = json
                                x = {"Data":"AddOn inserted"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating add-on"}
                                response= JsonResponse(x)
                                response(x)
                        else:
                            x = json
                            x = {"Data":"Add-on already exist"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid userType selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def updateAddOn(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesAddon = evaluatePropertyShop("useAddon",shpId)
                    if(usesAddon==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        addOnName = rString['addOnName']
                        addOnCharge = rString['addOnCharge']
                        isActiveFl = rString['isActiveFl']
                        isMultiSelect = rString['isMultiSelect']
                        doesExist = checkIfExistAddOn(addOnName, shpId)
                        if(doesExist!=0):
                            createNewAddOn = updatingAddOn(addOnCharge, addOnName, shpId, isActiveFl, doesExist, isMultiSelect)
                            if(createNewAddOn==1):
                                x = json
                                x = {"Data":"AddOn updated"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while updating add-on"}
                                response= JsonResponse(x)
                                response(x)
                        else:
                            x = json
                            x = {"Data":"Add-on does not exist"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid userType selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def createSubs(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("subscription",shpId)
                    if(usesSub==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        noOfDays = rString['noOfDays']
                        subValue = rString['subValue']
                        doesExist = checkIfExistSub(noOfDays, shpId)
                        if(doesExist==0):
                            createNewAddOn = insertSub(noOfDays, subValue, shpId)
                            if(createNewAddOn==1):
                                x = json
                                x = {"Data":"Subscription info created"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating Subscription"}
                                response= JsonResponse(x)
                                response(x)
                        else:
                            x = json
                            x = {"Data":"Subscription already exist"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid userType selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def updateSubs(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("subscription",shpId)
                    if(usesSub==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        noOfDays = rString['noOfDays']
                        subValue = rString['subValue']
                        isActiveFl = rString['isActiveFl']
                        doesExist = checkIfExistSub(noOfDays, shpId)
                        if(doesExist!=0):
                            createNewAddOn = doUpdateSubs(noOfDays, subValue, shpId, isActiveFl, doesExist)
                            if(createNewAddOn==1):
                                x = json
                                x = {"Data":"Subscription updated"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while updating Subscription"}
                                response= JsonResponse(x)
                                response(x)
                        else:
                            x = json
                            x = {"Data":"Subscription does not exist"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid userType selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def inserAddOnItemList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("useAddon",shpId)
                    if(usesSub==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        addOnId = rString['addOnId']
                        addOnItemName = rString['addOnItemName']
                        isValidAddOnId = validateAddOnId(addOnId, shpId)
                        if(isValidAddOnId>0):
                            doesExist = checkIfAddOnItemExist(addOnId, addOnItemName)
                            if(doesExist==0):
                                createNewAddOn = insertFinalItemAddOn(addOnId, addOnItemName)
                                if(createNewAddOn==1):
                                    x = json
                                    x = {"Data":"AddOn Id inserted"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while inserting AddOn Item"}
                                    response= JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Add-On item already exist"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Invalid addonId passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid userType selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def updateAddOnItemList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("useAddon",shpId)
                    if(usesSub==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        addOnId = rString['addOnId']
                        addOnItemName = rString['addOnItemName']
                        isActiveFl = rString['isActiveFl']
                        isValidAddOnId = validateAddOnId(addOnId, shpId)
                        if(isValidAddOnId>0):
                            doesExist = checkIfAddOnItemExist(addOnId, addOnItemName)
                            if(doesExist!=0):
                                createNewAddOn = updateFinalItemAddOn(addOnId, addOnItemName, isActiveFl, doesExist)
                                if(createNewAddOn==1):
                                    x = json
                                    x = {"Data":"AddOn Id updated"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while updating AddOn Item"}
                                    response= JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Add-On item already exist"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Invalid addonId passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid userType selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generateShopAddonList(request):
    try:
        rString = json.loads(request.body)
        shopId = rString['shopId']
        usesSub = evaluatePropertyShop("useAddon",shopId)
        if(usesSub>0):
            response = createAddOnListShop(shopId)
        else:
            x = json
            x = {"Data":"Shop has no addon info"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generateShopSubsList(request):
    try:
        rString = json.loads(request.body)
        shopId = rString['shopId']
        usesSub = evaluatePropertyShop("subscription",shopId)
        if(usesSub>0):
            response = createSubsListShop(shopId)
        else:
            x = json
            x = {"Data":"Shop has no addon info"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generatecustomshopitemcat(request):
    try:
        rString = json.loads(request.body)
        shopId = rString['shopId']
        response = createShopItemCat(shopId)
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def insertQuantInfo(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("useSubQuantity",shpId)
                    subItemId = rString['subItemId']
                    totalQuantInfo = rString['totalQuantInfo']
                    if totalQuantInfo<usesSub or totalQuantInfo==usesSub :
                        x = 0
                        isValidSubItem = validateSubItemShop(subItemId, shpId)
                        if(isValidSubItem==1):
                            isInsertionQuant = validateQuantityInsertion(subItemId)
                            if(isInsertionQuant==0):
                                x = 0
                                l = 0
                                for each in rString['quantInfo']:
                                    if(x==totalQuantInfo):
                                        l = 0
                                        break
                                    x = x+1
                                    if(each['value']>each['mrp']):
                                        l = 0
                                        break
                                    else:
                                        insertquantityinfo(each['quantity'], each['value'], each['mrp'], subItemId)
                                        l = 1
                                if(l==1):
                                    x = json
                                    x = {"Data":"Values inserted"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while inserting quantity"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"quantity information already present"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect sub-item selected"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect quantity info"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect user-type selected"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Invalid session info passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Spelling issue"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def uploadItemImage(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        logger.info(uId)
        userId = int(request.POST['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((userType==request.POST['userType'])):
                if(userType=="SHP"):
                    typeId1 = getShpId(uId)
                    typeId = int(request.POST['typeId'])
                    if(typeId1==typeId):
                        subItemId = (request.POST['subItemId'])
                        validItem = validateSubItemShop(subItemId,typeId)
                        if(validItem==1):
                            isUploadAllowed = validateUpload(subItemId,typeId)
                            if(isUploadAllowed==1):
                                response = saveItemImage(request.FILES['proPic'], subItemId)
                            else:
                                x = json
                                x = {"Data":"Image upload not allowed"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Invalid subItem selected"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Invalid type Id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Invalid user type selected"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Invalid user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def setTimeShopMaster(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("subscription",shpId)
                    if(usesSub==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        time = rString['time']
                        name = rString['name']
                        desc = rString['desc']
                        insertTimeData = validateTimePropInsert(time,name,desc,shpId)
                        if(insertTimeData==1):
                            x = json
                            x = {"Data":"Value inserted successfuly"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while creating time property"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect Type Id passed"}
                    response=JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def viewTimeShopMaster(request):
    try:
        if(request.body):
            rString = json.loads(request.body)
            shopId = rString['shopId']
            isPresentTime = checkIfPresentTime(shopId)
            if(isPresentTime==1):
                response = createTimeInfo(shopId)
            else:
                x = json
                x = {"totalCount":0,"Data":[]}
                response = JsonResponse(x)
                response.status_code = 200
        else:
            x = json
            x = {"Data":"We need a body"}
            response = JsonResponse(x)
            response.status_code = 404
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def updateTimeShopMaster(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    usesSub = evaluatePropertyShop("subscription",shpId)
                    if(usesSub==0):
                        x = json
                        x = {"Data":"Not allowed to create addOn"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        time = rString['time']
                        name = rString['name']
                        desc = rString['desc']
                        timeId = rString['timeId']
                        isActive = rString['isActive']
                        ifUpdateTimeData = validateIfUpdatePossible(shpId,timeId)
                        if(ifUpdateTimeData==1):
                            ifUpdateDone = updateTimeInformation(time,name,desc,timeId,isActive)
                            if(ifUpdateDone==1):
                                x = json
                                x = {"Data":"Update successful"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while updating"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect time-Id passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect Type Id passed"}
                    response=JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type passed"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def viewingAllMj(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="ADM")):
                adminId = getAdmId(uId)
                if adminId==rString['typeId']:
                    response = getMjListAdmin()
                else:
                    x = json
                    x = {"Data":"Incorrect session"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect session"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response
