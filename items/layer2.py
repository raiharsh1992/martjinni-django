# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import json
from django.http import JsonResponse
from django.db import connection
import django.db
import logging
from login.layer2 import evaluatePropertyShop
import boto3
from botocore.client import Config
from time import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/address.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

#returns itemlist that is public to all :)
def returnItemList(shopId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from items_mainitem where shopId_id=%s;",[shopId])
        rowM = cursor.fetchone()
        mItem = []
        if rowM is None:
            rData = {"Data":"Sorry the store has not added items yet","totalCount":0}
            response = JsonResponse(rData)
            response.status_code = 200
        else:
            countM = 0
            while rowM is not None:
                with connection.cursor() as cursorM:
                    cursorM.execute("SELECT * from items_subitem where mItemId_id=%s;",[rowM[0]])
                    rowS = cursorM.fetchone()
                    countS = 0
                    subItem = []
                    while rowS is not None:
                        with connection.cursor() as cursorG:
                            cursorG.execute("SELECT gstPercentage from items_gstmaster where gstId=%s",[rowS[9]])
                            rowG = cursorG.fetchone()
                        with connection.cursor() as cursorQ:
                            quantInfo = []
                            row_countQ = cursorQ.execute("SELECT * from items_quantityinfo where subItemId_id=%s;",[rowS[0]])
                            if(row_countQ>0):
                                rowQ = cursorQ.fetchone()
                                while rowQ is not None:
                                    quantInfo.append({
                                        "quantity":rowQ[1],
                                        "quantId":rowQ[0],
                                        "value":rowQ[2],
                                        "mrp":rowQ[3],
                                        "isActive":rowQ[4]
                                    })
                                    rowQ = cursorQ.fetchone()
                        subItem.append({
                            "Name":rowS[1],
                            "subItemId":rowS[0],
                            "description":rowS[2],
                            "gstId":rowS[9],
                            "gstRate":rowG[0],
                            "addOnInfo":rowS[5],
                            "imgUrl":rowS[7],
                            "subsInfo":rowS[6],
                            "spclCat":rowS[8],
                            "quantInfo":quantInfo,
                            "isActive":rowS[3]
                        })
                        countS = countS + 1
                        rowS = cursorM.fetchone()
                mItem.append({
                    "Name":rowM[1],
                    "mainItemId":rowM[0],
                    "itemCat":rowM[5],
                    "sutItemCount":countS,
                    "subItems":subItem,
                    "isActive":rowM[2]
                })
                countM = countM + 1
                rowM = cursor.fetchone()
            container = {}
            container['totalCount']=countM
            container['data']=mItem
            response=JsonResponse(container)
            response.status_code=200
        return response

def insertMainItem(mainItemName, shpId, itemCatId):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO items_mainitem (mItemName, isActive, isDailyNeeds, isSubscription, shopId_id, itemCat) VALUES(%s, 1,  1, 0, %s, %s)",[mainItemName,shpId,itemCatId])
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.exception(TypeError)
            return 0

def validateShopMainItem(mItemId, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_mainitem where mItemId=%s and shopId_id=%s",[mItemId,shopId])
        if(row_count>0):
            return 1
        else:
            return 0

def insertSubItem(subItemName, gstId, mItemId, shpId, addOnInfo, susbInfo, itemDesc, spclCat):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO items_subitem (subItemName, description, isActive, addedBy, mItemId_id, gstId_id, addOnInfo, subsInfo, spclCat, imgUrl) VALUES(%s, %s, 1, %s, %s, %s, %s, %s, %s,%s)",[subItemName, itemDesc, shpId, mItemId, gstId, addOnInfo, susbInfo, spclCat,""])
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.exception(TypeError)
            return 0

def validateSubItem(subItemId,mItemId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_subitem where subItemId=%s and mItemId_id=%s",[subItemId,mItemId])
        if(row_count>0):
            return 1
        else:
            return 0

def updateMItem(mainItemName,mItemId, isActiveFl):
    with connection.cursor() as cursor:
        try:
            cursor.execute("UPDATE items_mainitem SET mItemName=%s, isActive=%s where mItemId=%s",[mainItemName,isActiveFl,mItemId])
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.exception(TypeError)
            return 0

def updateSubItem(subItemName, subItemId, isActiveFl):
    with connection.cursor() as cursor:
        try:
            cursor.execute("UPDATE items_subitem SET subItemName=%s, isActive=%s where subItemId=%s",[subItemName, isActiveFl,subItemId])
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def validateGstId(gstId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_gstmaster where gstId=%s",[gstId])
        if(row_count>0):
            return 1
        else:
            return 0

def returnGstTable():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_gstmaster")
        if(row_count>0):
            row = cursor.fetchone()
            gstData = []
            while row is not None:
                gstData.append({
                    "gstName":row[1],
                    "gstId":row[0],
                    "gstRate":row[2]
                })
                row = cursor.fetchone()
            container = {}
            container['data'] = gstData
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Sorry no values present"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def insertValidGst(gstName, gstRate):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_gstmaster (gstName, gstPercentage) VALUES (%s, %s)",[gstName, gstRate])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updateValidGst(gstName, gstRate, gstId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_gstmaster set gstName=%s, gstPercentage=%s where gstId=%s",[gstName,gstRate,gstId])
        try:
            connection.commit()
            return 1
        except IntegrityError:
            connection.rollback()
            return 0

def validateGstName(gstName):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_gstmaster where gstName=%s",[gstName])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def validateCatId(catId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_cateogaryitems where catId=%s",[catId])
        if(row_count>0):
            return 1
        else:
            return 0

def viewItemsCateogary():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_cateogaryitems")
        if(row_count>0):
            row = cursor.fetchone()
            gstData = []
            while row is not None:
                gstData.append({
                    "catId":row[0],
                    "catName":row[1]
                })
                row = cursor.fetchone()
            container = {}
            container['data'] = gstData
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Sorry no values present"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def viewShopsCateogary():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_cateogaryshops")
        if(row_count>0):
            row = cursor.fetchone()
            gstData = []
            while row is not None:
                gstData.append({
                    "catId":row[0],
                    "catName":row[1]
                })
                row = cursor.fetchone()
            container = {}
            container['data'] = gstData
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Sorry no values present"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def insertItemCat(catName, catKey, shopCatId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_cateogaryitems (catName, catKeyword, shopCatId_id) VALUES (%s, %s, %s)",[catName,catKey,shopCatId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def insertShopCat(catName, catKey, mjId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO login_cateogaryshops (catName, catKeyword, mjId_id) VALUES (%s, %s, %s)",[catName,catKey,mjId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def validateShopCatId(catId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * from login_cateogaryshops where catId=%s",[catId])
        if(row_count>0):
            return 1
        else:
            return 0

def insertMjAvailable(mjName):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO login_mjavailable (mjName) VALUES (%s)",[mjName])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def creatingNewAddOn(addOnCharge, addOnName, shpId, isMultiSelect):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_addons (addOnName, addOnCharge, shopId_id, isActiveFl, isMultiSelect) VALUES (%s, %s, %s, %s, %s)",[addOnName, addOnCharge, shpId, 1, isMultiSelect])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def checkIfExistAddOn(addOnName, shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_addons where addOnName=%s and shopId_id=%s",[addOnName,shpId])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def updatingAddOn(addOnCharge, addOnName, shpId, isActiveFl, addId, isMultiSelect):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_addons set addOnCharge=%s, isActiveFl=%s, isMultiSelect=%s where addonId=%s",[addOnCharge, isActiveFl, isMultiSelect,addId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def checkIfExistSub(noOfDays, shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_subscription where subNoDays=%s and shopId_id=%s",[noOfDays,shpId])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def insertSub(noOfDays, subValue, shpId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_subscription (subNoDays, subValue, shopId_id, isActiveFl) VALUES (%s, %s, %s, %s)",[noOfDays, subValue, shpId, 1])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def doUpdateSubs(noOfDays, subValue, shpId, isActiveFl, subId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_subscription set subValue=%s, isActiveFl=%s where subId=%s",[subValue,isActiveFl,subId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def checkIfAddOnItemExist(addOnId, addOnItemName):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_addonitems where addOnId_id=%s and addOnItemName=%s",[addOnId, addOnItemName])
        if(row_count==1):
            row = cursor.fetchone()
            return row[1]
        else:
            return 0

def insertFinalItemAddOn(addOnId, addOnItemName):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_addonitems (addOnItemName, addOnId_id, isActiveFl) VALUES (%s, %s, %s)",[addOnItemName, addOnId, 1])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def validateAddOnId(addOnId, shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_addons where addOnId=%s and shopId_id=%s",[addOnId, shpId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def updateFinalItemAddOn(addOnId, addOnItemName, isActiveFl, doesExist):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_addonitems set addOnItemName=%s, isActiveFl=%s where addOnItemIt=%s",[addOnItemName, isActiveFl, doesExist])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def createAddOnListShop(shopId):
    print(shopId)
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_addons where shopId_id=%s",[shopId])
        if(row_count>0):
            container = []
            row = cursor.fetchone()
            while row is not None:
                dummyObj = []
                with connection.cursor() as cursorM:
                    rowCount = cursorM.execute("SELECT * from items_addonitems where addOnId_id=%s ",[row[0]])
                    if(rowCount>0):
                        rowM = cursorM.fetchone()
                        while rowM is not None:
                            dummyObj.append({
                                "addOnItemName":rowM[0],
                                "addOnItemId":rowM[1],
                                "isActiveFl":rowM[2]
                            })
                            rowM = cursorM.fetchone()
                container.append({
                    "groupId":row[0],
                    "groupName":row[1],
                    "groupCharge":row[2],
                    "isMultiSelect":row[3],
                    "isActiveFl":row[4],
                    "itemInfo":dummyObj
                })
                row = cursor.fetchone()
            myOby = {}
            myOby['data']=container
            response = JsonResponse(myOby)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No addon by shop"}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def createSubsListShop(shopId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * from items_subscription where shopId_id=%s",[shopId])
        if(row_count>0):
            row = cursor.fetchone()
            container = []
            while row is not None:
                container.append({
                    "noOfDays":row[1],
                    "suscriptionPrice":row[2],
                    "subscriptionId":row[0],
                    "isActive":row[3]
                })
                row = cursor.fetchone()
            myObj = {}
            myObj['data']=container
            response = JsonResponse(myObj)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No subscription by shop"}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def createShopItemCat(shopId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * from login_shopmaster where shopId=%s and isActiveFl=%s",[shopId,1])
        if(row_count>0):
            row = cursor.fetchone()
            catList = row[16].split(",")
            container = []
            for x in catList:
                with connection.cursor() as cursorM:
                    row_count1=cursorM.execute("SELECT * from items_cateogaryitems where shopCatId_id=%s",[x])
                    if(row_count1>0):
                        myObj = []
                        rowM = cursorM.fetchone()
                        while rowM is not None:
                            myObj.append({
                                "itemCatName":rowM[1],
                                "itemCatId":rowM[0]
                            })
                            rowM = cursorM.fetchone()
                with connection.cursor() as cursorU:
                    row_countU=cursorU.execute("SELECT * from login_cateogaryshops where catId=%s",[x])
                    if(row_countU>0):
                        rowU = cursorU.fetchone()
                        container.append({
                            "catName":rowU[1],
                            "catId":rowU[0],
                            "catItemList":myObj
                        })
            finalObj = {}
            finalObj['data']=container
            response = JsonResponse(finalObj)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No itemCat for shop"}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def validateItemCat(shpId,itemCatId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT catId from login_shopmaster where shopId=%s",[shpId])
        if(row_count==1):
            row = cursor.fetchone()
            catId = row[0].split(",")
            print(catId)
            l = 0
            for x in catId:
                print(x)
                with connection.cursor() as cursorM:
                    rowCount=cursorM.execute("SELECT catId from items_cateogaryitems where shopCatId_id=%s",[x])
                    if(rowCount>0):
                        rowM = cursorM.fetchone()
                        while rowM is not None:
                            subCat = rowM[0]
                            print(subCat)
                            if(subCat==itemCatId):
                                l=1
                                break
                            rowM = cursorM.fetchone()
            if(l==1):
                return 1
            else:
                return 0
        else:
            return 0

def updateShopmaste(itemCatId, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT subCatId from login_shopmaster where shopId=%s",[shopId])
        if(row_count==1):
            row = cursor.fetchone()
            catId = row[0]
            if(catId==""):
                catId = itemCatId
            else:
                if(str(itemCatId) not in catId):
                    catId = catId + "," +str(itemCatId)
    insertCatShop(catId, shopId)

def insertCatShop(catId, shopId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE login_shopmaster set subCatId=%s,isDailyNeeds=%s where shopId=%s",[catId,1,shopId])
        try:
            connection.commit()
        except:
            connection.rollback()

def isValidGstId(gstId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_gstmaster where gstId=%s",[gstId])
        if(row_count==1):
            return 1
        else:
            return 0

def isValidAddOnInfo(addOnInfo,shpId):
    smaalInfo = list(addOnInfo)
    with connection.cursor() as cursor:
        l = 1
        for x in smaalInfo:
            print(x)
            row_count = cursor.execute("SELECT * from items_addons where addOnId=%s and shopId_id=%s and isActiveFl=%s",[x,shpId,1])
            if(row_count==1):
                l = 1
            else:
                l = 0
                break
    if(l==0):
        return 0
    else:
        return 1

def isValidSubsInfo(subsInfoDummy,shpId):
    smaalInfo = list(subsInfoDummy)
    with connection.cursor() as cursor:
        l = 1
        for x in smaalInfo:
            row_count = cursor.execute("SELECT * from items_subscription where subId=%s and shopId_id=%s and isActiveFl=%s",[x,shpId,1])
            if(row_count!=1):
                if(row_count==1):
                    l = 1
                else:
                    l = 0
                    break
    if(l==0):
        return 0
    else:
        return 1

def validateSubItemShop(subItem, shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT mItemId_id from items_subitem where subItemId=%s",[subItem])
        if(row_count==1):
            row = cursor.fetchone()
            with connection.cursor() as cursorM:
                row_count1 = cursorM.execute("SELECT shopId_id from items_mainitem where mItemId=%s",[row[0]])
                if(row_count1==1):
                    rowU = cursorM.fetchone()
                    if(rowU[0]==shpId):
                        return 1
                    else:
                        return 0
                else:
                    return 0
        else:
            return 0

def insertquantityinfo(quantity, value, mrp, subItemId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_quantityinfo (quantity, value, mrp, isActiveFl, subItemId_id) values (%s, %s, %s, 1, %s)",[quantity, value, mrp, subItemId])
        try:
            connection.commit()
        except:
            connection.rollback()

def validateQuantityInsertion(subItemId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * FROM items_quantityinfo where subItemId_id=%s",[subItemId])
        if(row_count>0):
            return 1
        else:
            return 0

def validateQuantEntry(subItemId,quantityId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_quantityinfo where quantId=%s and subItemId_id=%s",[quantityId, subItemId])
        if(row_count==1):
            return 1
        else:
            return 0

def updateQuantInfo(quantity, value, mrp, quantityId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_quantityinfo set quantity=%s, value=%s, mrp=%s where quantId=%s",[quantity, value, mrp, quantityId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def saveItemImage(imageFile, subItemId):
    ACCESS_KEY_ID = 'AKIAI76WOXG7MZ6IT3YQ'
    ACCESS_SECRET_KEY = '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'
    BUCKET_NAME = 'marjiniitems'
    FILE_NAME = subItemId+".jpeg"
    s3 = boto3.resource(
        's3',
        aws_access_key_id=ACCESS_KEY_ID,
        aws_secret_access_key=ACCESS_SECRET_KEY,
        config=Config(signature_version='s3v4')
    )
    s3.Bucket(BUCKET_NAME).put_object(Key=FILE_NAME, Body=imageFile, ACL='public-read')
    xShop = int(subItemId)
    y = 0
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_subitem SET imgUrl='1' where subItemId=%s",[xShop])
        try:
            connection.commit()
            y = 1
        except:
            y = 0
            connection.rollback
    if(y==1):
        x = json
        x = {"Data":"Image uploaded"}
        response = JsonResponse(x)
        response.status_code = 200
    else:
        x = json
        x = {"Data":"Issue while uploading image"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def validateUpload(subItemId,shopId):
    usesSub = evaluatePropertyShop("maxImage",shopId)
    if(usesSub>0):
        with connection.cursor() as cursor:
            row_count = cursor.execute("SELECT imgUrl FROM items_subitem where subItemId=%s",[subItemId])
            if(row_count==1):
                row = cursor.fetchone()
                imgUrl = row[0]
                if(imgUrl==""):
                    with connection.cursor() as cursorM:
                        row_countM = cursorM.execute("SELECT * from items_subitem where addedBy=%s and imgUrl!=%s",[shopId,""])
                        if(row_countM<usesSub):
                            return 1
                        else:
                            return 0
                else:
                    return 1
            else:
                return 0
    else:
        return 0

def validateTimePropInsert(time,name,desc, shopId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO items_shoptimemaster (timming, name, descp, isActive, shopId_id) values (%s, %s, %s, 1, %s)",[time, name, desc, shopId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def checkIfPresentTime(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_shoptimemaster where shopId_id=%s",[shopId])
        if(row_count>0):
            return 1
        else:
            return 0

def createTimeInfo(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_shoptimemaster where shopId_id=%s",[shopId])
        if(row_count>0):
            myObj = []
            row = cursor.fetchone()
            while row is not None:
                timeUse = row[1]
                myObj.append({
                    "timeId":row[0],
                    "time":row[1],
                    "name":row[2],
                    "desc":row[3],
                    "isActive":row[4],
                })
                row = cursor.fetchone()
            container = {}
            container['Data']=myObj
            container['totalCount'] = row_count
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def validateIfUpdatePossible(shopId,timeId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_shoptimemaster where shopId_id=%s and timeId=%s",[shopId,timeId])
        if(row_count==1):
            return 1
        else:
            return 0

def updateTimeInformation(time,name,desc,timeId,isActive):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE items_shoptimemaster set timming=%s, name=%s, descp=%s, isActive=%s where timeId=%s",[time,name,desc,isActive,timeId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def getMjListAdmin():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_mjavailable")
        i = 0
        myObj = []
        while i<row_count:
            row = cursor.fetchone()
            myObj.append({
                "mjName":row[1],
                "mjId":row[0]
            })
            i = i+1
        x = json
        x = {"Data":myObj,"count":row_count}
        response = JsonResponse(x)
        response.status_code = 200
        return response
