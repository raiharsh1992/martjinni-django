# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from login.models import shopMaster,cateogaryShops
# Create your models here.
class mainItem(models.Model):
    """docstring for userMaster."""
    mItemId = models.AutoField(max_length=32, primary_key=True)
    mItemName = models.CharField(max_length=50, blank=False)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    isActive = models.BooleanField(blank=False)
    isDailyNeeds = models.BooleanField(blank=False)
    isSubscription = models.BooleanField(blank=False)
    itemCat = models.CharField(blank=False,default=1,max_length=32)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class gstMaster(models.Model):
    gstId = models.AutoField(max_length=32, primary_key=True)
    gstName = models.CharField(max_length=32, blank=False, unique=True)
    gstPercentage = models.IntegerField(blank=False)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class cateogaryItems(models.Model):
    catId = models.AutoField(max_length=32, primary_key=True)
    catName = models.CharField(max_length=32, blank=False)
    catKeyword = models.CharField(max_length=100, blank=False)
    shopCatId = models.ForeignKey(cateogaryShops, blank=True)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class addOns(models.Model):
    addonId = models.AutoField(max_length=32, primary_key=True)
    addOnName = models.CharField(max_length=32, blank=False)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    addOnCharge = models.IntegerField(blank=False)
    isMultiSelect = models.BooleanField(blank=False, default=0)
    isActiveFl = models.BooleanField(blank=False, default=1)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class addOnItems(models.Model):
    addOnId = models.ForeignKey(addOns, on_delete=models.PROTECT)
    addOnItemName = models.CharField(max_length=32, blank=False)
    addOnItemIt = models.AutoField(max_length=32, primary_key=True)
    isActiveFl = models.BooleanField(blank=False, default=1)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class subscription(models.Model):
    subId = models.AutoField(max_length=32, primary_key=True)
    subNoDays = models.IntegerField(blank=False)
    subValue = models.IntegerField(blank=False)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    isActiveFl = models.BooleanField(blank=False, default=1)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class subItem(models.Model):
    subItemId = models.AutoField(max_length=32, primary_key=True)
    mItemId = models.ForeignKey(mainItem, on_delete=models.PROTECT)
    subItemName = models.CharField(max_length=50, blank=False)
    description = models.CharField(max_length=254, blank=False)
    isActive = models.BooleanField(blank=False)
    addedBy = models.CharField(max_length=32, blank=False)
    gstId = models.ForeignKey(gstMaster, on_delete=models.PROTECT, default=1, blank=True)
    addOnInfo = models.CharField(max_length=32, blank=True)
    subsInfo = models.CharField(max_length=32, blank=True)
    imgUrl = models.CharField(blank=True, max_length=150, default="null")
    spclCat = models.CharField(blank=True, max_length=10)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class quantityInfo(models.Model):
    subItemId = models.ForeignKey(subItem, on_delete=models.PROTECT)
    quantId = models.AutoField(max_length=32, primary_key=True)
    quantity = models.CharField(max_length=32, blank=False)
    value = models.IntegerField(blank=False)
    mrp = models.IntegerField(blank=False)
    isActiveFl = models.BooleanField(blank=False)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class shopTimemaster(models.Model):
    timeId = models.AutoField(max_length=32, primary_key=True)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    timming = models.TimeField(blank=False, unique=False)
    name = models.CharField(max_length=32, unique=False, blank=False)
    descp = models.CharField(max_length=200, unique=False, blank=True)
    isActive = models.BooleanField(blank=False)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg
