# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import json
from django.http import JsonResponse
import logging
from layer1 import fetchItem,addItemMainLayer,addItemSubLayer,updateItemSubLayer,setTimeShopMaster,fetchGstList,createGstRate,updateGstRate,viewCateogaryList,createCateogary,createAddOn,updateAddOn,createSubs,updateSubs,inserAddOnItemList,updateAddOnItemList,generateShopAddonList,generateShopSubsList,generatecustomshopitemcat,insertQuantInfo,uploadItemImage,viewTimeShopMaster,updateTimeShopMaster,viewingAllMj

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/items.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)
# Create your views here.


def itemlist(request):
    try:
        if (request.method=="POST"):
            if (request.body):
                response = fetchItem(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def addmainitem(request):
    try:
        if (request.method=="POST"):
            if (request.body):
                response = addItemMainLayer(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def addsubitem(request):
    try:
        if (request.method=="POST"):
            if (request.body):
                response = addItemSubLayer(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def updateitem(request):
    try:
        if (request.method=="POST"):
            if (request.body):
                response = updateItemSubLayer(request)
                logger.info(response)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getGstMasterValues(request):
    try:
        if (request.method=="POST"):
            response = fetchGstList(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def createGstMasterValues(request):
    try:
        if (request.method=="POST"):
            response = createGstRate(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def updateGstMasterValues(request):
    try:
        if (request.method=="POST"):
            response = updateGstRate(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewCateogary(request):
    try:
        if (request.method=="POST"):
            response = viewCateogaryList(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def insertCateogary(request):
    try:
        if (request.method=="POST"):
            response = createCateogary(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def insertaddon(request):
    try:
        if (request.method=="POST"):
            response = createAddOn(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def updateaddon(request):
    try:
        if (request.method=="POST"):
            response = updateAddOn(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def insertsubsinfo(request):
    try:
        if (request.method=="POST"):
            response = createSubs(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def updatesubsinfo(request):
    try:
        if (request.method=="POST"):
            response = updateSubs(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def iteminsertaddon(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = inserAddOnItemList(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def itemupdateaddon(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = updateAddOnItemList(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def shopaddoninfo(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = generateShopAddonList(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def shopsubsinfo(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = generateShopSubsList(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def showshopitemcat(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = generatecustomshopitemcat(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def insertquantityinfo(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = insertQuantInfo(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def insertitemimage(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = uploadItemImage(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def insertshoptime(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = setTimeShopMaster(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def viewshoptime(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = viewTimeShopMaster(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def updateshoptime(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = updateTimeShopMaster(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response

def viewmjcat(request):
    try:
        if (request.method=="POST"):
            logger.info(request)
            response = viewingAllMj(request)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method for fetching shoplist')
            return response
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
    return response
