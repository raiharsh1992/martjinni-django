#space for import
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
# from django.shortcuts import render
from django.http import JsonResponse
import logging
from layer2 import validateClientLogin,validateAdminLogin,validateCustPH,validateShpPH,validateDmPH,validateAdmPH,insertOtp,validateOTP,createCust,validateClientExistence,validateKey,vlaidatesessiontype,getAdmId,createShop,getShpId,createDmuser,sessionInactiver,generateDMList,storeStatus,updateStoreStatus,getListShop,getListCust,validateCatId,saveImage,validShopById,insertPopertyValueMaster,isValidInsertProperty,insertShopProperty,updateShopProperty,checkShopProperty,doesPropertyExist,generateShopPropertyList,updateStoreActive,getCustId,validateUserPhoneCust,validateShopPhoneCust,validateAdmPhoneCust,validateDmPhoneCust,getDMId,validateChngOtp,validateChngOtp2,custUid,shopId,admId,dmId,updatingPass,isExistingFav,addingFav,generateFinalList,validDeleteFav,creatingContact,generateContactFormDetails,getValidToken,InsertNewUserToken,updateUserToken,validateKeyOld,sendAllRegNotification,sendToAllShops,sendToAllCust,sendToAllDm,sendToUser,sendAdminNotif,reviewList,reviewDetails,getCurrentDMCount,evaluatePropertyShop,insertNewTokenClient
from items.layer2 import checkIfExistSub,insertSub
from payment.layer2 import getMaxPromoCount

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/login.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

class userLoginData:
    uName = ""
    password = ""
    def __init__(self, uName, password):
        self.uName = uName
        self. password = password

class otpData:
    phoneNumber = ""
    userType = ""
    userNeed = ""
    def __init__(self, phoneNumber, userType, userNeed):
        self.phoneNumber = phoneNumber
        self.userType = userType
        self.userNeed = userNeed

def validateLogin(request):
    try:
        rString = json.loads(request)
        mode = rString['mode']
        if(mode=="client"):
            uName = rString['userName']
            password = rString['password']
            user = userLoginData(uName, password)
            response = validateClientLogin(user)
        elif(mode=="admin"):
            uName = rString['userName']
            password = rString['password']
            user = userLoginData(uName, password)
            response = validateAdminLogin(user,"ADM")
        elif(mode=="shop"):
            uName = rString['userName']
            password = rString['password']
            user = userLoginData(uName, password)
            response = validateAdminLogin(user,"SHP")
        elif(mode=="delmed"):
            uName = rString['userName']
            password = rString['password']
            user = userLoginData(uName, password)
            response = validateAdminLogin(user,"DM")
        else:
            rData = json
            rData = {"Data":"Wrong login mode selected"}
            response = JsonResponse(rData)
            response.status_code = 404
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def existingUser(request):
    try:
        rString = json.loads(request)
        uName = rString['userName']
        mode = rString['mode']
        if(mode=="client"):
            response = validateClientExistence(uName)
        else:
            rData = json
            rData = {"Data":"Wrong login mode selected"}
            response = JsonResponse(rData)
            response.status_code = 404
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def otpLayer(request):
    try:
        rString = json.loads(request)
        phoneNumber = rString['phoneNumber']
        userType = rString['userType']
        userNeed = rString['userNeed']
        if(userType=="CUST"):
            exist = validateCustPH(phoneNumber)
            if(((userNeed=="SIGNUP")&(exist==1))|((userNeed=="CHNGPWD")&(exist==0))):
                x =  json
                x = {"Data":"Incorrect parameters passed"}
                response = JsonResponse(x)
                response.status_code = 401
                logger.info(request)
            elif(((userNeed=="SIGNUP")&(exist==0))|((userNeed=="CHNGPWD")&(exist==1))):
                otrData = otpData(phoneNumber,userType,userNeed)
                otp = insertOtp(otrData)
                if otp==1:
                    x = json
                    x = {"Data":"Message sent"}
                    response = JsonResponse(x)
                    response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Something went wrong"}
                    response = JsonResponse(x)
                    response.status_code = 409
                    logger.warn(request)
            else:
                x = json
                x = {"Data":"Wrong parameters"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="SHP"):
            exist = validateShpPH(phoneNumber)
            if(((userNeed=="CREATE")&(exist==1))|((userNeed=="CHNGPWD")&(exist==0))):
                x =  json
                x = {"Data":"Incorrect parameters passed"}
                response = JsonResponse(x)
                response.status_code = 401
                logger.info(request)
            elif(((userNeed=="CREATE")&(exist==0))|((userNeed=="CHNGPWD")&(exist==1))):
                otrData = otpData(phoneNumber,userType,userNeed)
                otp = insertOtp(otrData)
                if otp==1:
                    x = json
                    x = {"Data":"Message sent"}
                    response = JsonResponse(x)
                    response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Something went wrong"}
                    response = JsonResponse(x)
                    response.status_code = 409
                    logger.warn(request)
            else:
                x = json
                x = {"Data":"Wrong parameters"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="DM"):
            exist = validateDmPH(phoneNumber)
            if(((userNeed=="CREATE")&(exist==1))|((userNeed=="CHNGPWD")&(exist==0))):
                x =  json
                x = {"Data":"Incorrect parameters passed"}
                response = JsonResponse(x)
                response.status_code = 401
                logger.info(request)
            elif(((userNeed=="CREATE")&(exist==0))|((userNeed=="CHNGPWD")&(exist==1))):
                otrData = otpData(phoneNumber,userType,userNeed)
                otp = insertOtp(otrData)
                if otp==1:
                    x = json
                    x = {"Data":"Message sent"}
                    response = JsonResponse(x)
                    response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Something went wrong"}
                    response = JsonResponse(x)
                    response.status_code = 409
                    logger.warn(request)
            else:
                x = json
                x = {"Data":"Wrong parameters"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="ADM"):
            exist = validateAdmPH(phoneNumber)
            if(((userNeed=="CREATE")&(exist==1))|((userNeed=="CHNGPWD")&(exist==0))):
                x =  json
                x = {"Data":"Incorrect parameters passed"}
                response = JsonResponse(x)
                response.status_code = 401
                logger.info(request)
            elif(((userNeed=="CREATE")&(exist==0))|((userNeed=="CHNGPWD")&(exist==1))):
                otrData = otpData(phoneNumber,userType,userNeed)
                otp = insertOtp(otrData)
                if otp==1:
                    x = json
                    x = {"Data":"Message sent"}
                    response = JsonResponse(x)
                    response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Something went wrong"}
                    response = JsonResponse(x)
                    response.status_code = 409
                    logger.warn(request)
            else:
                x = json
                x = {"Data":"Wrong parameters"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            rData = json
            rData = {"Data":"Not all variables are passed as needed"}
            response = JsonResponse(rData)
            response.status_code = 409
            logger.info(request)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def phoneLayer(request):
    try:
        rString = json.loads(request)
        phoneNumber = rString['phoneNumber']
        userType = rString['userType']
        if(userType=="CUST"):
            exist = validateCustPH(phoneNumber)
            if(exist==0):
                x = json
                x = {"Data":"Phone number available"}
                response = JsonResponse(x)
                response.status_code = 200
            else:
                x = json
                x = {"Data":"Phone number is taken"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="SHP"):
            exist = validateShpPH(phoneNumber)
            if(exist==0):
                x = json
                x = {"Data":"Phone number available"}
                response = JsonResponse(x)
                response.status_code = 200
            else:
                x = json
                x = {"Data":"Phone number is taken"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="DM"):
            exist = validateDmPH(phoneNumber)
            if(exist==0):
                x = json
                x = {"Data":"Phone number available"}
                response = JsonResponse(x)
                response.status_code = 200
            else:
                x = json
                x = {"Data":"Phone number is taken"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="ADM"):
            exist = validateAdmPH(phoneNumber)
            if(exist==0):
                x = json
                x = {"Data":"Phone number available"}
                response = JsonResponse(x)
                response.status_code = 200
            else:
                x = json
                x = {"Data":"Phone number is taken"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            rData = json
            rData = {"Data":"Not all variables are passed as needed"}
            response = JsonResponse(rData)
            response.status_code = 409
            logger.info(request)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def createUserLayer(request):
    try:
        rString = json.loads(request.body)
        userType = rString['userType']
        userNameLogin = rString['userNameLogin']
        password = rString['password']
        phoneNumber = rString['phoneNumber']
        otp = rString['otp']
        if(userType=="CUST"):
            custName = rString['custName']
            emailId = rString['emailId']
            r = validateOTP(phoneNumber,otp,userType,"SIGNUP")
            if(r==1):
                x = createCust(userNameLogin,password,phoneNumber,custName,emailId,userType)
                if(x==1):
                    userLData = userLoginData(userNameLogin,password)
                    response = validateClientLogin(userLData)
                else:
                    y = json
                    y = {"Data":"Something went wrong"}
                    response = JsonResponse(y)
                    response.status_code = 500
            else:
                x = json
                x = {"Data":"Incorrect otp"}
                response = JsonResponse(x)
                response.status_code = 409
        elif(userType=="SHP"):
            key = request.META['HTTP_BASICAUTHENTICATE']
            uId = validateKey(key)
            if(uId==0):
                x = json
                x = {"Data":"Incorrect session"}
                response = JsonResponse(x)
                response.status_code = 401
            else:
                userType1 = vlaidatesessiontype(uId)
                if(userType1=="ADM"):
                    admId = getAdmId(uId)
                    adminId = rString['typeId']
                    if(adminId==admId)&(admId!=0):
                        otp = rString['otp']
                        r = validateOTP(phoneNumber,otp,userType,"CREATE")
                        if(r==1):
                            shopName = rString['shopName']
                            phoneNumber = rString['phoneNumber']
                            lat = rString['lat']
                            lng = rString['lng']
                            description = rString['description']
                            deliveryRadius = rString['delRadius']
                            emailId = rString['emailId']
                            addressValue = rString['address']
                            minOrderValue = rString['minOrderValue']
                            delToken = rString['delToken']
                            delCharges = rString['delCharges']
                            catId = rString['catId']
                            delType = rString['delType']
                            if(delType == 1):
                                x = createShop(userNameLogin,password,userType,shopName,phoneNumber,lat,lng,description,deliveryRadius,emailId,addressValue, minOrderValue, delCharges, delToken, catId, delType)
                                if (x==1):
                                    y = json
                                    y = {"Data":"User created successfuly"}
                                    response = JsonResponse(y)
                                    response.status_code = 200
                                else:
                                    y = json
                                    y = {"Data":"Issue while creating a shop"}
                                    response = JsonResponse(y)
                                    response.status_code = 403
                            elif (delType == 2):
                                x = createShop(userNameLogin,password,userType,shopName,phoneNumber,lat,lng,description,deliveryRadius,emailId,addressValue, minOrderValue, delCharges, delToken, catId, delType)
                                if (x==1):
                                    shopId = validateShpPH(phoneNumber)
                                    doesExist = checkShopProperty(2, shopId)
                                    if(doesExist == 1):
                                        validateInsert = updateShopProperty("subscription", 2, 1, "SHP", shopId)
                                    else:
                                        validateInsert = insertShopProperty("subscription", 2, 1, "SHP", shopId)
                                    if(validateInsert==1):
                                        doesExist = checkIfExistSub(1, shopId)
                                        if(doesExist==0):
                                            createNewAddOn = insertSub(1, 0, shopId)
                                            if(createNewAddOn==1):
                                                y = json
                                                y = {"Data":"User created successfuly"}
                                                response = JsonResponse(y)
                                                response.status_code = 200
                                            else:
                                                x = json
                                                x = {"Data":"Issue while creating User of service type"}
                                                response= JsonResponse(x)
                                                response(x)
                                        else:
                                            x = json
                                            x = {"Data":"Subscription already exist"}
                                            response = JsonResponse(x)
                                            response.status_code = 409
                                    else:
                                        x = json
                                        x = {"Data":"Subscription already exist"}
                                        response = JsonResponse(x)
                                        response.status_code = 409
                                else:
                                    y = json
                                    y = {"Data":"Issue while creating a shop"}
                                    response = JsonResponse(y)
                                    response.status_code = 403
                            elif (delType == 3):
                                x = createShop(userNameLogin,password,userType,shopName,phoneNumber,lat,lng,description,deliveryRadius,emailId,addressValue, minOrderValue, delCharges, delToken, catId, delType)
                                if (x==1):
                                    shopId = validateShpPH(phoneNumber)
                                    doesExist = checkShopProperty(2, shopId)
                                    if(doesExist == 1):
                                        validateInsert = updateShopProperty("subscription", 2, 1, "SHP", shopId)
                                    else:
                                        validateInsert = insertShopProperty("subscription", 2, 1, "SHP", shopId)
                                    if(validateInsert==1):
                                        y = json
                                        y = {"Data":"User created successfuly"}
                                        response = JsonResponse(y)
                                        response.status_code = 200
                                    else:
                                        y = json
                                        y ={"Data":"Issue while creating the user, contact Admin"}
                                        response = JsonResponse(y)
                                        response.status_code = 409
                                else:
                                    y = json
                                    y = {"Data":"Issue while creating a shop"}
                                    response = JsonResponse(y)
                                    response.status_code = 403
                            else:
                                y = json
                                y = {"Data":"Incorrect shopp Type selected"}
                                response = JsonResponse(y)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect OTP value"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Incorrect session"}
                    response = JsonResponse(x)
                    response.status_code = 401
        elif(userType=="DM"):
            key = request.META['HTTP_BASICAUTHENTICATE']
            uId = validateKey(key)
            if(uId==0):
                x = json
                x = {"Data":"Incorrect session"}
                response = JsonResponse(x)
                response.status_code = 401
            else:
                userType1 = vlaidatesessiontype(uId)
                if(userType1=="SHP"):
                    shpId = getShpId(uId)
                    shopId = rString['typeId']
                    if(shpId==shopId)&(shpId!=0):
                        maxDelboyCount = evaluatePropertyShop("maxDelboyCount",shpId)
                        currentBoyCount = getCurrentDMCount(shpId)
                        if(currentBoyCount<maxDelboyCount):
                            otp = rString['otp']
                            r = validateOTP(phoneNumber,otp,userType,"CREATE")
                            if(r==1):
                                dmName = rString['dmName']
                                x = createDmuser(userNameLogin, password, userType, shopId, phoneNumber, dmName)
                                if(x==1):
                                    y = json
                                    y = {"Data":"DM created successfuly"}
                                    response = JsonResponse(y)
                                    response.status_code = 200
                                else:
                                    y = json
                                    y = {"Data":"Issue while creating DM"}
                                    response = JsonResponse(y)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Invalid otp passed"}
                                response = JsonResponse(x)
                                response.status_code = 403
                        else:
                            x = json
                            x = {"Data":"MAX del boy already present"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Invalid session parameters"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Invalid session parameters"}
                    response = JsonResponse(x)
                    response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid user type selected"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def signOutLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                r = sessionInactiver(key)
                if (r==1):
                    x = json
                    x = {"Data":"Success"}
                    response = JsonResponse(x)
                    response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Issue while logging out"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Invalid session"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def getListDM(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    shopId = rString['typeId']
                    if(shpId==shopId):
                        response = generateDMList(shopId)
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def getStatusShop(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    shopId = rString['typeId']
                    if(shpId==shopId):
                        statusStore = storeStatus(shopId)
                        if(statusStore=='N'):
                            x = json
                            x = {"Data":"Not able to locate the store"}
                            response = JsonResponse(x)
                            response.status_code = 409
                        else:
                            x = json
                            x = {"storeStatus":statusStore}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Not authorized"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def updateStatusShop(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    shopId = rString['typeId']
                    if(shpId==shopId):
                        storeStatusI = storeStatus(shopId)
                        updateStatusStore = rString['storeStatus']
                        if(storeStatusI=='N'):
                            x = json
                            x = {"Data":"Not able to locate the store"}
                            response = JsonResponse(x)
                            response.status_code = 409
                        elif(storeStatusI==1):
                            if(updateStatusStore==0):
                                storeUpdateStatus = updateStoreStatus(shopId, updateStatusStore)
                                if (storeUpdateStatus==1):
                                    x = json
                                    x = {"Data":"Update successful"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while updating the order status"}
                                    response = JsonResponse(x)
                                    response.status_code = 204
                            else:
                                x = json
                                x = {"Data":"Store is already active"}
                                response = JsonResponse(x)
                                response.status_code = 204
                        elif(storeStatusI==0):
                            if(updateStatusStore==1):
                                storeUpdateStatus = updateStoreStatus(shopId, updateStatusStore)
                                if (storeUpdateStatus==1):
                                    x = json
                                    x = {"Data":"Update successful"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while updating the order status"}
                                    response = JsonResponse(x)
                                    response.status_code = 204
                            else:
                                x = json
                                x = {"Data":"Store is already in-active"}
                                response = JsonResponse(x)
                                response.status_code = 204
                        else:
                            x = json
                            x = {"Data":"Incorrect store status contact admin"}
                            response = JsonResponse(x)
                            response.status_code = 500
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def generateShopList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        if(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="ADM"):
                    typeId1 = getAdmId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        response = getListShop()
                    else:
                        x = json
                        x = {"Data":"Incorrect typeId provided"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect userType"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Sorry not authorized"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def generateCustList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="ADM"):
                    typeId1 = getAdmId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        response = getListCust()
                    else:
                        x = json
                        x = {"Data":"Incorrect typeId provided"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect userType"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Sorry not authorized"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def uploadProfileImage(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        logger.info(uId)
        userId = int(request.POST['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((userType==request.POST['userType'])):
                if(userType=="ADM"):
                    typeId1 = getAdmId(uId)
                    typeId = int(request.POST['typeId'])
                    if(typeId1==typeId):
                        shopId = (request.POST['shopId'])
                        validShop = validShopById(shopId)
                        if(validShop==1):
                            response = saveImage(request.FILES['proPic'], shopId)
                        else:
                            x = json
                            x = {"Data":"Invalid store id"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Invalid type Id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Invalid user type selected"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Invalid user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response


def insertClientProperty(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        logger.info(uId)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((userType==rString['userType'])):
                if(userType=="ADM"):
                    typeId1 = getAdmId(uId)
                    typeId = int(rString['typeId'])
                    if(typeId1==typeId):
                        propertyName = rString['propertyName']
                        propertyDefault = rString['default']
                        forUserType = rString['forUserType']
                        propertExist = doesPropertyExist(propertyName, forUserType)
                        if(propertExist==0):
                            valideateInsert = insertPopertyValueMaster(propertyName, propertyDefault, forUserType)
                            if(valideateInsert==1):
                                x = json
                                x = {"Data":"Valude created"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Something went wrong"}
                                logger.WARN(request)
                                response = JsonResponse(x)
                                response.status_code = 500
                        else:
                            x = json
                            x = {"Data":"Property value already present"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect typeId passed"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect userType passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def insertClientShopProperty(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        logger.info(uId)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((userType==rString['userType'])):
                if(userType=="ADM"):
                    typeId1 = getAdmId(uId)
                    typeId = int(rString['typeId'])
                    if(typeId1==typeId):
                        propertyName = rString['propertyName']
                        propertyValue = rString['propertyValue']
                        propertyId = rString['propertyId']
                        forUserType = rString['forUserType']
                        validateInsertion = isValidInsertProperty(propertyName, forUserType, propertyId)
                        if(validateInsertion==1):
                            if(forUserType=="SHP"):
                                shopId = rString['shopId']
                                validShop = validShopById(shopId)
                                if(validShop==1):
                                    doesExist = checkShopProperty(propertyId, shopId)
                                    if(doesExist == 1):
                                        validateInsert = updateShopProperty(propertyName, propertyId, propertyValue, forUserType, shopId)
                                    else:
                                        validateInsert = insertShopProperty(propertyName, propertyId, propertyValue, forUserType, shopId)
                                    if(validateInsert==1):
                                        x = json
                                        x = {"Data":"Property set"}
                                        response = JsonResponse(x)
                                        response.status_code = 200
                                    else:
                                        x = json
                                        x = {"Data":"Issue while creating property"}
                                        response = JsonResponse(x)
                                        response.status_code = 409
                                else:
                                    x = json
                                    x = {"Data":"Incorrect shop selected"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Incorrect userType selected"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect property settings"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect typeId passed"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect userType passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def getRequestedShopProperty(request):
    try:
        rString = json.loads(request.body)
        shopId = rString['shopId']
        validShop = validShopById(shopId)
        if(validShop==1):
            response = generateShopPropertyList(shopId)
        else:
            x = json
            x = {"Data":"Not valid shop"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def markStoreActive(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        logger.info(uId)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((userType==rString['userType'])):
                if(userType=="ADM"):
                    typeId1 = getAdmId(uId)
                    typeId = int(rString['typeId'])
                    if(typeId1==typeId):
                        isActive = rString['isActive']
                        shopId = rString['shopId']
                        isValidShop = validShopById(shopId)
                        if(isValidShop==1):
                            isValidUpdate = updateStoreActive(shopId, isActive)
                            if(isValidUpdate==1):
                                x = json
                                x = {"Data":"Store marked successfully"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Sorry not able to mark the store"}
                                response = JsonResponse(x)
                                response.status_code=409
                        else:
                            x = json
                            x = {"Data":"Sorry not valid store id"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect typeId passed"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect userType passed"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect userId passed"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def validateUserPhone(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if(userType=="CUST" and userType==rString['userType']):
                custId = getCustId(uId)
                if(custId==rString['typeId']):
                    phoneNumber = rString['phoneNumber']
                    isValidPhone = validateUserPhoneCust(custId, phoneNumber)
                    if(isValidPhone==1):
                        x = json
                        x = {"Data":"true"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"false"}
                        response = JsonResponse(x)
                        response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif(userType=="SHP" and userType==rString['userType']):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    phoneNumber = rString['phoneNumber']
                    isValidPhone = validateShopPhoneCust(shpId, phoneNumber)
                    if(isValidPhone==1):
                        x = json
                        x = {"Data":"true"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"false"}
                        response = JsonResponse(x)
                        response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Incorrect type Id"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif(userType=="ADM" and userType==rString['userType']):
                admId = getAdmId(uId)
                if(admId==rString['typeId']):
                    phoneNumber = rString['phoneNumber']
                    isValidPhone = validateAdmPhoneCust(admId, phoneNumber)
                    if(isValidPhone==1):
                        x = json
                        x = {"Data":"true"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"false"}
                        response = JsonResponse(x)
                        response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Incorrect type Id"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif(userType=="DM" and userType==rString['userType']):
                admId = getDMId(userId)
                if(admId==rString['typeId']):
                    phoneNumber = rString['phoneNumber']
                    isValidPhone = validateDmPhoneCust(admId, phoneNumber)
                    if(isValidPhone==1):
                        x = json
                        x = {"Data":"true"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"false"}
                        response = JsonResponse(x)
                        response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Incorrect type Id"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect userId"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def validateUserOTP(request):
    try:
        rString = json.loads(request.body)
        phoneNumber = rString['phoneNumber']
        userType = rString['userType']
        otp = rString['otp']
        ifValidOtp = validateChngOtp(phoneNumber, userType, otp)
        if(ifValidOtp==1):
            x = json
            x = {"Data":"true"}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"false"}
            response = JsonResponse(x)
            response.status_code = 200
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def chngPassword(request):
    try:
        rString = json.loads(request.body)
        phoneNumber = rString['phoneNumber']
        userType = rString['userType']
        otp = rString['otp']
        ifValidOtp = validateChngOtp2(phoneNumber, userType, otp)
        if(ifValidOtp==1):
            password = rString['password']
            if(userType=="CUST"):
                get = custUid(phoneNumber)
                if(get>0):
                    updatePass = updatingPass(get, password)
                    if(updatePass==1):
                        x = json
                        x = {"Data":"Update successful"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Update unsuccess"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect phone"}
                    response = JsonResponse(x)
                    response.status_code = 409
            elif(userType=="SHP"):
                get = shopId(phoneNumber)
                if(get>0):
                    updatePass = updatingPass(get, password)
                    if(updatePass==1):
                        x = json
                        x = {"Data":"Update successful"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Update unsuccess"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect phone"}
                    response = JsonResponse(x)
                    response.status_code = 409
            elif(userType=="ADM"):
                get = admId(phoneNumber)
                if(get>0):
                    updatePass = updatingPass(get, password)
                    if(updatePass==1):
                        x = json
                        x = {"Data":"Update successful"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Update unsuccess"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect phone"}
                    response = JsonResponse(x)
                    response.status_code = 409
            elif(userType=="DM"):
                get = dmId(phoneNumber)
                if(get>0):
                    updatePass = updatingPass(get, password)
                    if(updatePass==1):
                        x = json
                        x = {"Data":"Update successful"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Update unsuccess"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect phone"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"false"}
            response = JsonResponse(x)
            response.status_code = 200
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def addToFavor(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if(userType=="CUST" and userType==rString['userType']):
                custId = getCustId(uId)
                if(custId==rString['typeId']):
                    shopId = rString['shopId']
                    isValidShop = validShopById(shopId)
                    if(isValidShop==1):
                        doesExist = isExistingFav(shopId, custId)
                        if(doesExist==1):
                            x = json
                            x = {"Data":"Shop Already fav"}
                            response = JsonResponse(x)
                            response.status_code = 409
                        else:
                            addFav = addingFav(shopId, custId)
                            if(addFav == 1):
                                x = json
                                x = {"Data":"Shop added to fav"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while adding to fav"}
                                response = JsonResponse(x)
                                response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect shop selected"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code  = 401
        else:
            x = json
            x = {"Data":"Incorrect session passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def viewingFav(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if(userType=="CUST" and userType==rString['userType']):
                custId = getCustId(uId)
                if(custId==rString['typeId']):
                    response = generateFinalList(custId)
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code  = 401
        else:
            x = json
            x = {"Data":"Incorrect session passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def deteFav(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if(userType=="CUST" and userType==rString['userType']):
                custId = getCustId(uId)
                if(custId==rString['typeId']):
                    shopId = rString['shopId']
                    isValidShop = validShopById(shopId)
                    if(isValidShop==1):
                        doesExist = isExistingFav(shopId, custId)
                        if(doesExist==1):
                            deleteFav = validDeleteFav(shopId,custId)
                            if(deleteFav==1):
                                x = json
                                x = {"Data":"Removed from fav"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while removing fav"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Shop is not fav"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect shop selected"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Incorrect typeId passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect userType passed"}
                response = JsonResponse(x)
                response.status_code  = 401
        else:
            x = json
            x = {"Data":"Incorrect session passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def makingQuery(request):
    try:
        rString = json.loads(request.body)
        userName = rString['userName']
        userType = rString['userType']
        message = rString['message']
        phoneNumber = rString['phoneNumber']
        address = rString['address']
        emailId = rString['email']
        createContact = creatingContact(userName,userType,message,phoneNumber,address,emailId)
        if createContact == 1:
            x = json
            x = {"Data":"Form filled"}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Error"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def viewingQuery(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((rString['userType']==userType)and(userType=="ADM")):
                admId = getAdmId(uId)
                if admId == rString['typeId']:
                    response = generateContactFormDetails()
                else:
                    x = json
                    x = {"Data":"Invalid type Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Not Authorized"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def insertNewToken(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if(rString['userType']==userType):
                if(userType=="SHP"):
                    typeId = getShpId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], userId)
                        if(isValidToken==0):
                            createToken = InsertNewUserToken(rString['token'], userId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token created"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token already exist"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="CUST"):
                    typeId = getCustId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], userId)
                        if(isValidToken==0):
                            createToken = InsertNewUserToken(rString['token'], userId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token created"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token already exist"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    typeId = getAdmId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], userId)
                        if(isValidToken==0):
                            createToken = InsertNewUserToken(rString['token'], userId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token created"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token already exist"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="DM"):
                    typeId = getDMId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], userId)
                        if(isValidToken==0):
                            createToken = InsertNewUserToken(rString['token'], userId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token created"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token already exist"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
            else:
                x = json
                x = {"Data":"Not Authorized"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def newUserToken(request):
    try:
        rString = json.loads(request.body)
        token = rString['token']
        userType = rString['userType']
        if(userType=='CUST' or userType=='SHP'):
            if(insertNewTokenClient(token,userType)==1):
                x = json
                x = {"Data":"Inserted successfully"}
                response = JsonResponse(x)
                response.status_code = 200
            else:
                x = json
                x = {"Data":"Inserted successfully"}
                response = JsonResponse(x)
                response.status_code = 409
        else:
            x = json
            x = {"Data":"Incorrect type selected"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def deleteOldToken(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if uId==0:
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        if(uId==rString['userId']):
            userType = vlaidatesessiontype(uId)
            if(rString['userType']==userType):
                if(userType=="SHP"):
                    typeId = getShpId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], uId)
                        if(isValidToken==1):
                            createToken = updateUserToken(rString['token'], uId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token removed"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token not present"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="CUST"):
                    typeId = getCustId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], uId)
                        if(isValidToken==1):
                            createToken = updateUserToken(rString['token'], uId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token removed"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token not present"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    typeId = getAdmId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], uId)
                        if(isValidToken==1):
                            createToken = updateUserToken(rString['token'], uId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token removed"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token not present"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="DM"):
                    typeId = getDMId(uId)
                    if(typeId==rString['typeId']):
                        isValidToken = getValidToken(rString['token'], uId)
                        if(isValidToken==1):
                            createToken = updateUserToken(rString['token'], uId)
                            if(createToken==1):
                                x = json
                                x = {"Data":"Token removed"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while creating token"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Token not present"}
                            response = JsonResponse(x)
                            response.status_code = 200
                    else:
                        print "YAHA2"
                        x = json
                        x = {"Data":"Invalid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
            else:
                print "YAHA1"
                x = json
                x = {"Data":"Not Authorized"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            print "YAHA"
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def sendFinalNotification(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((rString['userType']==userType)and(userType=="ADM")):
                admId = getAdmId(uId)
                if admId == rString['typeId']:
                    body = rString['msgBody']
                    title = rString['title']
                    sendTo = rString['sendTo']
                    if(sendTo=="all"):
                        sendAllRegNotification(body, title)
                        x = json
                        x = {"Data":"Sent to all users"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    elif(sendTo=="SHP"):
                        sendToAllShops(body, title)
                        x = json
                        x = {"Data":"Sent to all shops"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    elif(sendTo=="CUST"):
                        sendToAllCust(body, title)
                        x = json
                        x = {"Data":"Sent to all customers"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    elif(sendTo=="ADM"):
                        sendAdminNotif(body, title)
                        x = json
                        x = {"Data":"Sent to all admins"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    elif(sendTo=="DM"):
                        sendToAllDm(body, title)
                        x = json
                        x = {"Data":"Sent to all DM"}
                        response = JsonResponse(x)
                        response.status_code = 200
                    else:
                        load = sendToUser(sendTo, body, title)
                        if load==1:
                            x = json
                            x = {"Data":"Sent to user"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Sorry Invalid type"}
                            response = JsonResponse(x)
                            response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Invalid type Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Not Authorized"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def generateReviewList(request):
    try:
        rString = json.loads(request.body)
        shopId = rString['shopId']
        response = reviewList(shopId)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def generateReviewDetails(request):
    try:
        rString = json.loads(request.body)
        reviewListId = rString['reviewIdList']
        response = reviewDetails(reviewListId)
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response

def validateShopProperty(request):
    try:
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        rString = json.loads(request.body)
        userId = int(rString['userId'])
        if(uId==userId):
            userType = vlaidatesessiontype(uId)
            if((rString['userType']==userType)and(userType=="SHP")):
                shopId = getShpId(uId)
                if shopId == rString['typeId']:
                    propertyName = rString['propertyName']
                    if(propertyName=="maxPromoCount"):
                        maxDelboyCount = int(evaluatePropertyShop("maxPromoCount",shopId))
                        currentBoyCount = int(getMaxPromoCount(shopId))
                        if(currentBoyCount<maxDelboyCount):
                            x = json
                            x = {"Data":"Can proceed"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Can't proceed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    elif(propertyName=="maxDelboyCount"):
                        maxDelboyCount = int(evaluatePropertyShop("maxDelboyCount",shopId))
                        currentBoyCount = int(getCurrentDMCount(shopId))
                        print maxDelboyCount
                        if(currentBoyCount<maxDelboyCount):
                            x = json
                            x = {"Data":"Can proceed"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Can't proceed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Invalid use"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Not authorized to view this section"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Not authorized to view this section"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Not authorized to view this section"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
     #check when a value is passed with a variable
    except ValueError :
         x = json
         x = {"Data":"Not all variable values passed"}
         response = JsonResponse(x)
         response.status_code = 404
         logger.exception(request)
    return response
