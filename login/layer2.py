# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.http import JsonResponse
from django.db import connection
from datetime import datetime,timedelta
import uuid
import logging
from random import randint
import boto3
from botocore.client import Config
from layer3 import sendOtp,sendNotification

def generate_n_otp(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/login.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

class insertSessionObject():
    sessionKey = ""
    createdOnDate = ""
    userId = ""
    typeId =""
    userType =""
    userName =""
    def __init__(self,key,createdOnDate,userId,typeId,userType,userName):
        self.sessionKey = key
        self.createdOnDate = createdOnDate
        self.userId = userId
        self.typeId = typeId
        self.userType = userType
        self.userName = userName

def validateClientLogin(self):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_loginmaster where userNameLogin = %s and password =%s and userType='CUST' and isActiveFl=1;",[self.uName,self.password])
        row = cursor.fetchone()
        if row is None:
            with connection.cursor() as cursorM:
                row_count = cursorM.execute("SELECT uId_id from login_customermaster where phoneNumber=%s",[self.uName])
                if(row_count>0):
                    rowU = cursorM.fetchone()
                    with connection.cursor() as cursorL:
                        row_countL = cursorL.execute("SELECT * from login_loginmaster where uId=%s and password=%s and userType='CUST' and isActiveFl=1",[rowU[0],self.password])
                        if(row_countL>0):
                            rowL = cursorL.fetchone()
                            userId=rowL[3]
                            cursor.execute("SELECT userType from login_loginmaster where uId = %s",[userId])
                            uType = cursor.fetchone()
                            cursor.execute("SELECT custId, custName, phoneNumber, emailId from login_customermaster where uId_id = %s",[userId])
                            uDetails = cursor.fetchone()
                            key = uuid.uuid4().hex
                            isTrue = validateKey(key)
                            #validating if key value ie unique
                            while (isTrue!=0):
                                key = uuid.uuid4().hex
                                isTrue = validateKey(key)
                            createdOnDate =  datetime.now()
                            sessionObject = insertSessionObject(key,createdOnDate,userId,uDetails[0],uType[0],uDetails[1])
                            sessionValue = insertSession(sessionObject)
                            while (sessionValue!=1):
                                sessionValue= insertSessionObject(sessionObject)
                            rData = json
                            sUserId = (sessionObject.userId)
                            sTypeId = (sessionObject.typeId)
                            rData = {"userId":sUserId,"basicAuthenticate":sessionObject.sessionKey,"createdOnDate":sessionObject.createdOnDate,"typeId":sTypeId,"userType":sessionObject.userType,"userName":sessionObject.userName,"emailId":uDetails[3],"phoneNumber":uDetails[2]}
                            response = JsonResponse(rData)
                            response.status_code = 200
                            response['basicAuthenticate'] = key
                            logger.info('Logged in for:'+(str(sessionObject.userId)))
                        else:
                            response = JsonResponse({"Data":"Incorrect username"})
                            response.status_code = 401
                            logger.info('login failed:'+self.uName)
                else:
                    response = JsonResponse({"Data":"Incorrect username"})
                    response.status_code = 401
                    logger.info('login failed:'+self.uName)
        else:
            userId=row[3]
            cursor.execute("SELECT userType from login_loginmaster where uId = %s",[userId])
            uType = cursor.fetchone()
            cursor.execute("SELECT custId, custName, phoneNumber, emailId from login_customermaster where uId_id = %s",[userId])
            uDetails = cursor.fetchone()
            key = uuid.uuid4().hex
            isTrue = validateKey(key)
            #validating if key value ie unique
            while (isTrue!=0):
                key = uuid.uuid4().hex
                isTrue = validateKey(key)
            createdOnDate =  datetime.now()
            sessionObject = insertSessionObject(key,createdOnDate,userId,uDetails[0],uType[0],uDetails[1])
            sessionValue = insertSession(sessionObject)
            while (sessionValue!=1):
                sessionValue= insertSessionObject(sessionObject)
            rData = json
            sUserId = (sessionObject.userId)
            sTypeId = (sessionObject.typeId)
            rData = {"userId":sUserId,"basicAuthenticate":sessionObject.sessionKey,"createdOnDate":sessionObject.createdOnDate,"typeId":sTypeId,"userType":sessionObject.userType,"userName":sessionObject.userName,"phoneNumber":uDetails[2]}
            response = JsonResponse(rData)
            response.status_code = 200
            response['basicAuthenticate'] = key
            logger.info('Logged in for:'+(str(sessionObject.userId)))
    return response

def validateAdminLogin(self, userType):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_loginmaster where userNameLogin = %s and password =%s and userType=%s;",[self.uName,self.password,userType])
        row = cursor.fetchone()
        if row is None:
            response = JsonResponse({"Data":"Incorrect username"})
            response.status_code = 401
            logger.info('login failed:'+self.uName)
        elif(row[2]=="CUST"):
            response = JsonResponse({"Data":"You are unauthorized to use this"})
            response.status_code = 401
            logger.info('login failed:'+self.uName)
        else:
            userId=row[3]
            if(row[2]=="ADM"):
                cursor.execute("SELECT adminId, adminName, phoneNumber from login_adminmaster where uId_id = %s",[userId])
                uDetails = cursor.fetchone()
            elif(row[2]=="SHP"):
                cursor.execute("SELECT shopId, shopName, phoneNumber from login_shopmaster where uId_id = %s",[userId])
                uDetails = cursor.fetchone()
            elif(row[2]=="DM"):
                cursor.execute("SELECT dmId, dmName, phoneNumber from login_delivermedmaster where uId_id = %s",[userId])
                uDetails = cursor.fetchone()
            key = uuid.uuid4().hex
            isTrue = validateKey(key)
            #validating if key value ie unique
            while (isTrue!=0):
                key = uuid.uuid4().hex
                isTrue = validateKey(key)
            createdOnDate =  datetime.now()
            sessionObject = insertSessionObject(key,createdOnDate,userId,uDetails[0],row[2],uDetails[1])
            sessionValue = insertSession(sessionObject)
            while (sessionValue!=1):
                sessionValue= insertSessionObject(sessionObject)
            rData = json
            sUserId = (sessionObject.userId)
            sTypeId = (sessionObject.typeId)
            rData = {"userId":sUserId,"basicAuthenticate":sessionObject.sessionKey,"createdOnDate":sessionObject.createdOnDate,"typeId":sTypeId,"userType":sessionObject.userType,"userName":sessionObject.userName,"phoneNumber":uDetails[2]}
            response = JsonResponse(rData)
            response.status_code = 200
            response['basicAuthenticate'] = key
            logger.info('Logged in for:'+str(sessionObject.userId))
        return response

def validateKey(key):
    with connection.cursor() as cursor:
        cursor.execute("SELECT uId_id from login_sessionmaster where basicAuthenticate=%s and isActiveFl=1",[key])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def validateKeyOld(key):
    with connection.cursor() as cursor:
        cursor.execute("SELECT uId_id from login_sessionmaster where basicAuthenticate=%s and isActiveFl=0",[key])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def insertSession(self):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO login_sessionmaster (basicAuthenticate, loginTime, deviceType, userType, isActiveFl, uId_id) VALUES (%s, %s, %s, %s, '1', %s);",[self.sessionKey, self.createdOnDate, self.typeId, self.userType, self.userId])
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def vlaidatesessiontype(userId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT userType from login_loginmaster where uId=%s",[userId])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def getCustId(userId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT custId from login_customermaster where uId_id=%s",[userId])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def getDMId(userId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT dmId from login_delivermedmaster where uId_id=%s",[userId])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def getAdmId(userId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT adminId from login_adminmaster where uId_id=%s",[userId])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def getShpId(userId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT shopId from login_shopmaster where uId_id=%s",[userId])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def validateClientExistence(uName):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_loginmaster where userNameLogin=%s",[uName])
        isTrue = cursor.fetchone()
        if isTrue is None:
            x = json
            x = {"Data":"User doesn't exist"}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"User exist","userType":isTrue[2]}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def validateCustPH(phoneNumber):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_customermaster where phoneNumber=%s",[phoneNumber])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return 1

def validateShpPH(phoneNumber):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_shopmaster where phoneNumber=%s",[phoneNumber])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return isTrue[0]

def validateDmPH(phoneNumber):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_delivermedmaster where phoneNumber=%s",[phoneNumber])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return 1

def validateAdmPH(phoneNumber):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_adminmaster where phoneNumber=%s",[phoneNumber])
        isTrue = cursor.fetchone()
        if isTrue is None:
            return 0
        else:
            return 1

def insertOtp(self):
    createdOnDate =  datetime.now()
    otpDelta = timedelta(days=1)
    endDate = createdOnDate + otpDelta
    otp = generate_n_otp(6)
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO login_otpmaster (phoneNumber, otp, purpose, userType, isUsed, expiryTime) VALUES (%s, %s, %s, %s, '0', %s);",[self.phoneNumber, otp, self.userNeed, self.userType, endDate])
            connection.commit()
            sendOtp(self.phoneNumber, otp, self.userNeed)
            return 1
        except:
            connection.rollback()
            logger.exception(self)
            return 0

def validateOTP(phoneNumber,otp,userType,userNeed):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from login_otpmaster where phoneNumber=%s and otp=%s and userType=%s and purpose=%s",[phoneNumber,otp,userType,userNeed])
        currentDate = datetime.now()
        row = cursor.fetchone()
        logger.info(row)
        if row is not None:
            if ((currentDate<row[5])&(row[4]==0)):
                try:
                    cursor.execute("UPDATE login_otpmaster SET isUsed=1 WHERE otpId=%s;",[row[0]])
                    connection.commit()
                    return 1
                except:
                    connection.rollback()
                    logger.warn("Issue while proccesing otp request for"+phoneNumber)
                    return 0
            else:
                return 0
        else:
            return 0

def createCust(userNameLogin,password,phoneNumber,custName,emailId,userType):
    x = insertLoginMaster(userNameLogin,password,userType)
    if(x==1):
        with connection.cursor() as cursor:
            cursor.execute("SELECT uId from login_loginmaster where userNameLogin=%s",[userNameLogin])
            row = cursor.fetchone()
            if row is None:
                return 0
            else:
                return insertCustDetails(row[0], custName, phoneNumber, emailId)

def insertCustDetails(uId,custName,phoneNumber,emailId):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO login_customermaster(custName, phoneNumber, uId_id, emailId, favourite)VALUES(%s,%s,%s,%s,%s)",[custName,phoneNumber,uId,emailId,""])
            connection.commit()
            return 1
        except:
            logger.exception("Issue while inserting user details")
            return 0

def insertLoginMaster(userNameLogin,password,userType):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO login_loginmaster(userNameLogin, password, userType, isActiveFl)VALUES(%s,%s,%s,'1')",[userNameLogin,password,userType])
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.exception("Issue while inserting new user in user master")
            return 0

def createShop(userNameLogin,password,userType,shopName,phoneNumber,lat,lng,description,deliveryRadius,emailId,addressValue,minOrderValue, delCharges, delToken, catId, delType):
    x = insertLoginMaster(userNameLogin,password,userType)
    if(x==1):
        with connection.cursor() as cursor:
            cursor.execute("SELECT uId from login_loginmaster where userNameLogin=%s",[userNameLogin])
            row = cursor.fetchone()
            if row is None:
                return 0
            else:
                return insertShopDetails(row[0], shopName, phoneNumber, emailId, description, lat, lng, deliveryRadius, addressValue, minOrderValue, delCharges, delToken, catId, delType)

def insertShopDetails(uId, shopName, phoneNumber, emailId, description, lat, lng, deliveryRadius, addressValue, minOrderValue, delCharges, delToken, catId, delType):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO login_shopmaster(shopName, phoneNumber, isOpen, sLat, sLng, description, isActiveFl, isSubscription, isDailyNeeds, uId_id, delRadius, emailId, address, minOrderValue, delCharges, delToken, catId, imageUploaded, subCatId, isVerified, rating, totalRater,totalOrders)VALUES(%s,%s,'0',%s,%s,%s,'1','0',%s,%s,%s,%s,%s,%s,%s,%s,%s,'0',%s,'0','0','0','0')",[shopName, phoneNumber, lat, lng, description, delType,uId, deliveryRadius, emailId, addressValue, minOrderValue, delCharges, delToken, catId, ""])
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.exception("Issue while inserting shop details")
            return 0

def createDmuser(userNameLogin, password, userType, shopId, phoneNumber, dmName):
    x = insertLoginMaster(userNameLogin,password,userType)
    if(x==1):
        with connection.cursor() as cursor:
            cursor.execute("SELECT uId from login_loginmaster where userNameLogin=%s",[userNameLogin])
            row = cursor.fetchone()
            if row is None:
                return 0
            else:
                return insertDmDetails(row[0], shopId, dmName, phoneNumber)

def insertDmDetails(uId, shopId, dmName, phoneNumber):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO login_delivermedmaster (dmName, dmLat, dmLng, withOrders, isActiveFl, shopId_id, uId_id, phoneNumber) VALUES(%s, '0', '0', '0', '1', %s, %s, %s)",[dmName, shopId, uId, phoneNumber])
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.exception("HELO")
            return 0

def sessionInactiver(key):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE login_sessionmaster SET isActiveFl=0 WHERE basicAuthenticate=%s;",[key])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            logger.warn("Issue while proccesing otp request for"+key)
            return 0

def generateDMList(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_delivermedmaster where shopId_id=%s and isActiveFl=1",[shopId])
        if(row_count>0):
            row = cursor.fetchone()
            dmList = []
            count = 0
            while row is not None:
                dmList.append({
                    "dmId":row[0],
                    "dmName":row[2],
                    "dmPhone":row[1]
                })
                count = count + 1
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['dmList']=dmList
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No dm available","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def storeStatus(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT isOpen from login_shopmaster where shopId=%s",[shopId])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 'N'

def updateStoreStatus(shopId, updateStoreStatus):
    with connection.cursor() as cursor:
        try:
            cursor.execute("UPDATE login_shopmaster SET isOpen=%s where shopId=%s and isVerified=1",[updateStoreStatus,shopId])
            connection.commit()
            return 1
        except:
            logger.exception("YAHA")
            connection.rollback()
            return 0

def getListShop():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_shopmaster where isActiveFl=1")
        if(row_count>0):
            row = cursor.fetchone()
            shoplist = []
            count = 0
            while row is not None:
                shoplist.append({
                    "shopId":row[0],
                    "shopName":row[1],
                    "shopPhoneNumber":row[2],
                    "shopIsOpen":row[3],
                    "shopLat":row[4],
                    "shopLng":row[5],
                    "shopDescription":row[6],
                    "deliveryRadius":row[10],
                    "shopEmail":row[11],
                    "shopAddress":row[12],
                    "minOrderValue":row[14],
                    "delToken":row[15],
                    "delCharges":row[13],
                    "catId":row[16],
                    "imageUploaded":row[17],
                    "subCatId":row[18],
                    "isVerified":row[19]
                })
                count = count + 1
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['shopList']=shoplist
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No active shops","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def getListCust():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_customermaster")
        if(row_count>0):
            row = cursor.fetchone()
            shoplist = []
            count = 0
            while row is not None:
                shoplist.append({
                    "custId":row[0],
                    "custName":row[1],
                    "custPhoneNumber":row[2],
                    "custEmail":row[3]
                })
                count = count + 1
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['custList']=shoplist
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No active shops","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def validateCatId(catId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_cateogaryshops where catId=%s",[catId])
        if(row_count>0):
            return 1
        else:
            return 0

def saveImage(imageFile, shopId):
    ACCESS_KEY_ID = 'AKIAI76WOXG7MZ6IT3YQ'
    ACCESS_SECRET_KEY = '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'
    BUCKET_NAME = 'martjinni'
    FILE_NAME = shopId+".jpeg"
    s3 = boto3.resource(
        's3',
        aws_access_key_id=ACCESS_KEY_ID,
        aws_secret_access_key=ACCESS_SECRET_KEY,
        config=Config(signature_version='s3v4')
    )
    s3.Bucket(BUCKET_NAME).put_object(Key=FILE_NAME, Body=imageFile, ACL='public-read')
    xShop = int(shopId)
    y = 0
    with connection.cursor() as cursor:
        cursor.execute("UPDATE login_shopmaster SET imageUploaded='1' where shopId=%s",[xShop])
        try:
            connection.commit()
            y = 1
        except:
            y = 0
            connection.rollback
    if(y==1):
        x = json
        x = {"Data":"Image uploaded"}
        response = JsonResponse(x)
        response.status_code = 200
    else:
        x = json
        x = {"Data":"Issue while uploading image"}
        response = JsonResponse(x)
        response.status_code = 409
    return response

def validShopById(shopId):
    with connection.cursor() as cursor:
        xShop = int(shopId)
        row_count = cursor.execute("SELECT * from login_shopmaster where shopId=%s",[xShop])
        if(row_count>0):
            return 1
        else:
            return 0

def insertPopertyValueMaster(propertyName, propertyDefault, forUserType):
    with connection.cursor() as cursor:
        cursor.execute("INSERT into login_clientpropertiesmaster (cpName, userType, propertyDefault) values (%s, %s, %s) ",[propertyName, forUserType, propertyDefault])
        try:
            connection.commit()
            return 1
        except:
            logger.exception("YAHA")
            connection.rollback()
            return 0

def isValidInsertProperty(propertyName, forUserType, propertyId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_clientpropertiesmaster where cpName=%s and cpId=%s and userType=%s",[propertyName, propertyId, forUserType])
        if(row_count==1):
            return 1
        else:
            return 0

def insertShopProperty(propertyName, propertyId, propertyValue, forUserType, shopId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT into login_clientpropertiesvaluesshops (cpName, cpId_id, userType, typeId_id, cpValue) VALUES (%s, %s, %s, %s, %s)",[propertyName, propertyId, forUserType, shopId, propertyValue])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updateShopProperty (propertyName, propertyId, propertyValue, forUserType, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT id from login_clientpropertiesvaluesshops where cpId_id = %s and typeId_id = %s and cpName=%s and userType=%s",[propertyId, shopId, propertyName, forUserType])
        if(row_count==1):
            row = cursor.fetchone()
            shopPropId = row[0]
            with connection.cursor() as cursorM:
                cursorM.execute("UPDATE login_clientpropertiesvaluesshops SET cpValue = %s where id=%s", [propertyValue,shopPropId])
                try:
                    connection.commit()
                    return 1
                except:
                    connection.rollback()
                    return 0
        else:
            return 0

def checkShopProperty(propertyId, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_clientpropertiesvaluesshops where cpId_id=%s and typeId_id=%s",[propertyId, shopId])
        if(row_count>0):
            return 1
        else:
            return 0

def doesPropertyExist(propertyName, forUserType):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_clientpropertiesmaster where cpName=%s and userType=%s",[propertyName, forUserType])
        if(row_count==1):
            return 1
        else:
            return 0

def generateShopPropertyList(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_clientpropertiesmaster where userType=%s",["SHP"])
        if(row_count>0):
            container = []
            row = cursor.fetchone()
            logger.info("Yahi hai")
            while row is not None:
                with connection.cursor() as cursorM:
                    propId = row[0]
                    row_countM = cursorM.execute("SELECT cpValue from login_clientpropertiesvaluesshops where cpId_id=%s and typeId_id=%s",[propId,shopId])
                    if(row_countM==1):
                        rowM = cursorM.fetchone()
                        container.append({
                            "propertyName":row[1],
                            "propertyValue":rowM[0],
                            "propertyId":row[0]
                        })
                    else:
                        container.append({
                            "propertyName":row[1],
                            "propertyValue":row[3],
                            "propertyId":row[0]
                        })
                row = cursor.fetchone()
            resObj = {}
            resObj['data'] = container
            response = JsonResponse(resObj)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No information available"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def evaluatePropertyShop(propertyName, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT cpValue from login_clientpropertiesvaluesshops where cpName=%s and typeId_id=%s",[propertyName, shopId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            row_count1 = cursor.execute("SELECT propertyDefault from login_clientpropertiesmaster where cpName=%s and userType=%s",[propertyName,"SHP"])
            if(row_count1==1):
                row = cursor.fetchone()
                return row[0]
            else:
                return 0

def updateStoreActive(shopId, isActive):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE login_shopmaster set isVerified=%s where shopId=%s",[isActive,shopId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def validateUserPhoneCust(custId, phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_customermaster where custId=%s and phoneNumber=%s",[custId, phoneNumber])
        if(row_count==1):
            return 1
        else:
            return 0

def validateShopPhoneCust(custId, phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_shopmaster where shopId=%s and phoneNumber=%s",[custId, phoneNumber])
        if(row_count==1):
            return 1
        else:
            return 0

def validateAdmPhoneCust(admId, phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_adminmaster where adminId = %s and phoneNumber=%s",[admId, phoneNumber])
        if(row_count==1):
            return 1
        else:
            return 0

def validateDmPhoneCust(admId, phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_delivermedmaster where dmId=%s and phoneNumber=%s",[admId,phoneNumber])
        if(row_count==1):
            return 1
        else:
            return 0

def validateChngOtp(phoneNumber, userType, otp):
    with connection.cursor() as cursor:
        createdOnDate =  datetime.now()
        row_count = cursor.execute("SELECT expiryTime from login_otpmaster where otp=%s and phoneNumber=%s and userType=%s and purpose=%s and expiryTime>%s and isUsed=0",[otp, phoneNumber, userType, "CHNGPWD", createdOnDate])
        if(row_count==1):
            return 1
        else:
            return 0

def validateChngOtp2(phoneNumber, userType, otp):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_otpmaster where otp=%s and phoneNumber=%s and userType=%s and purpose=%s and isUsed=0",[otp, phoneNumber, userType, "CHNGPWD"])
        if(row_count==1):
            row = cursor.fetchone()
            cursor.execute("UPDATE login_otpmaster set isUsed=1 where otpId=%s",[row[0]])
            try:
                connection.commit()
                return 1
            except:
                connection.rollback()
                return 0
        else:
            return 0

def custUid(phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT uId_id from login_customermaster where phoneNumber=%s",[phoneNumber])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def shopId(phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT uId_id from login_shopmaster where phoneNumber=%s",[phoneNumber])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def admId(phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT uId_id from login_adminmaster where phoneNumber=%s",[phoneNumber])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def dmId(phoneNumber):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT uId_id from login_delivermedmaster where phoneNumber=%s",[phoneNumber])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def updatingPass(uid, password):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE login_loginmaster set password=%s where uId=%s",[password, uid])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def isExistingFav(shopId, custId):
    shopId = str(shopId)
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT favourite from login_customermaster where custId=%s",[custId])
        if(row_count==1):
            row = cursor.fetchone()
            if row[0] is "":
                return 0
            else:
                favList = row[0].split(",")
                if shopId in favList:
                    return 1
                else:
                    return 0
        else:
            return 1

def addingFav(shopId, custId):
    shopId = str(shopId)
    with connection.cursor() as cursor:
        cursor.execute("SELECT favourite from login_customermaster where custId=%s",[custId])
        row = cursor.fetchone()
        if row[0] is "":
            with connection.cursor() as cursorM:
                cursorM.execute("UPDATE login_customermaster set favourite=%s where custId=%s",[shopId,custId])
                try:
                    connection.commit()
                    return 1
                except:
                    connection.rollback()
                    return 0
        else:
            updateValue = row[0]+","+shopId
            with connection.cursor() as cursorM:
                cursorM.execute("UPDATE login_customermaster set favourite=%s where custId=%s",[updateValue,custId])
                try:
                    connection.commit()
                    return 1
                except:
                    connection.rollback()
                    return 0

def generateFinalList(custId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT favourite from login_customermaster where custId=%s",[custId])
        if(row_count==1):
            row = cursor.fetchone()
            x = json
            #mList = [int(e) if e.isdigit() else e for e in row[0].split(',')]
            useData = row[0].split(",")
            mList = []
            if len(useData)>0:
                for each in useData:
                    if(each!=""):
                        mList.append(int(each))
            x = {"Data":"Success","favList":mList}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Invalid input"}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def validDeleteFav(shopId,custId):
    shopId = str(shopId)
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT favourite from login_customermaster where custId=%s",[custId])
        row = cursor.fetchone()
        favList = row[0].split(",")
        favList.remove(shopId)
        favListUpdate = ",".join(favList)
        with connection.cursor() as cursorL:
            cursorL.execute("UPDATE login_customermaster set favourite=%s where custId=%s",[favListUpdate,custId])
            try:
                connection.commit()
                return 1
            except:
                connection.rollback()
                return 0

def creatingContact(userName,userType,message,phoneNumber,address,emailId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO login_contactus (userName, phoneNumber, message, address, userType, emailId) VALUES (%s, %s, %s, %s, %s, %s)",[userName, phoneNumber, message, address, userType, emailId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def generateContactFormDetails():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * FROM login_contactus")
        if(row_count>0):
            row = cursor.fetchone()
            container = []
            count = 0
            while row is not None:
                container.append({
                    "userName":row[1],
                    "phoneNumber":row[2],
                    "message":row[3],
                    "address":row[4],
                    "userType":row[5],
                    "emailId":row[6]
                })
                count = count + 1
                row = cursor.fetchone()
            myObj = {}
            myObj['Data']=container
            myObj['totalCount']=count
            response = JsonResponse(myObj)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def getValidToken(token, userId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_messagetoke where userId_id=%s and clientToken = %s and isActive=1",[userId, token])
        if(row_count>0):
            return 1
        elif(row_count==0):
            return 0

def InsertNewUserToken(token, userId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO login_messagetoke (clientToken, userId_id, isActive) VALUES (%s, %s, 1)",[token, userId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updateUserToken(token, userId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE login_messagetoke set isActive=0 where userId_id=%s and clientToken=%s",[userId, token])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def sendAdminNotif(body, title):
    with connection.cursor() as cursor:
        cursor.execute("SELECT uId_id from login_adminmaster where isActiveFl=1")
        row = cursor.fetchone()
        while row is not None:
            with connection.cursor() as cursorM:
                row_count=cursorM.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[row[0]])
                if(row_count>0):
                    rowM = cursorM.fetchone()
                    while rowM is not None:
                        sendNotification(rowM[0], title, body)
                        rowM = cursorM.fetchone()
            row = cursor.fetchone()

def sendAllRegNotification(body, title):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT clientToken from login_messagetoke where isActive=1")
        if row_count>0:
            row = cursor.fetchone()
            while row is not None:
                sendNotification(row[0], title, body)
                row = cursor.fetchone()

def sendToAllShops(body, title):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT uId_id from login_shopmaster where isActiveFl=1")
        if row_count>0:
            row = cursor.fetchone()
            while row is not None:
                with connection.cursor() as cursorI:
                    row_count1=cursorI.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[row[0]])
                    if(row_count1>0):
                        rowI = cursorI.fetchone()
                        while rowI is not None:
                            sendNotification(rowI[0], title, body)
                            rowI = cursorI.fetchone()
                row = cursor.fetchone()

def sendToAllCust(body, title):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT uId_id from login_customermaster")
        if row_count>0:
            row = cursor.fetchone()
            while row is not None:
                with connection.cursor() as cursorI:
                    row_count1=cursorI.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[row[0]])
                    if(row_count1>0):
                        rowI = cursorI.fetchone()
                        while rowI is not None:
                            sendNotification(rowI[0], title, body)
                            rowI = cursorI.fetchone()
                row = cursor.fetchone()

def sendToAllDm(body, title):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT uId_id from login_delivermedmaster where isActiveFl=1")
        if row_count>0:
            row = cursor.fetchone()
            while row is not None:
                with connection.cursor() as cursorI:
                    row_count1=cursorI.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[row[0]])
                    if(row_count1>0):
                        rowI = cursorI.fetchone()
                        while rowI is not None:
                            sendNotification(rowI[0], title, body)
                            rowI = cursorI.fetchone()
                row = cursor.fetchone()

def sendToUser(sendTo, body, title):
    with connection.cursor() as cursor:
        row_count1=cursor.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[sendTo])
        if(row_count1>0):
            rowI = cursor.fetchone()
            while rowI is not None:
                sendNotification(rowI[0], title, body)
                rowI = cursor.fetchone()
            return 1
        else:
            return 0

def reviewList(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT reviewId from login_reviewmaster where shopId_id=%s",[shopId])
        if(row_count>0):
            row = cursor.fetchone()
            data = []
            count = 0
            while row is not None:
                count = count+1
                data.append(row[0])
                row = cursor.fetchone()
            x = {"Data":data,"count":count}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"count":0}
            response = JsonResponse(x)
            response.status_code = 200
        return response

def reviewDetails(reviewListId):
    with connection.cursor() as cursor:
        if(len(reviewListId)>25):
            x = json
            x = {"Data":[],"count":0}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            reviewListId = list(set(reviewListId))
            data = []
            count = 0
            for each in reviewListId:
                row_count = cursor.execute("SELECT * from login_reviewmaster where reviewId=%s",[each])
                if(row_count==1):
                    count = count + 1
                    row = cursor.fetchone()
                    with connection.cursor() as cursorM:
                        cursorM.execute("SELECT custName from login_customermaster where custId=%s",[row[4]])
                        rowM = cursorM.fetchone()
                    data.append({
                        "rating":row[2],
                        "custName":rowM[0],
                        "reviewText":row[3],
                        "date":row[1]
                    })
            x = {}
            x = {"Data":data,"count":count}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def getCurrentDMCount(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT COUNT(*) from login_delivermedmaster where shopId_id=%s",[shopId])
        if row_count==1:
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def insertNewTokenClient(token, userType):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO login_usertoken (clientToken, userType) VALUES (%s, %s)",[token,userType])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0
