# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.http import JsonResponse
from django.db import connection
import logging
import urllib
import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/login.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def sendOtp(phoneNumber, otp, userNeed):
    userName = "2000177109"
    password = "FtmCoUCrw"
    message = "Your OTP for MARTJINNI sign-up is "+str(otp)+". Do not share. Kindly update the same."
    useMessage = urllib.quote(message)
    logger.info(useMessage)
    url = "https://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91"
    xUrl = str(phoneNumber)+"&msg="+useMessage+"&msg_type=TEXT&userid="+userName+"&auth_scheme=plain&password="+password+"&v=1.1&format=json"
    xUrl = url+xUrl
    response = requests.request("GET", xUrl)
    logger.exception(response.text)

def sendNotification(token, title, body):
    url = "https://fcm.googleapis.com/fcm/send"
    headers = {
    'content-type': "application/json",
    'authorization': "key=AAAA0XmdeuM:APA91bG9cbOFwbIkWX8Z0_rFqNGlj9Kh1ud3fNePDLSmucMwHQygGz4vDq2RuJ5W4RKZsFsD9P7TH0EPRF1RjQov4jc5RUtWDnMifpGKHueoyi5_MNX4QEQS9sd5jGhbRCAnaYWrenfs"
    }
    myObj = {"data":{"title":title,"body":body,"sound":"default"},"to":token}
    myObjFinal = json.dumps(myObj)
    response = requests.request("POST", url, data=myObjFinal, headers=headers)
    logger.exception(response.text)
