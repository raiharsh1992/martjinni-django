# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
# Create your models here.

class loginMaster(models.Model):
    """docstring for loginMaster."""
    userNameLogin = models.CharField(max_length=100, unique=True)
    password = models.TextField(max_length=32, blank=False)
    userType = models.CharField(max_length=6, blank=False)
    uId = models.AutoField(max_length=32, primary_key=True)
    isActiveFl = models.BooleanField(default="1")
    def __init__(self, arg):
        super(loginMaster, self).__init__()
        self.arg = arg

class sessionMaster(models.Model):
    """docstring for sessionMaster."""
    basicAuthenticate = models.CharField(max_length=50, primary_key=True)
    loginTime = models.DateTimeField(auto_now_add=True)
    deviceType = models.CharField(max_length=10, blank=True)
    uId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    userType = models.CharField(max_length=6)
    isActiveFl = models.BooleanField()
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg


class customerMaster(models.Model):
    """docstring for sessionMaster."""
    uId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    custId = models.AutoField(max_length=32, primary_key=True)
    custName = models.CharField(max_length=32)
    phoneNumber = models.BigIntegerField(blank=False, unique=True)
    emailId = models.EmailField(max_length=254, blank=False, unique=True)
    favourite = models.CharField(max_length=100, blank=True, unique=False, default="")
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class mjAvailable(models.Model):
    mjId = models.AutoField(max_length=32, primary_key=True)
    mjName = models.CharField(max_length=32, unique=True)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class cateogaryShops(models.Model):
    catId = models.AutoField(max_length=32, primary_key=True)
    catName = models.CharField(max_length=32, blank=False)
    mjId = models.ForeignKey(mjAvailable, on_delete=models.PROTECT, default=1)
    catKeyword = models.CharField(max_length=100, blank=False)
    def __init__(self, arg):
        super(userMaster, self).__init__()
        self.arg = arg

class shopMaster(models.Model):
    """docstring for sessionMaster."""
    uId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    shopId = models.AutoField(max_length=32, primary_key=True)
    shopName = models.CharField(max_length=500)
    phoneNumber = models.BigIntegerField(blank=False, unique=True)
    isOpen = models.BooleanField(blank=False)
    sLat = models.DecimalField(max_digits=13, decimal_places=5, blank=False)
    sLng = models.DecimalField(max_digits=13, decimal_places=5, blank=False)
    description = models.CharField(max_length=200, blank=True)
    isActiveFl = models.BooleanField(blank=False)
    isSubscription = models.BooleanField(blank=False)
    isDailyNeeds = models.BooleanField(blank=False)
    delRadius = models.IntegerField(blank=False, default='2')
    emailId = models.EmailField(max_length=254, blank=False, unique=True)
    address = models.CharField(max_length=254, blank=False, default="ABC")
    delCharges = models.IntegerField(blank=False, default=20)
    minOrderValue = models.IntegerField(blank=False, default=100)
    delToken = models.IntegerField(blank=False, default=20)
    catId = models.CharField(max_length=100, blank=False, default=1)
    imageUploaded = models.BooleanField(default=0)
    subCatId = models.CharField(max_length=100, blank=True)
    isVerified = models.BooleanField(default=1)
    rating = models.DecimalField(max_digits=3, decimal_places=2, blank=False, default=0)
    totalRater = models.IntegerField(blank=False, default=0)
    totalOrders = models.IntegerField(blank=False, default=0)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class adminMaster(models.Model):
    """docstring for sessionMaster."""
    uId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    phoneNumber = models.BigIntegerField(blank=False, unique=True)
    adminId = models.AutoField(max_length=32, primary_key=True)
    adminName = models.CharField(max_length=32)
    adminType = models.CharField(max_length=6)
    isActiveFl = models.BooleanField(blank=False)
    emailId = models.EmailField(max_length=254, blank=False, unique=True)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class deliverMedMaster(models.Model):
    """docstring for sessionMaster."""
    uId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    dmId = models.AutoField(max_length=32, primary_key=True)
    phoneNumber = models.BigIntegerField(blank=False, unique=True)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    dmName = models.CharField(max_length=32)
    dmLat = models.DecimalField(max_digits=13, decimal_places=5, blank=False)
    dmLng = models.DecimalField(max_digits=13, decimal_places=5, blank=False)
    withOrders = models.BooleanField()
    isActiveFl = models.BooleanField(blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class otpMaster(models.Model):
    """docstring for sessionMaster."""
    otpId = models.AutoField(max_length=32, primary_key=True)
    phoneNumber = models.BigIntegerField(blank=False, unique=False)
    otp = models.IntegerField(blank=False)
    purpose = models.CharField(max_length=32, blank=False)
    isUsed = models.BooleanField(blank=False)
    expiryTime = models.DateTimeField(auto_now_add=False)
    userType = models.CharField(max_length=5, blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class clientPropertiesMaster(models.Model):
    cpId = models.AutoField(max_length=32, primary_key=True)
    cpName = models.CharField(max_length=32, unique=True)
    userType = models.CharField(max_length=10, blank=False)
    propertyDefault = models.IntegerField(blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg =arg

class clientPropertiesValuesShops(models.Model):
    cpId = models.ForeignKey(clientPropertiesMaster, on_delete=models.PROTECT)
    cpValue = models.CharField(max_length=32, blank=False)
    userType = models.CharField(max_length=10, blank=False)
    cpName = models.CharField(max_length=32, unique=False)
    typeId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class contactUs(models.Model):
    userName = models.CharField(max_length=32, blank=False)
    phoneNumber = models.CharField(blank=False, unique=False, max_length=12)
    message = models.CharField(blank=False, max_length=150)
    address = models.CharField(blank=True, max_length=100)
    userType = models.CharField(max_length=5, blank=False)
    emailId = models.CharField(max_length=80, blank=False, unique=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class messageToke(models.Model):
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    clientToken = models.CharField(max_length=300, unique=False, blank=False)
    isActive = models.BooleanField(blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class userToken(models.Model):
    clientToken = models.CharField(max_length=300, unique=False, blank=False)
    userType = models.CharField(max_length=255, unique=False, blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class reviewMaster(models.Model):
    reviewId = models.AutoField(primary_key=True, max_length=32)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    custId = models.ForeignKey(customerMaster, on_delete=models.PROTECT)
    date = models.DateTimeField(auto_now_add=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, blank=False, default=0)
    reviewText = models.CharField(max_length=500)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg
