#space for import
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.http import JsonResponse
import logging
from django.db import connection
from datetime import datetime,timedelta

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/login.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)
