# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.shortcuts import render
from django.http import JsonResponse
import logging
from layer1 import validateLogin,existingUser,otpLayer,phoneLayer,createUserLayer,signOutLayer,getListDM,getStatusShop,updateStatusShop,generateShopList,generateCustList,uploadProfileImage,insertClientProperty,insertClientShopProperty,getRequestedShopProperty,markStoreActive,validateUserPhone,validateUserOTP,chngPassword,addToFavor,viewingFav,deteFav,makingQuery,viewingQuery,insertNewToken,deleteOldToken,sendFinalNotification,generateReviewList,generateReviewDetails,validateShopProperty,newUserToken

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/login.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)


# Create your views here.
#LOGIN API
def login(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Login in for ' + request.body)
                response = validateLogin(request.body)
            else :
                logger.info('Login in for')
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def validateuser(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Validating username for ' + request.body)
                response = existingUser(request.body)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def generateotp(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Validating phonenumber for ' + request.body)
                response = otpLayer(request.body)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def ifvalidphone(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Validating phonenumber for ' + request.body)
                response = phoneLayer(request.body)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def createuser(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Validating phonenumber for ' + request.body)
                response = createUserLayer(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def signout(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Validating phonenumber for ' + request.body)
                response = signOutLayer(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getdmlist(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Getting dm list for ' + request.body)
                response = getListDM(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getshopstatus(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Getting shop status list for ' + request.body)
                response = getStatusShop(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def updateshopstatus(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Getting shop status list for ' + request.body)
                response = updateStatusShop(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getshoplist(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Getting shop status list for ' + request.body)
                response = generateShopList(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getcustlist(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Getting shop status list for ' + request.body)
                response = generateCustList(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def profilpicupload(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info(request.POST)
                response = uploadProfileImage(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def createclientproperty(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                response = insertClientProperty(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def clientshopproperty(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info(request.POST)
                response = insertClientShopProperty(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getshopproperty(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                response = getRequestedShopProperty(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def storeactive(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                response = markStoreActive(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def isvaliduserphone(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Checking phone number for' + request.body)
                response = validateUserPhone(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def isvalidchangeotp(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Checking OTP for' + request.body)
                response = validateUserOTP(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def chngpwd(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Changing password for' + request.body)
                response = chngPassword(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def addtofav(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Changing password for' + request.body)
                response = addToFavor(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewfav(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Changing password for' + request.body)
                response = viewingFav(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def delfav(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Changing password for' + request.body)
                response = deteFav(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def makequery(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = makingQuery(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewquery(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = viewingQuery(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def setusertoken(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = insertNewToken(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def newtokenclient(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = newUserToken(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def remusertoken(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = deleteOldToken(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def sendnotification(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = sendFinalNotification(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def getreviewlist(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = generateReviewList(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def reviewdetails(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = generateReviewDetails(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def validateshopprop(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                logger.info('Query for' + request.body)
                response = validateShopProperty(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
