"""martjinni URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
import login.views
import address.views
import items.views
import orders.views
import payment.views

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^login', login.views.login ),
    url(r'^shoplist', address.views.shoplist ),
    url(r'^validatedelivery', address.views.validatedelivery ),
    url(r'^trendlist', address.views.trendlist ),
    url(r'^itemlist', items.views.itemlist ),
    url(r'^custaddresslist', address.views.custaddresslist ),
    url(r'^addaddress', address.views.addaddress ),
    url(r'^validateuser', login.views.validateuser ),
    url(r'^generateotp', login.views.generateotp ),
    url(r'^createuser', login.views.createuser ),
    url(r'^validphone', login.views.ifvalidphone ),
    url(r'^signout', login.views.signout ),
    url(r'^createorder', orders.views.createorder ),
    url(r'^orderlist', orders.views.orderlist ),
    url(r'^orderdetails', orders.views.orderdetails ),
    url(r'^updateorderstatus', orders.views.updateorderstatus ),
    url(r'^getdmlist', login.views.getdmlist ),
    url(r'^getshopstatus', login.views.getshopstatus ),
    url(r'^updateshopstatus', login.views.updateshopstatus ),
    url(r'^getshoplist', login.views.getshoplist ),
    url(r'^getcustlist', login.views.getcustlist ),
    url(r'^assigndm', orders.views.assigndm ),
    url(r'^addmainitem', items.views.addmainitem ),
    url(r'^addsubitem', items.views.addsubitem ),
    url(r'^updateitem', items.views.updateitem ),
    url(r'^getgstratelist', items.views.getGstMasterValues ),
    url(r'^insertgstrate', items.views.createGstMasterValues ),
    url(r'^updategstrate', items.views.updateGstMasterValues ),
    url(r'^viewcateogary', items.views.viewCateogary ),
    url(r'^insertcateogary', items.views.insertCateogary ),
    url(r'^uploadprofileshop', login.views.profilpicupload ),
    url(r'^createclientproperty', login.views.createclientproperty ),
    url(r'^clientshopproperty', login.views.clientshopproperty ),
    url(r'^getshopproperty', login.views.getshopproperty ),
    url(r'^insertaddon', items.views.insertaddon ),
    url(r'^updateaddon', items.views.updateaddon ),
    url(r'^insertsubsinfo', items.views.insertsubsinfo ),
    url(r'^updatesubsinfo', items.views.updatesubsinfo ),
    url(r'^iteminsertaddon', items.views.iteminsertaddon ),
    url(r'^itemupdateaddon', items.views.itemupdateaddon ),
    url(r'^shopaddoninfo', items.views.shopaddoninfo ),
    url(r'^shopsubsinfo', items.views.shopsubsinfo ),
    url(r'^showshopitemcat', items.views.showshopitemcat ),
    url(r'^insertquantityinfo', items.views.insertquantityinfo ),
    url(r'^insertitemimage', items.views.insertitemimage ),
    url(r'^storeactive', login.views.storeactive ),
    url(r'^payfororder', orders.views.payfororder ),
    url(r'^insertshoptime', items.views.insertshoptime ),
    url(r'^viewshoptime', items.views.viewshoptime ),
    url(r'^updateshoptime', items.views.updateshoptime ),
    url(r'^vieworderformark', orders.views.vieworderformark ),
    url(r'^isvaliduserphone', login.views.isvaliduserphone ),
    url(r'^isvalidchangeotp', login.views.isvalidchangeotp ),
    url(r'^chngpwd', login.views.chngpwd ),
    url(r'^viesublist', orders.views.viesublist ),
    url(r'^viesubdetails', orders.views.viesubdetails ),
    url(r'^addtofav', login.views.addtofav ),
    url(r'^viewfav', login.views.viewfav ),
    url(r'^delfav', login.views.delfav ),
    url(r'^addpocket', address.views.addpocket ),
    url(r'^viewpocket', address.views.viewpocket ),
    url(r'^generatebill', payment.views.generatebill ),
    url(r'^makequery', login.views.makequery ),
    url(r'^viewquery', login.views.viewquery ),
    url(r'^viewbilllist', payment.views.viewbilllist ),
    url(r'^getcustomerlist', payment.views.getcustomerlist ),
    url(r'^paythebill', payment.views.paythebill ),
    url(r'^keywordsearch', address.views.keywordsearch ),
    url(r'^setusertoken', login.views.setusertoken ),
    url(r'^remusertoken', login.views.remusertoken ),
    url(r'^sendnotification', login.views.sendnotification ),
    url(r'^getshopinfo', address.views.getshopinfo ),
    url(r'^getreviewlist', login.views.getreviewlist ),
    url(r'^reviewdetails', login.views.reviewdetails ),
    url(r'^resigsterreview', orders.views.resigsterreview ),
    url(r'^getpromolist', payment.views.getpromolist ),
    url(r'^insertpromo', payment.views.insertpromo ),
    url(r'^isnewpromo', payment.views.isnewpromo ),
    url(r'^updatepromo', payment.views.updatepromo ),
    url(r'^applypromo', payment.views.applypromo ),
    url(r'^cancelorder', orders.views.cancelorder ),
    url(r'^validateshopprop', login.views.validateshopprop ),
    url(r'^newtokenclient', login.views.newtokenclient),
    url(r'^viewmjcat',items.views.viewmjcat),
]
