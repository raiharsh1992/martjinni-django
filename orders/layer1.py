#space for import
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import threading
import sys
from django.shortcuts import render
from django.http import JsonResponse
import logging
from login.layer2 import validateKey,vlaidatesessiontype,getCustId,getShpId,getAdmId,getDMId
#space for import
#space for import
from layer2 import isDelPossible,getItemValidity,isValidAdd,insertOrderItem,getUserOrderList,getShopOrderList,getADMOrderList,getDMOrderList,validateCustomerOrder,orderDetailsCust,validateShopOrder,getShopOrderDetails,validareAdmOrder,getADMOrderDetails,validateDMOrder,getDMOrderDetails,orderStatus,changeOrderStatus,validateShopDm,getGstRate,getMinOrderValue,getDelChargese,createStoreOrder,createBasicOrder,validateSingleShopOrder,inserBasicStoreOrder,validateItemInfo,calculateItemCost,calculateItemGst,calculateAddOnName,checkIfDelIncluded,updatingShopOrInfo,updateMasterInfo,getShopName,calculateDaysRemaining,evaluateItemName,evaluateStartDate,evaluateSubsId,insertItemInformation,isOrderReadyForPay,isValidAmount,updatingUserOrder,validateShopOrderItem,updateMasterOrder,insertingHistoryEvent,updatingOrderItemList,changeOrderStatus,getDmOrderStatus,updateItemHistoryStatus,validateLastItem,updateOrderItem,validateLastStoreItem,updatingStoreOrderStatus,validateFinalMasterOrder,updatingMasterOrder,validateDMOrderList,validateIfShopOrder,generateItemList,generateSubShopList,generateSubCustList,generateSubAdmList,validateShpSub,validateCustSub,validateAdmSub,generateSubsDetails,sendNotificationOrderCreate,masterShopOrder,rateOrder,markAReview,validateMasterStoreLink,finalUserCancel,validateStoreOrderCancel,cancelOrderForStore,updateShopCount

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/orders.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def createOrderLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId!=userId):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        else:
            userType = vlaidatesessiontype(userId)
            if(userType=="CUST"):
                typeId1 = getCustId(userId)
                typeId = rString['typeId']
                if(typeId==typeId1):
                    addId = rString['custAddId']
                    validAdd = isValidAdd(addId, userId)
                    if(validAdd==1):
                        orderId = createBasicOrder(typeId, addId)
                        if(orderId!=0):
                            totalOrderAmount = 0
                            totalShopCount = 0
                            totalGstAmount = 0
                            shopCount = 0
                            failedShop = []
                            successShop = []
                            shopItemList = rString['shopItemList']
                            for each in shopItemList:
                                totalShopAmount = 0
                                totalShopGst = 0
                                itemCount = 0
                                failedItem = []
                                successItem = []
                                shopId = each['shopId']
                                ifDelPos = isDelPossible(addId, shopId)
                                if ifDelPos == 1:
                                    ifSingleShopOrder = validateSingleShopOrder(orderId,shopId)
                                    if ifSingleShopOrder == 0 :
                                        orderStoreId = inserBasicStoreOrder(orderId, shopId)
                                        if(orderStoreId != 0):
                                            for x in each['itemList']:
                                                itemId = x['itemId']
                                                quantId = x['quantId']
                                                addOnInfo = x['addOnInfo']
                                                subsInfo = x['subsInfo']
                                                #NEED TO CREATE FUNCTIONS FROM HERE FOR VARIOUS ITEM LEVEL VALIDATIONS#
                                                ifValidItem = validateItemInfo(shopId,itemId,quantId,addOnInfo,subsInfo)
                                                if(ifValidItem==1):
                                                    units = x['units']
                                                    itemCost = calculateItemCost(itemId,quantId,addOnInfo,subsInfo,units)
                                                    itemGst = calculateItemGst(itemId,itemCost)
                                                    addOnName = calculateAddOnName(addOnInfo)
                                                    subsDaysLeft = calculateDaysRemaining(subsInfo)
                                                    itemName = evaluateItemName(itemId,quantId,units)
                                                    subsStartDate = evaluateStartDate(subsInfo)
                                                    subsId =evaluateSubsId(subsInfo)
                                                    insertItemInfo = insertItemInformation(itemName, units, itemCost, addOnName, subsDaysLeft, subsStartDate, orderStoreId, quantId, itemId, subsId)
                                                    if(insertItemInfo==1):
                                                        itemCount = itemCount+1
                                                        totalShopAmount = totalShopAmount + itemCost
                                                        totalShopGst = totalShopGst + itemGst
                                                        successItem.append({
                                                            "itemId":itemId,
                                                            "itemCost":itemCost,
                                                            "itemGst":itemGst,
                                                            "subsDays":subsDaysLeft,
                                                            "subsStartDate":subsStartDate,
                                                            "itemName":itemName,
                                                            "units":units,
                                                            "addOnName":addOnName
                                                        })
                                                    else:
                                                        failedItem.append({
                                                            "itemId":itemId,
                                                            "reason":"Not able to create record"
                                                        })
                                                else:
                                                    failedItem.append({
                                                        "itemId":itemId,
                                                        "reason":"Not valid information"
                                                    })
                                            getDelValue = checkIfDelIncluded(totalShopAmount, shopId)
                                            if(getDelValue>0):
                                                isDelIncluded = 1
                                                finalTotal = totalShopAmount + totalShopGst + getDelValue
                                            else:
                                                isDelIncluded = 0
                                                finalTotal = totalShopAmount + totalShopGst
                                            updateShopOrInfo = updatingShopOrInfo(finalTotal, totalShopGst, isDelIncluded, itemCount, orderStoreId)
                                            if(updateShopOrInfo==1):
                                                totalOrderAmount = totalOrderAmount + finalTotal
                                                totalGstAmount = totalGstAmount + totalShopGst
                                                totalShopCount = totalShopCount + 1
                                                shopName = getShopName(shopId)
                                                successShop.append({
                                                    "shopId":shopId,
                                                    "shopName":shopName,
                                                    "shopTotal":finalTotal,
                                                    "deliveryCharges":getDelValue,
                                                    "shopGst":totalShopGst,
                                                    "successItem":successItem,
                                                    "failedItem":failedItem,
                                                    "orderStoreId":orderStoreId
                                                })
                                                updateShopCount(shopId)
                                            else:
                                                failedShop.append({
                                                    "shopId":shopId,
                                                    "reason":"Issue while updating shop calculations"
                                                })
                                        else:
                                            failedShop.append({
                                                "shopId":shopId,
                                                "reason":"Issue while creating basic order"
                                            })
                                    else:
                                        failedShop.append({
                                            "shopId":shopId,
                                            "reason":"Duplicate shop order"
                                        })
                                else:
                                    failedShop.append({
                                        "shopId":shopId,
                                        "reason":"delNot possible"
                                    })
                            isMasterUpdated = updateMasterInfo(totalOrderAmount, totalGstAmount, totalShopCount, orderId)
                            if(isMasterUpdated==1):
                                container = {}
                                container['totalShopCount']=totalShopCount
                                container['totalBilledAmount']=totalOrderAmount
                                container['totalGstPaid']=totalGstAmount
                                container['failedShop']=failedShop
                                container['successShop']=successShop
                                container['orderId']=orderId
                                response = JsonResponse(container)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Something miserable happened"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Something real bad happened"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Wrong addId"}
                        response = JsonResponse(x)
                        response.status_code = 403
                else:
                    x = json
                    x = {"Data":"Not authorized2"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Not authorized1"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    except UnboundLocalError:
        x = json
        x = {"Data":"Invalid quantity passed"}
        response = JsonResponse(x)
        response.status_code = 409
        logger.exception(request)
    return response

def orderListLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId!=userId):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        else:
            userType1 = vlaidatesessiontype(userId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="CUST"):
                    typeId1 = getCustId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        response = getUserOrderList(typeId)
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="SHP"):
                    typeId1 = getShpId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        viewType = rString['viewType']
                        response = getShopOrderList(typeId, viewType)
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    typeId1 = getAdmId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        viewType = rString['viewType']
                        response = getADMOrderList(viewType)
                    else:
                        x = json
                        x = {"Data":"Incorrect type id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="DM"):
                    typeId1 = getDMId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        viewType = rString['viewType']
                        response = getDMOrderList(typeId,viewType)
                    else:
                        x = json
                        x = {"Data":"Incorrect type is passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Something is not correct"}
                    response = JsonResponse(x)
                    response.status_code = 409
                    logger.warn(request.body)
            else:
                x = json
                x = {"Data":"Incorrect usertype selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def orderDetailsLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId!=userId):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        else:
            userType1 = vlaidatesessiontype(userId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="CUST"):
                    typeId1 = getCustId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        orderId = rString['orderId']
                        validOrder = validateCustomerOrder(typeId, orderId)
                        if(validOrder==1):
                            response = orderDetailsCust(typeId, orderId)
                        else:
                            x = json
                            x = {"Data":"Incorrect order id passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="SHP"):
                    typeId1 = getShpId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        orderId = rString['orderId']
                        validOrder = validateShopOrder(typeId, orderId)
                        if(validOrder==1):
                            response = getShopOrderDetails(typeId, orderId)
                        else:
                            x = json
                            x = {"Data":"Incorrect order Id"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect type Id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    typeId1 = getAdmId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        orderId = rString['orderId']
                        validOrder = validareAdmOrder(orderId)
                        if(validOrder==1):
                            response = getADMOrderDetails(orderId)
                        else:
                            x = json
                            x = {"Data":"Incorrect order Id"}
                            response = JsonResponse(x)
                            response.status_code=409
                    else:
                        x = json
                        x = {"Data":"Incorrect type id"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="DM"):
                    typeId1 = getDMId(userId)
                    typeId = rString['typeId']
                    if(typeId==typeId1):
                        orderId = rString['orderId']
                        validOrder = validateDMOrderList(typeId, orderId)
                        if(validOrder==1):
                            response = getDMOrderDetails(typeId, orderId)
                        else:
                            x = json
                            x = {"Data":"Incorrect order Id"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect type Id"}
                        response = JsonResponse(x)
                        response.status_code = 200
                else:
                    x = json
                    x = {"Data":"Something is not right"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def orderStausUpdater(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId!=userId):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        else:
            userType1 = vlaidatesessiontype(userId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    typeId1 = getShpId(userId)
                    typeId = rString['typeId']
                    if(typeId1==typeId):
                        orderId = rString['orderId']
                        validOrder = validateShopOrder(typeId, orderId)
                        if(validOrder==1):
                            updateStatus = rString['orderStatus']
                            currentStatus = orderStatus(orderId)
                            if(currentStatus=="NEW"):
                                if(updateStatus=="ACCEPTED"):
                                    updateStatusOrder = changeOrderStatus(orderId,updateStatus)
                                    if(updateStatusOrder==1):
                                        well = updateMasterOrder(orderId, updateStatus)
                                        if(well==1):
                                            x = json
                                            x = {"Data":"Order ACCEPTED"}
                                            response = JsonResponse(x)
                                            response.status_code = 200
                                        else:
                                            x = json
                                            x = {"Data":"Issue while updating master"}
                                            response = JsonResponse(x)
                                            response.status_code = 409
                                    else:
                                        x = json
                                        x = {"Data":"Issue while updating order"}
                                        response = JsonResponse(x)
                                        response.status_code = 409
                                else:
                                    x = json
                                    x = {"Data":"Invalid order status"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Invalid order selected"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Invalid orderId passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 409
                elif(userType=="DM"):
                    typeId1 = getDMId(userId)
                    typeId = rString['typeId']
                    if(typeId1==typeId):
                        orderStoreId = rString['orderStoreId']
                        statusBeingMarked = rString['orderStatus']
                        failure = []
                        success = []
                        for each in rString['itemBeingMarked']:
                            dayCount = each['dayCount']
                            orderItemId = each['orderItemId']
                            validOrder = validateDMOrder(typeId, orderStoreId, dayCount, orderItemId)
                            if(validOrder>0):
                                orderPresentStatus = getDmOrderStatus(validOrder,typeId)
                                if((orderPresentStatus=="ASSIGNED")and(statusBeingMarked=="PICKED")):
                                    markOrderPicked = updateItemHistoryStatus("PICKED",validOrder)
                                    if(markOrderPicked==1):
                                        success.append({
                                            "orderItemId":orderItemId,
                                            "reason":"update success"
                                        })
                                    else:
                                        failure.append({
                                            "orderItemId":orderItemId,
                                            "reason":"not able to mark it"
                                        })
                                elif((orderPresentStatus=="PICKED")and(statusBeingMarked=="DELIVER")):
                                    markOrderDeliver = updateItemHistoryStatus("DELIVER",validOrder)
                                    if(markOrderDeliver==1):
                                        isLastDel = validateLastItem(orderItemId)
                                        if(isLastDel==1):
                                            updateItemStatus = updateOrderItem(orderItemId,"COMPLETED",orderStoreId)
                                            if(updateItemStatus==1):
                                                ifLastStoreItem = validateLastStoreItem(orderStoreId)
                                                if(ifLastStoreItem==1):
                                                    updateStoreOrderStatus = updatingStoreOrderStatus(orderStoreId,"COMPLETED")
                                                    if(updateStoreOrderStatus ==1):
                                                        isFinalMasterOrder = validateFinalMasterOrder(orderStoreId)
                                                        if(isFinalMasterOrder==1):
                                                            updateMasterOrder1 = updatingMasterOrder(orderStoreId,"COMPLETED")
                                                            if(updateMasterOrder1==1):
                                                                success.append({
                                                                    "orderItemId":orderItemId,
                                                                    "reason":"update success"
                                                                })
                                                            else:
                                                                failure.append({
                                                                    "orderItemId":orderItemId,
                                                                    "reason":"marking master failed"
                                                                })
                                                        else:
                                                            success.append({
                                                                "orderItemId":orderItemId,
                                                                "reason":"update success"
                                                            })
                                                    else:
                                                        failure.append({
                                                            "orderItemId":orderItemId,
                                                            "reason":"store marking failed"
                                                        })
                                                else:
                                                    success.append({
                                                        "orderItemId":orderItemId,
                                                        "reason":"update success"
                                                    })
                                            else:
                                                failure.append({
                                                    "orderItemId":orderItemId,
                                                    "reason":"order item marking failed"
                                                })
                                        else:
                                            success.append({
                                                "orderItemId":orderItemId,
                                                "reason":"update success"
                                            })
                                    else:
                                        failure.append({
                                            "orderItemId":orderItemId,
                                            "reason":"not able to mark order deliver"
                                        })
                                else:
                                    failure.append({
                                        "orderId":orderItemId,
                                        "reason":"incore order status"
                                    })
                            else:
                                failure.append({
                                    "orderItemId":orderItemId,
                                    "reason":"not valid order information"
                                })
                        container = {}
                        container['failed']=failure
                        container['success']=success
                        response = JsonResponse(container)
                        response.status_code = 200
                    else:
                        x = json
                        x = {"Data":"Incorrect type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Incorrect type selected"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect type ID passed"}
                response = JsonResponse(x)
                response.status_code=401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def assignOrderDm(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId!=userId):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        else:
            userType1 = vlaidatesessiontype(userId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    dmId = rString['dmId']
                    typeId = rString['typeId']
                    typeId1 = getShpId(userId)
                    if(typeId==typeId1):
                        validM = validateShopDm(typeId, dmId)
                        orderId = rString['orderId']
                        if(validM==1):
                            success=[]
                            failure = []
                            for each in rString['items']:
                                orderItemId = each['itemId']
                                getDayCount=validateShopOrderItem(typeId,orderId,orderItemId)
                                if(getDayCount!=0):
                                    insertHistoryEvent = insertingHistoryEvent(orderId,orderItemId,dmId,getDayCount)
                                    if(insertHistoryEvent==1):
                                        updatingOrderItemList1 = updatingOrderItemList(orderItemId)
                                        changeStoreOrderStatus=changeOrderStatus(orderId,"WORKING")
                                        if((updatingOrderItemList1==1)and(changeStoreOrderStatus==1)):
                                            success.append({
                                                "itemId":orderItemId,
                                                "reason":"Item assigned"
                                            })
                                        else:
                                            failure.append({
                                                "itemid":orderItemId,
                                                "reason":"Order item list not updated"
                                            })
                                    else:
                                        failure.append({
                                            "itemId":orderItemId,
                                            "reason":"Failed during history creation"
                                        })
                                else:
                                    failure.append({
                                        "itemId":orderItemId,
                                        "reason":"Not allowed"
                                    })
                            container = {}
                            container['success']=success
                            container['failure']=failure
                            response = JsonResponse(container)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Invalid dm Id passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Invalid typeId"}
                        response = JsonResponse(x)
                        response,status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Wrong userType"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def payingForOrder(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId!=userId):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        else:
            userType1 = vlaidatesessiontype(userId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="CUST"):
                    typeId = rString['typeId']
                    typeId1 = getCustId(userId)
                    if(typeId==typeId1):
                        orderId = rString['orderId']
                        ##Functions missing##
                        isValidOrder = isOrderReadyForPay(orderId,typeId)
                        if(isValidOrder==1):
                            amount = rString['amount']
                            validAmount = isValidAmount(orderId, amount)
                            if(validAmount==1):
                                ##PAYMENT GATEWAY TO BE INTEGRATED HERE####
                                updateUserOrder = updatingUserOrder(orderId)
                                if(updateUserOrder==1):
                                    download_thread = threading.Thread(target=sendNotificationOrderCreate, args=(orderId,))
                                    download_thread.start()
                                    x = json
                                    x = {"Data":"Order paid successfully"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Sorry not able to pay"}
                                    response=JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Sorry Incorrect amount selected"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Sorry Invalid order selected"}
                            response = JsonResponse(x)
                            response.status_code = 409
                        ##Functions Present##
                    else:
                        x = json
                        x = {"Data":"Invalid typeId"}
                        response = JsonResponse(x)
                        response,status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 409
            else:
                x = json
                x = {"Data":"Wrong userType"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def getListForShop(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    orderId = rString['orderId']
                    isValidOrder = validateIfShopOrder(orderId,shpId)
                    if(isValidOrder==1):
                        response= generateItemList(orderId)
                    else:
                        x = json
                        x = {"Data":"Invalid orderId passed"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Invalid userType"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid userId passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generateSubList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    response = generateSubShopList(shpId)
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif((userType==userType1)&(userType=="CUST")):
                shpId = getCustId(uId)
                if(shpId==rString['typeId']):
                    response = generateSubCustList(shpId)
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif((userType==userType1)&(userType=="ADM")):
                shpId = getAdmId(uId)
                if(shpId==rString['typeId']):
                    response = generateSubAdmList(shpId)
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Invalid userType"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid userId passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generateSubDetails(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    subsId = rString['subsId']
                    isValidSub = validateShpSub(subsId, shpId)
                    if(isValidSub==1):
                        response = generateSubsDetails(subsId)
                    else:
                        x = json
                        x = {"Data":"Incorrect subsId"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif((userType==userType1)&(userType=="CUST")):
                shpId = getCustId(uId)
                if(shpId==rString['typeId']):
                    subsId = rString['subsId']
                    isValidSub = validateCustSub(subsId, shpId)
                    if(isValidSub==1):
                        response = generateSubsDetails(subsId)
                    else:
                        x = json
                        x = {"Data":"Incorrect subsId"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif((userType==userType1)&(userType=="ADM")):
                shpId = getAdmId(uId)
                if(shpId==rString['typeId']):
                    subsId = rString['subsId']
                    isValidSub = validateAdmSub(subsId)
                    if(isValidSub==1):
                        response = generateSubsDetails(subsId)
                    else:
                        x = json
                        x = {"Data":"Incorrect subsId"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Invalid type-Id passed"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Invalid userType"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid userId passed"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def registerRatingReview(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="CUST")):
                custId = getCustId(uId)
                if(custId==rString['typeId']):
                    orderMasterId = rString['orderMasterId']
                    isValidUserOrder = validateCustomerOrder(custId, orderMasterId)
                    if(isValidUserOrder==1):
                        orderStoreId = rString['orderStoreId']
                        isValidStoreOrder = masterShopOrder(orderMasterId, orderStoreId)
                        if(isValidStoreOrder==1):
                            rating = rString['rating']
                            if((rating>0)and(rating<=5)):
                                isOrderRated = rateOrder(rating, orderStoreId)
                                if(isOrderRated==1):
                                    if(rString['reviewMark']=="YES"):
                                        isReviewed = markAReview(custId, orderStoreId, rString['review'],rating)
                                        x = json
                                        x = {"Data":"Shop rated and reviewed"}
                                        response = JsonResponse(x)
                                        response.status_code = 200
                                    else:
                                        x = json
                                        x = {"Data":"Shop Rated"}
                                        response = JsonResponse(x)
                                        response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Something went wrong"}
                                    response = JsonResponse(x)
                                    response.status_code = 500
                            else:
                                x = json
                                x = {"Data":"Incorrect rating"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect store order"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect orderId"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Incorrect session"}
                    response = JsonResponse(x)
                    response.status_code = 401
        else:
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def cancellingOrder(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        if(uId==0):
            x = json
            x = {"Data":"Incorrect session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==rString['userId']):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if((userType==userType1)&(userType=="CUST")):
                custId = getCustId(uId)
                if(custId==rString['typeId']):
                    masterId = rString['orderId']
                    isValidUserOrder = validateCustomerOrder(custId, masterId)
                    if(isValidUserOrder==1):
                        orderList = rString['orderList']
                        isValidMasterStoreLink = validateMasterStoreLink(orderList,masterId)
                        if isValidMasterStoreLink==1:
                            response = finalUserCancel(orderList)
                        else:
                            x = json
                            x = {"Data":"Incorrect orders passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Incorrect order passed"}
                        response = JsonResponse(x)
                        response.status_code = 409
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            elif((userType==userType1)&(userType=="SHP")):
                shpId = getShpId(uId)
                if(shpId==rString['typeId']):
                    orderId = rString['orderId']
                    isStoreOrder = validateStoreOrderCancel(orderId,shpId)
                    if isStoreOrder==0:
                        x = json
                        x = {"Data":"Order cannot be canceled"}
                        response = JsonResponse(x)
                        response.status_code = 409
                    else:
                        calededStoreOrder = cancelOrderForStore(orderId,isStoreOrder)
                        if calededStoreOrder==1:
                            x = json
                            x = {"Data":"Order canceled"}
                            response = JsonResponse(x)
                            response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Issue while cancelling order"}
                            response = JsonResponse(x)
                            response.status_code = 500
                else:
                    x = json
                    x = {"Data":"not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Not authorized"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Not authorized"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response
