#space for import
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.db import connection
from django.http import JsonResponse
import logging
from haversine import haversine
from datetime import datetime,timedelta
import random
import string
from login.layer3 import sendNotification

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/orders.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def updateShopCount(shopId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT totalOrders from login_shopmaster where shopId=%s",[shopId])
        row = cursor.fetchone();
        totalOrders = row[0]
        totalOrders = totalOrders + 1
        with connection.cursor() as cursorL:
            cursorL.execute("UPDATE login_shopmaster set totalOrders=%s where shopId=%s",[totalOrders,shopId])
            try:
                connection.commit()
            except:
                connection.rollback()

def isDelPossible(addId, shopId):
    delRadius = getDelRadius(shopId)
    userLatLng = getUserLatLng(addId)
    shopLatLng = getShopLatLng(shopId)
    if((userLatLng or shopLatLng or delR) != 0):
        if(haversine(userLatLng, shopLatLng)<delRadius):
            return 1
        else:
            return 0
    else:
        return 0

def getItemValidity(subItemId,mItemId,value,shopId):
    validMitem = getValidMItem(mItemId,shopId)
    validSItem = getValidSItem(mItemId,subItemId,value)
    if((validSItem&validMitem)==1):
        return 1
    else:
        return 0

def getValidMItem(mItemId, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_mainitem where isActive=1 AND shopId_id=%s AND mItemId=%s",[shopId, mItemId])
        if(row_count):
            return 1
        else:
            return 0

def getValidSItem(mItemId,subItemId,value):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_subitem where isActive=1 AND subItemId=%s AND mItemId_id=%s AND rate=%s",[subItemId, mItemId, value])
        if(row_count>0):
            return 1
        else:
            return 0

def getDelRadius(shopId):
    with connection.cursor() as cursor:
        rows_count = cursor.execute("SELECT delRadius from login_shopmaster where isActiveFl=1 AND shopId=%s AND isOpen=1",[shopId])
        if rows_count>0:
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getUserLatLng(addId):
    with connection.cursor() as cursor:
        row_count =cursor.execute("SELECT dmLat, dmLng from address_addressmaster where isActiveFl=1 AND addId=%s",[addId])
        if row_count>0:
            row = cursor.fetchone()
            centerPoint = (row[0], row[1])
            return centerPoint
        else:
            centerPoint = (0, 0)
            return centerPoint

def getShopLatLng(shopId):
    with connection.cursor() as cursor:
        rows_count = cursor.execute("SELECT sLat, sLng from login_shopmaster where isActiveFl=1 AND shopId=%s AND isOpen=1",[shopId])
        if rows_count>0:
            row = cursor.fetchone()
            centerPoint = (row[0], row[1])
            return centerPoint
        else:
            centerPoint = (0, 0)
            return centerPoint

def isValidAdd(addId, userId):
    with connection.cursor() as cursor:
        rows_count = cursor.execute("SELECT * from address_addressmaster where isActiveFl=1 AND addId=%s AND uId_id=%s",[addId,userId])
        if(rows_count>0):
            return 1
        else:
            return 0

#def createMasterOrder(self):
    #with connection.cursor() as cursor:
    #    orderDateTime = datetime.now()
    #    etaDelta = timedelta(minutes=40)
    #    orderEta = orderDateTime + etaDelta
    #    try:
    #        cursor.execute("INSERT INTO orders_ordermaster (orderDateTime, amount, paymentMethod, paymentReference, orderEta, custAddId_id, custId_id, totalShopCount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",[orderDateTime, self.amount, self.paymentMethod, self.paymentReference, orderEta, self.custAddId,  self.custId, self.shopCount])
    #        connection.commit()
    #        orderId = getOrderPaymentRef(self.paymentReference)
    #        if orderId==0:
    #            logger.info('SORRY')
    #            return 0
    #        else:
    #            return orderId
    #    except:
    #        connection.rollback()
    #        logger.exception('SORRY1')
    #        return 0

def createStoreOrder(self):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO orders_orderstoremaster (orderStatus, paymentMethod, amount, gstAmount, orderMasterId_id, shopId_id) VALUES (%s, %s, %s, %s, %s, %s)",["NEW", self.paymentMethod, self.amount, self.gstAmount, self.masterId, self.shopId])
            connection.commit()
            storeOrderId = getStoreOrderId(self.masterId, self.shopId)
            if(storeOrderId==0):
                return 0
            else:
                return storeOrderId
        except:
            connection.rollback()
            return 0

def getStoreOrderId(masterId, storeId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderStoreId from orders_orderstoremaster where orderMasterId_id=%s and shopId_id=%s",[masterId, storeId])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getOrderPaymentRef(paymentReference):
    with connection.cursor() as cursor:
        rows_count = cursor.execute("SELECT orderId from orders_ordermaster where paymentReference=%s",[paymentReference])
        if(rows_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def insertOrderItem(self):
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO orders_orderitemlist (quantity, value, mItemId_id, orderStoreId_id, subItemId_id) VALUES (%s, %s, %s, %s, %s)",[self.quantity, self.value, self.mItemId, self.orderId, self.subItemId])
            connection.commit()
            return 1
        except:
            return 0

def getUserOrderList(custId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where custId_id=%s ORDER BY orderId DESC",[custId])
        if(row_count>0):
            row = cursor.fetchone()
            count = 0
            orderInfo = []
            while row is not None:
                if(row[7]!="CALCULATING"):
                    orderInfo.append({
                        "orderId":row[0],
                        "creationDate":row[1],
                        "paymentMethod":row[4],
                        "gstAmount":row[3],
                        "amount":row[2],
                        "shopCount":row[6],
                        "orderStatus":row[7],
                        "shopsLeft":row[8]
                    })
                    count = count+1
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['Data']=orderInfo
            response=JsonResponse(container)
            response.status_code=200
        else:
            x = json
            x = {"totalCount":0,"Data":"No orders available"}
            response = JsonResponse(x)
            response.status_code = 200
        return response

def getShopOrderList(shpId, viewType):
    with connection.cursor() as cursor:
        if viewType=="NEW":
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s AND orderStatus='NEW' ORDER BY orderStoreId DESC",[shpId])
            if(row_count>0):
                row = cursor.fetchone()
                count = 0
                with connection.cursor() as cursorL:
                    cursorL.execute("SELECT custId_id,orderDateTime from orders_ordermaster where orderId=%s",[row[9]])
                    rowL = cursorL.fetchone()
                orderInfo = []
                while row is not None:
                    with connection.cursor() as cursorS:
                        cursorS.execute("SELECT custName from login_customermaster where custId=%s",[rowL[0]])
                        rowM = cursorS.fetchone()
                    orderInfo.append({
                        "orderId":row[0],
                        "orderStatus":row[2],
                        "creationDate":rowL[1],
                        "paymentMethod":row[3],
                        "amount":row[4],
                        "gstAmount":row[6],
                        "custName":rowM[0],
                        "itemCount":row[7],
                        "itemsLeft":row[8]
                    })
                    count = count+1
                    row = cursor.fetchone()
                container = {}
                container['totalCount']=count
                container['Data']=orderInfo
                response=JsonResponse(container)
                response.status_code=200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        elif viewType=="INPROGRESS":
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s AND ((orderStatus='WORKING')or(orderStatus='ACCEPTED')or(orderStatus='ASSIGNED')) ORDER BY orderStoreId DESC",[shpId])
            if(row_count>0):
                row = cursor.fetchone()
                count = 0
                orderInfo = []
                while row is not None:
                    with connection.cursor() as cursorL:
                        cursorL.execute("SELECT custId_id,orderDateTime from orders_ordermaster where orderId=%s",[row[9]])
                        rowL = cursorL.fetchone()
                    with connection.cursor() as cursorS:
                        cursorS.execute("SELECT custName from login_customermaster where custId=%s",[rowL[0]])
                        rowM = cursorS.fetchone()
                    orderInfo.append({
                        "orderId":row[0],
                        "orderStatus":row[2],
                        "creationDate":rowL[1],
                        "paymentMethod":row[3],
                        "amount":row[4],
                        "gstAmount":row[6],
                        "custName":rowM[0],
                        "itemCount":row[7],
                        "itemsLeft":row[8]
                    })
                    count = count+1
                    row = cursor.fetchone()
                container = {}
                container['totalCount']=count
                container['Data']=orderInfo
                response=JsonResponse(container)
                response.status_code=200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        elif viewType=="COMPLETED":
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s AND orderStatus='COMPLETED' ORDER BY orderStoreId DESC",[shpId])
            if(row_count>0):
                row = cursor.fetchone()
                count = 0
                orderInfo = []
                while row is not None:
                    with connection.cursor() as cursorL:
                        cursorL.execute("SELECT custId_id,orderDateTime from orders_ordermaster where orderId=%s",[row[9]])
                        rowL = cursorL.fetchone()
                    with connection.cursor() as cursorS:
                        cursorS.execute("SELECT custName from login_customermaster where custId=%s",[rowL[0]])
                        rowM = cursorS.fetchone()
                    orderInfo.append({
                        "orderId":row[0],
                        "orderStatus":row[2],
                        "creationDate":rowL[1],
                        "paymentMethod":row[3],
                        "amount":row[4],
                        "gstAmount":row[6],
                        "custName":rowM[0],
                        "itemCount":row[7],
                        "itemsLeft":row[8]
                    })
                    count = count+1
                    row = cursor.fetchone()
                container = {}
                container['totalCount']=count
                container['Data']=orderInfo
                response=JsonResponse(container)
                response.status_code=200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        else:
            x = json
            x = {"Data":"Incorrect view type selected"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def getADMOrderList(viewType):
    with connection.cursor() as cursor:
        if viewType=="NEW":
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStatus='NEW' ORDER BY orderStoreId DESC",)
            if(row_count>0):
                row = cursor.fetchone()
                count = 0
                orderInfo = []
                while row is not None:
                    with connection.cursor() as cursorL:
                        cursorL.execute("SELECT custId_id,orderDateTime,orderId from orders_ordermaster where orderId=%s",[row[9]])
                        rowL = cursorL.fetchone()
                    with connection.cursor() as cursorM:
                        cursorM.execute("SELECT shopName from login_shopmaster where shopId=%s",[row[10]])
                        rowS = cursorM.fetchone()
                    with connection.cursor() as cursorS:
                        cursorS.execute("SELECT custName from login_customermaster where custId=%s",[rowL[0]])
                        rowM = cursorS.fetchone()
                    orderInfo.append({
                        "orderId":row[0],
                        "orderStatus":row[2],
                        "creationDate":rowL[1],
                        "paymentMethod":row[3],
                        "amount":row[5],
                        "gstAmount":row[6],
                        "shopName":rowS[0],
                        "custName":rowM[0],
                        "orderMasterId":rowL[2]
                    })
                    count = count+1
                    row = cursor.fetchone()
                container = {}
                container['totalCount']=count
                container['Data']=orderInfo
                response=JsonResponse(container)
                response.status_code=200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        elif viewType=="INPROGRESS":
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where((orderStatus='WORKING')or(orderStatus='ACCEPTED')or(orderStatus='ASSIGNED')) ORDER BY orderStoreId DESC",)
            if(row_count>0):
                row = cursor.fetchone()
                count = 0
                orderInfo = []
                while row is not None:
                    with connection.cursor() as cursorL:
                        cursorL.execute("SELECT custId_id,orderDateTime,orderId from orders_ordermaster where orderId=%s",[row[9]])
                        rowL = cursorL.fetchone()
                    with connection.cursor() as cursorM:
                        cursorM.execute("SELECT shopName from login_shopmaster where shopId=%s",[row[10]])
                        rowS = cursorM.fetchone()
                    with connection.cursor() as cursorS:
                        cursorS.execute("SELECT custName from login_customermaster where custId=%s",[rowL[0]])
                        rowM = cursorS.fetchone()
                    orderInfo.append({
                        "orderId":row[0],
                        "orderStatus":row[2],
                        "creationDate":rowL[1],
                        "paymentMethod":row[3],
                        "amount":row[5],
                        "gstAmount":row[6],
                        "shopName":rowS[0],
                        "custName":rowM[0],
                        "orderMasterId":rowL[2],
                    })
                    count = count+1
                    row = cursor.fetchone()
                container = {}
                container['totalCount']=count
                container['Data']=orderInfo
                response=JsonResponse(container)
                response.status_code=200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        elif viewType=="COMPLETED":
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStatus='COMPLETED' ORDER BY orderStoreId DESC",)
            if(row_count>0):
                row = cursor.fetchone()
                count = 0
                orderInfo = []
                while row is not None:
                    with connection.cursor() as cursorL:
                        cursorL.execute("SELECT custId_id,orderDateTime,orderId from orders_ordermaster where orderId=%s",[row[9]])
                        rowL = cursorL.fetchone()
                    with connection.cursor() as cursorM:
                        cursorM.execute("SELECT shopName from login_shopmaster where shopId=%s",[row[10]])
                        rowS = cursorM.fetchone()
                    with connection.cursor() as cursorS:
                        cursorS.execute("SELECT custName from login_customermaster where custId=%s",[rowL[0]])
                        rowM = cursorS.fetchone()
                    orderInfo.append({
                        "orderId":row[0],
                        "orderStatus":row[2],
                        "creationDate":rowL[1],
                        "paymentMethod":row[3],
                        "amount":row[4],
                        "gstAmount":row[6],
                        "shopName":rowS[0],
                        "custName":rowM[0],
                        "orderMasterId":rowL[2],
                    })
                    count = count+1
                    row = cursor.fetchone()
                container = {}
                container['totalCount']=count
                container['Data']=orderInfo
                response=JsonResponse(container)
                response.status_code=200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        else:
            x = json
            x = {"Data":"Incorrect view type selected"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def getDMOrderList(dmId, viewType):
    with connection.cursor() as cursor:
        if viewType=="NEW":
            row_count = cursor.execute("SELECT orderStoreId_id from orders_orderitemlisthistory where dmId_id=%s AND itemStatus='ASSIGNED' GROUP BY orderStoreId_id;",[dmId])
            if(row_count>0):
                row = cursor.fetchone()
                orderList = []
                while row is not None:
                    with connection.cursor() as cursorO:
                        cursorO.execute("SELECT orderMasterId_id from orders_orderstoremaster  where orderStoreId=%s;",[row[0]])
                        rowC = cursorO.fetchone()
                    with connection.cursor() as cursorC:
                        cursorC.execute("SELECT custId_id from orders_ordermaster where orderId=%s;",[rowC[0]])
                        rowM = cursorC.fetchone()
                    with connection.cursor() as cursorP:
                        cursorP.execute("SELECT custName, phoneNumber from login_customermaster where custId=%s;",[rowM[0]])
                        rowP = cursorP.fetchone()
                    itemList = []
                    with connection.cursor() as cursorI:
                        cursorI.execute("SELECT orderItemId_id, dayCount from orders_orderitemlisthistory where orderStoreId_id=%s and dmId_id=%s and itemStatus='ASSIGNED';",[row[0],dmId])
                        rowI = cursorI.fetchone()
                        while rowI is not None:
                            itemList.append({
                                "orderItemId":rowI[0],
                                "dayCount":rowI[1]
                            })
                            rowI = cursorI.fetchone()
                    orderList.append({
                        'custName':rowP[0],
                        'phoneNumber':rowP[1],
                        'orderStoreId':row[0],
                        'itemList':itemList
                    })
                    row = cursor.fetchone()
                containerFinal = {}
                containerFinal['totalCount']=row_count
                containerFinal['data']=orderList
                response = JsonResponse(containerFinal)
                response.status_code = 200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        elif viewType=="INPROGRESS":
            row_count = cursor.execute("SELECT orderStoreId_id from orders_orderitemlisthistory where dmId_id=%s AND itemStatus='PICKED' GROUP BY orderStoreId_id",[dmId])
            if(row_count>0):
                row = cursor.fetchone()
                orderList = []
                while row is not None:
                    with connection.cursor() as cursorO:
                        cursorO.execute("SELECT orderMasterId_id from orders_orderstoremaster  where orderStoreId=%s;",[row[0]])
                        rowC = cursorO.fetchone()
                    with connection.cursor() as cursorC:
                        cursorC.execute("SELECT custId_id from orders_ordermaster where orderId=%s;",[rowC[0]])
                        rowM = cursorC.fetchone()
                    with connection.cursor() as cursorP:
                        cursorP.execute("SELECT custName, phoneNumber from login_customermaster where custId=%s;",[rowM[0]])
                        rowP = cursorP.fetchone()
                    itemList = []
                    with connection.cursor() as cursorI:
                        cursorI.execute("SELECT orderItemId_id, dayCount from orders_orderitemlisthistory where orderStoreId_id=%s and dmId_id=%s and itemStatus='PICKED';",[row[0],dmId])
                        rowI = cursorI.fetchone()
                        while rowI is not None:
                            itemList.append({
                                "orderItemId":rowI[0],
                                "dayCount":rowI[1]
                            })
                            rowI = cursorI.fetchone()
                    orderList.append({
                        'custName':rowP[0],
                        'phoneNumber':rowP[1],
                        'orderStoreId':row[0],
                        'itemList':itemList
                    })
                    row = cursor.fetchone()
                containerFinal = {}
                containerFinal['totalCount']=row_count
                containerFinal['data']=orderList
                response = JsonResponse(containerFinal)
                response.status_code = 200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        elif viewType=="COMPLETED":
            row_count = cursor.execute("SELECT orderStoreId_id from orders_orderitemlisthistory where dmId_id=%s AND itemStatus='DELIVER' GROUP BY orderStoreId_id",[dmId])
            if(row_count>0):
                row = cursor.fetchone()
                orderList = []
                while row is not None:
                    with connection.cursor() as cursorO:
                        cursorO.execute("SELECT orderMasterId_id from orders_orderstoremaster  where orderStoreId=%s;",[row[0]])
                        rowC = cursorO.fetchone()
                    with connection.cursor() as cursorC:
                        cursorC.execute("SELECT custId_id from orders_ordermaster where orderId=%s;",[rowC[0]])
                        rowM = cursorC.fetchone()
                    with connection.cursor() as cursorP:
                        cursorP.execute("SELECT custName, phoneNumber from login_customermaster where custId=%s;",[rowM[0]])
                        rowP = cursorP.fetchone()
                    itemList = []
                    with connection.cursor() as cursorI:
                        cursorI.execute("SELECT orderItemId_id, dayCount from orders_orderitemlisthistory where orderStoreId_id=%s and dmId_id=%s and itemStatus='DELIVER';",[row[0],dmId])
                        rowI = cursorI.fetchone()
                        while rowI is not None:
                            itemList.append({
                                "orderItemId":rowI[0],
                                "dayCount":rowI[1]
                            })
                            rowI = cursorI.fetchone()
                    orderList.append({
                        'custName':rowP[0],
                        'phoneNumber':rowP[1],
                        'orderStoreId':row[0],
                        'itemList':itemList
                    })
                    row = cursor.fetchone()
                containerFinal = {}
                containerFinal['totalCount']=row_count
                containerFinal['data']=orderList
                response = JsonResponse(containerFinal)
                response.status_code = 200
            else:
                x = json
                x = {"totalCount":0,"Data":"No orders available"}
                response = JsonResponse(x)
                response.status_code = 200
        else:
            x = json
            x = {"Data":"Incorrect view type selected"}
            response = JsonResponse(x)
            response.status_code = 409
        return response

def validateCustomerOrder(custId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where orderId=%s and custId_id=%s",[orderId, custId])
        if(row_count>0):
            return 1
        else:
            return 0

def orderDetailsCust(custId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where orderId=%s and custId_id=%s",[orderId, custId])
        if(row_count>0):
            with connection.cursor() as cursorT:
                row_countT = cursorT.execute("SELECT* from orders_orderstoremaster where orderMasterId_id=%s",[orderId])
                shopInfo = []
                if(row_countT>0):
                    rowT = cursorT.fetchone()
                    while rowT is not None:
                        count = 0
                        orderInfo = []
                        with connection.cursor() as cursorI:
                            row_countI = cursorI.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s",[rowT[0]])
                            if(row_countI>0):
                                rowI = cursorI.fetchone()
                                while rowI is not None:
                                    with connection.cursor() as cursorDD:
                                        row_countDD = cursorDD.execute("SELECT * from orders_orderitemlisthistory where orderItemId_id=%s",[rowI[0]])
                                        itemHistory = []
                                        if(row_countDD>0):
                                            rowDD = cursorDD.fetchone()
                                            while rowDD is not None:
                                                with connection.cursor() as cursord:
                                                    rowd_count = cursord.execute("SELECT dmName, phoneNumber from login_delivermedmaster where dmId=%s",[rowDD[4]])
                                                    if(rowd_count>0):
                                                        rowd = cursord.fetchone()
                                                    else:
                                                        rowd = "NULL"
                                                itemHistory.append({
                                                    "status":rowDD[1],
                                                    "orderEta":rowDD[2],
                                                    "dayCount":rowDD[3],
                                                    "dmName":rowd[0],
                                                    "dmNumber":rowd[1]
                                                })
                                                rowDD = cursorDD.fetchone()
                                    orderInfo.append({
                                        "itemName":rowI[1],
                                        "amount":rowI[3],
                                        "quantity":rowI[2],
                                        "addOnName":rowI[4],
                                        "subsDay":rowI[5],
                                        "subDaysLeft":rowI[6],
                                        "subStartDate":rowI[7],
                                        "itemStatus":rowI[8],
                                        "itemTranHistory":itemHistory,
                                        "orderItemId":rowI[0]
                                    })
                                    count = count + 1
                                    rowI = cursorI.fetchone()
                                with connection.cursor() as cursoro:
                                    cursoro.execute("SELECT shopName, phoneNumber from login_shopmaster where shopId=%s",[rowT[10]])
                                    rowo = cursoro.fetchone()
                                shopInfo.append({
                                    "shopName":rowo[0],
                                    "shopNumber":rowo[1],
                                    "shopAmount":rowT[4],
                                    "shopGst":rowT[6],
                                    "shopItems":orderInfo,
                                    "orderStatus":rowT[2],
                                    "shopId":rowT[10],
                                    "shopOrderId":rowT[0],
                                    "rating":rowT[11]
                                })
                        rowT = cursorT.fetchone()
            container = {}
            row = cursor.fetchone()
            with connection.cursor() as cursorl:
                cursorl.execute("SELECT * from address_addressmaster where addId=%s",[row[9]])
                rowl = cursorl.fetchone()
                container['addLine1']=rowl[2]
                container['addLine2']=rowl[3]
                container['city']=rowl[4]
                container['state']=rowl[5]
                container['country']=rowl[6]
                container['pincode']=rowl[7]
                container['addLat']=rowl[8]
                container['addLng']=rowl[9]
                container['addName']=rowl[0]
            container['orderAmount']=row[2]
            container['paymentMethod']=row[4]
            container['creationDate']=row[1]
            container['totalShopCount']=row[6]
            container['shopsLeft']=row[8]
            container['orderId']=row[0]
            container['shopInfo']=shopInfo
            container['orderStatus']=row[7]
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"No details","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def validateShopOrder(typeId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s and orderStoreId=%s",[typeId,orderId])
        if(row_count>0):
            return 1
        else:
            return 0

def insertingHistoryEvent(orderId,orderItemId,dmId,dayCount):
    with connection.cursor() as cursor:
        orderDateTime = datetime.now()
        etaDelta = timedelta(minutes=30)
        orderEta = orderDateTime + etaDelta
        cursor.execute("INSERT INTO orders_orderitemlisthistory (itemStatus, orderEta, dayCount, dmId_id, orderStoreId_id, orderItemId_id) values (%s, %s, %s, %s, %s, %s)",["ASSIGNED",orderEta, dayCount, dmId, orderId, orderItemId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def getOrderStatusItem(orderItemId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT itemStatus from orders_orderitemlist where orderItemId=%s",[orderItemId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getOrderDaysLeftItem(orderItemId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT subsDaysLeft from orders_orderitemlist where orderItemId=%s",[orderItemId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def updatingItemValuesOrder(orderItemId,subsDaysLeft,status):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderitemlist set itemStatus=%s, subsDaysLeft=%s where orderItemId=%s",[status,subsDaysLeft,orderItemId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updatingOrderItemList(orderItemId):
    orderStatus1 = getOrderStatusItem(orderItemId)
    subDaysLeft = getOrderDaysLeftItem(orderItemId)
    if(orderStatus1=="NEW"):
        if(subDaysLeft is None):
            updateItemList = updatingItemValuesOrder(orderItemId,None,"WORKING")
            if(updateItemList==1):
                return 1
            else:
                return 0
        elif(subDaysLeft>0):
            updateItemList = updatingItemValuesOrder(orderItemId,subDaysLeft-1,"WORKING")
            if(updateItemList==1):
                return 1
            else:
                return 0
        else:
            return 0
    else:
        if(subDaysLeft is None):
            return 0
        elif(subDaysLeft>0):
            updateItemList = updatingItemValuesOrder(orderItemId,subDaysLeft-1,"WORKING")
            if(updateItemList==1):
                return 1
            else:
                return 0
        else:
            return 0

def validateShopOrderItem(typeId, orderId, orderItemId):
    validShop = validateShopOrder(typeId,orderId)
    if(validShop==1):
        with connection.cursor() as cursor:
            row_count = cursor.execute("SELECT subsDays,subsDaysLeft,itemStatus from orders_orderitemlist where orderItemId=%s and orderStoreId_id=%s",[orderItemId,orderId])
            if(row_count==1):
                row = cursor.fetchone()
                if row[0] is None:
                    if row[2]=="NEW":
                        return 1
                    else:
                        return 0
                elif((row[0]-row[1])+1)<=row[0]:
                    return ((row[0]-row[1])+1)
                else:
                    return 0
            else:
                return 0
    else:
        return 0

def getShopOrderDetails(typeId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s and shopId_id=%s",[orderId, typeId])
        if(row_count>0):
            count = 0
            orderInfo = []
            with connection.cursor() as cursorI:
                row_countI = cursorI.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s",[orderId])
                if(row_countI>0):
                    rowI = cursorI.fetchone()
                    while rowI is not None:
                        itemList = []
                        with connection.cursor() as cursorDD:
                            row_countDD = cursorDD.execute("SELECT * from orders_orderitemlisthistory where orderItemId_id=%s",[rowI[0]])
                            itemHistory = []
                            if(row_countDD>0):
                                rowDD = cursorDD.fetchone()
                                while rowDD is not None:
                                    with connection.cursor() as cursord:
                                        rowd_count = cursord.execute("SELECT dmName, phoneNumber from login_delivermedmaster where dmId=%s",[rowDD[4]])
                                        if(rowd_count>0):
                                            rowd = cursord.fetchone()
                                        else:
                                            rowd = "NULL"
                                    itemHistory.append({
                                        "status":rowDD[1],
                                        "orderEta":rowDD[2],
                                        "dayCount":rowDD[3],
                                        "dmName":rowd[0],
                                        "dmNumber":rowd[1]
                                    })
                                    rowDD = cursorDD.fetchone()
                        orderInfo.append({
                            "itemName":rowI[1],
                            "amount":rowI[3],
                            "quantity":rowI[2],
                            "addOnName":rowI[4],
                            "subsDay":rowI[5],
                            "subDaysLeft":rowI[6],
                            "subStartDate":rowI[7],
                            "itemStatus":rowI[8],
                            "itemTranHistory":itemHistory
                        })
                        count = count + 1
                        rowI = cursorI.fetchone()
            row = cursor.fetchone()
            container = {}
            with connection.cursor() as cursorT:
                cursorT.execute("SELECT custId_id,orderDateTime,custAddId_id from orders_ordermaster where orderId=%s",[row[9]])
                rowT = cursorT.fetchone()
            with connection.cursor() as cursorj:
                cursorj.execute("SELECT custName from login_customermaster where custId=%s",[rowT[0]])
                rowj = cursorj.fetchone()
                container['custName']=rowj[0]
            with connection.cursor() as cursorl:
                cursorl.execute("SELECT * from address_addressmaster where addId=%s",[rowT[2]])
                rowl = cursorl.fetchone()
                container['addLine1']=rowl[2]
                container['addLine2']=rowl[3]
                container['city']=rowl[4]
                container['state']=rowl[5]
                container['country']=rowl[6]
                container['pincode']=rowl[7]
                container['addLat']=rowl[8]
                container['addLng']=rowl[9]
                container['addName']=rowl[0]
            container['orderStatus']=row[2]
            container['items']=orderInfo
            container['itemCount']=count
            container['paymentMethod']=row[3]
            container['amount']=row[4]
            container['gstAmount']=row[6]
            container['creationDate']=row[1]
            container['orderId']=row[0]
            container['orderMasterId']=row[9]
            response=JsonResponse(container)
            response.status_code=200
        else:
            x = json
            x = {"Data":"No details","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def validareAdmOrder(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s",[orderId])
        if(row_count>0):
            return 1
        else:
            return 0

def getADMOrderDetails(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s",[orderId])
        if(row_count>0):
            count = 0
            orderInfo = []
            with connection.cursor() as cursorI:
                row_countI = cursorI.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s",[orderId])
                if(row_countI>0):
                    rowI = cursorI.fetchone()
                    while rowI is not None:
                        with connection.cursor() as cursorDD:
                            row_countDD = cursorDD.execute("SELECT * from orders_orderitemlisthistory where orderItemId_id=%s",[rowI[0]])
                            itemHistory = []
                            if(row_countDD>0):
                                rowDD = cursorDD.fetchone()
                                while rowDD is not None:
                                    with connection.cursor() as cursord:
                                        rowd_count = cursord.execute("SELECT dmName, phoneNumber from login_delivermedmaster where dmId=%s",[rowDD[4]])
                                        if(rowd_count>0):
                                            rowd = cursord.fetchone()
                                        else:
                                            rowd = "NULL"
                                    itemHistory.append({
                                        "status":rowDD[1],
                                        "orderEta":rowDD[2],
                                        "dayCount":rowDD[3],
                                        "dmName":rowd[0],
                                        "dmNumber":rowd[1]
                                    })
                                    rowDD = cursorDD.fetchone()
                        orderInfo.append({
                            "itemName":rowI[1],
                            "amount":rowI[3],
                            "quantity":rowI[2],
                            "addOnName":rowI[4],
                            "subsDay":rowI[5],
                            "subDaysLeft":rowI[6],
                            "subStartDate":rowI[7],
                            "itemStatus":rowI[8],
                            "itemTranHistory":itemHistory
                        })
                        count = count + 1
                        rowI = cursorI.fetchone()
            row = cursor.fetchone()
            container = {}
            with connection.cursor() as cursorT:
                cursorT.execute("SELECT custId_id,orderDateTime,custAddId_id from orders_ordermaster where orderId=%s",[row[9]])
                rowT = cursorT.fetchone()
            with connection.cursor() as cursorj:
                cursorj.execute("SELECT custName, phoneNumber from login_customermaster where custId=%s",[rowT[0]])
                rowj = cursorj.fetchone()
                container['custName']=rowj[0]
                container['custPhoneNumber']=rowj[1]
            with connection.cursor() as cursoro:
                cursoro.execute("SELECT shopName, phoneNumber from login_shopmaster where shopId=%s",[row[10]])
                rowo = cursoro.fetchone()
                container['shpName']=rowo[0]
                container['shpPhoneNumber']=rowo[1]
            with connection.cursor() as cursorl:
                cursorl.execute("SELECT * from address_addressmaster where addId=%s",[rowT[2]])
                rowl = cursorl.fetchone()
                container['addLine1']=rowl[2]
                container['addLine2']=rowl[3]
                container['city']=rowl[4]
                container['state']=rowl[5]
                container['country']=rowl[6]
                container['pincode']=rowl[7]
                container['addLat']=rowl[8]
                container['addLng']=rowl[9]
                container['addName']=rowl[0]
            container['orderStatus']=row[2]
            container['items']=orderInfo
            container['itemCount']=count
            container['paymentMethod']=row[3]
            container['amount']=row[4]
            container['creationDate']=row[1]
            container['orderId']=row[0]
            response=JsonResponse(container)
            response.status_code=200
        else:
            x = json
            x = {"Data":"No details","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def validateDMOrder(typeId, orderStoreId, dayCount, orderItemId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderitemlisthistory where orderStoreId_id=%s and dmId_id=%s and orderItemId_id=%s and dayCount=%s",[orderStoreId,typeId,orderItemId,dayCount])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def validateDMOrderList(typeId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderitemlisthistory where dmId_id=%s and orderStoreId_id=%s",[typeId,orderId])
        if(row_count>0):
            return 1
        else:
            return 0

def getDMOrderDetails(typeId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderItemId_id FROM orders_orderitemlisthistory where dmId_id=%s and orderStoreId_id=%s group by orderItemId_id;",[typeId,orderId])
        if(row_count>0):
            itemInfo = []
            row = cursor.fetchone()
            while row is not None:
                itemHistory = []
                with connection.cursor() as cursorM:
                    row_countM = cursorM.execute("SELECT * from orders_orderitemlisthistory where orderItemId_id=%s and dmId_id=%s;",[row[0],typeId])
                    if(row_countM>0):
                        rowDD = cursorM.fetchone()
                        while rowDD is not None:
                            itemHistory.append({
                                "status":rowDD[1],
                                "orderEta":rowDD[2],
                                "dayCount":rowDD[3],
                            })
                            rowDD = cursorM.fetchone()
                with connection.cursor() as cursorY:
                    cursorY.execute("SELECT * FROM orders_orderitemlist where orderItemId=%s",[row[0]])
                    rowY = cursorY.fetchone()
                itemInfo.append({
                    "itemName":rowY[1],
                    "units":rowY[2],
                    "value":rowY[3],
                    "addOnName":rowY[4],
                    "itemId":rowY[0],
                    "itemTranHistory":itemHistory
                })
                row = cursor.fetchone()
            with connection.cursor() as cursorO:
                cursorO.execute("SELECT orderMasterId_id from orders_orderstoremaster  where orderStoreId=%s;",[orderId])
                rowC = cursorO.fetchone()
            with connection.cursor() as cursorC:
                cursorC.execute("SELECT custId_id,custAddId_id from orders_ordermaster where orderId=%s;",[rowC[0]])
                rowM = cursorC.fetchone()
            with connection.cursor() as cursorP:
                cursorP.execute("SELECT custName, phoneNumber from login_customermaster where custId=%s;",[rowM[0]])
                rowP = cursorP.fetchone()
            with connection.cursor() as cursorl:
                cursorl.execute("SELECT * from address_addressmaster where addId=%s",[rowM[1]])
                rowl = cursorl.fetchone()
            container = {}
            container['addLine1']=rowl[2]
            container['addLine2']=rowl[3]
            container['city']=rowl[4]
            container['state']=rowl[5]
            container['country']=rowl[6]
            container['pincode']=rowl[7]
            container['addLat']=rowl[8]
            container['addLng']=rowl[9]
            container['addName']=rowl[0]
            container['custName']=rowP[0]
            container['custNumber']=rowP[1]
            container['orderId']=orderId
            container['itemList']= itemInfo
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Nothing to display"}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def orderStatus(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderStatus from orders_orderstoremaster where orderStoreId=%s",[orderId])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getMasterOrderId(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderMasterId_id from orders_orderstoremaster where orderStoreId=%s",[orderId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getMasterOrderStatus(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderStatus from orders_ordermaster where orderId=%s",[orderId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def updateMasterOrder(orderId, orderStatus):
    orderMasterId = getMasterOrderId(orderId)
    orderMStatus = getMasterOrderStatus(orderMasterId)
    if orderMStatus=="NEW" and orderStatus=="ACCEPTED":
        with connection.cursor() as cursor:
            cursor.execute("UPDATE orders_ordermaster set orderStatus=%s where orderId=%s",[orderStatus,orderMasterId])
            try:
                connection.commit()
                return 1
            except:
                connection.rollback()
                return 0


def changeOrderStatus(orderId,updateStatus):
    with connection.cursor() as cursor:
        try:
            cursor.execute("UPDATE orders_orderstoremaster SET orderStatus=%s where orderStoreId=%s",[updateStatus,orderId])
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def validateShopDm(shopId, dmId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_delivermedmaster where dmId = %s and shopId_id=%s",[dmId, shopId])
        if(row_count>0):
            return 1
        else:
            return 0

def getGstRate(subItemId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT gstId_id from items_subitem where subItemId = %s",[subItemId])
        row = cursor.fetchone()
        cursor.execute("SELECT gstPercentage from items_gstmaster where gstId = %s",[row[0]])
        rowD = cursor.fetchone()
        return rowD[0]

def getMinOrderValue(shopId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT minOrderValue from login_shopmaster where shopId=%s",[shopId])
        row = cursor.fetchone()
        return row[0]

def getDelChargese(shopId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT delCharges from login_shopmaster where shopId=%s",[shopId])
        row = cursor.fetchone()
        return row[0]

def isPayRefNew(key):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where paymentReference=%s",[key])
        return row_count

def createBasicOrder(custId, addId):
    key = id_generator()
    ifValidKey = isPayRefNew(key)
    while ifValidKey != 0 :
        key = id_generator()
        ifValidKey = isPayRefNew(key)
    orderDateTime = datetime.now()
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO orders_ordermaster (orderDateTime, amount, gstAmount, paymentReference, totalShopCount, orderStatus, totalShopCountLeft, custAddId_id, custId_id, paymentMethod) values (%s, 0, 0, %s, 0, %s, 0, %s, %s, %s)",[orderDateTime,key,"CALCULATING",addId,custId,"COD"])
        try:
            connection.commit()
            orderId = getOrderPaymentRef(key)
            return orderId
        except:
            connection.rollback()
            return 0

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def validateSingleShopOrder(orderId,shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s and orderMasterId_id=%s",[shopId,orderId])
        if(row_count==0):
            return 0
        else:
            row = cursor.fetchone()
            return row[0]

def validateIfShopOrder(orderId,shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s and orderStoreId=%s",[shopId,orderId])
        if(row_count==1):
            return 1
        else:
            return 0

def inserBasicStoreOrder(orderId, shopId):
    orderDateTime = datetime.now()
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO orders_orderstoremaster (orderDateTime, orderStatus, amount, gstAmount, itemCount, itemLeft, orderMasterId_id, shopId_id, deliveryIncluded, paymentMethod, rating) values (%s, %s, 0, 0, 0, 0, %s, %s, 0, %s, 0)",[orderDateTime,"CREATING",orderId, shopId,"COD"])
        try:
            connection.commit()
            return validateSingleShopOrder(orderId, shopId)
        except:
            connection.rollback()
            return 0

def validateItem(shopId, itemId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT mItemId_id from items_subitem where subItemId=%s",[itemId])
        if(row_count==1):
            row = cursor.fetchone()
            mItemId = row[0]
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT shopId_id from items_mainitem where mItemId=%s and isActive=1",[mItemId])
                if(row_countM==1):
                    rowM = cursorM.fetchone()
                    if(rowM[0]==shopId):
                        return 1
                    else:
                        return 0
                else:
                    return 0
        else:
            return 0

def isValidQuantId(itemId, quantId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_quantityinfo where quantId=%s and subItemId_id=%s and isActiveFl=1",[quantId, itemId])
        if(row_count==1):
            return 1
        else:
            return 0

def isValidAddOnId(addOnId,itemId):
    strAddOn = "%"+str(addOnId)+"%"
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_subitem where addOnInfo like %s and subItemId=%s",[strAddOn, itemId])
        if(row_count==1):
            return 1
        else:
            return 0

def validateSubAddOn(subAddId,addOnId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from items_addonitems where addOnItemIt=%s and addOnId_id=%s",[subAddId,addOnId])
        if(row_count==1):
            return 1
        else:
            return 0

def validateQuantAddOn(addOnId,c):
    if(c==1):
        return 1
    elif(c>1):
        with connection.cursor() as cursor:
            row_count = cursor.execute("SELECT isMultiSelect from items_addons where addOnId=%s",[addOnId])
            if(row_count==1):
                row = cursor.fetchone()
                isMultiSelect = row[0]
                if(isMultiSelect==1):
                    return 1
                else:
                    return 0
            else:
                return 0
    else:
        return 0

def validateAddOn(itemId, addOnInfo):
    if(addOnInfo):
        addOnUsed = []
        l = 0
        for each in addOnInfo:
            addOnId = each['addOnId']
            if addOnId in addOnUsed:
                l = 0
                break
            else:
                validAddOnId = isValidAddOnId(addOnId,itemId)
                if(validAddOnId==1):
                    x = 1
                    c = 0
                    for x in each['itemId']:
                        c = c + 1
                        isValidSubAddOn = validateSubAddOn(x['subAddId'],addOnId)
                        if(isValidSubAddOn==0):
                            x = 0
                            break
                    if(x==0):
                        l = 0
                        break
                    else:
                        if(c == each['quantity']):
                            isValidQuantAddOn = validateQuantAddOn(addOnId,c)
                            if(isValidQuantAddOn==1):
                                l = 1
                            else:
                                l = 0
                                break
                        else:
                            l = 0
                            break
                else:
                    l = 0
                    break
        if(l==1):
            return 1
        else:
            return 0
    else:
        return 1

def validateSubsInfo(subsInfo, itemId):
    if(is_empty(subsInfo)):
        subId = subsInfo['subId']
        strAddOn = "%"+str(subId)+"%"
        with connection.cursor() as cursor:
            row_count = cursor.execute("SELECT * from items_subitem where subsInfo like %s and subItemId=%s",[strAddOn, itemId])
            if(row_count==1):
                startDate = subsInfo['startDate']
                my_input_time = datetime.strptime(startDate, '%Y-%m-%d %H:%M:%S')
                present = datetime.now()
                if(present<my_input_time):
                    return 1
                else:
                    return 0
            else:
                return 0
    else:
        return 1

def is_empty(any_structure):
    if any_structure:
        return True
    else:
        return False

def validateItemInfo(shopId,itemId,quantId,addOnInfo,subsInfo):
    validItem = validateItem(shopId,itemId)
    if(validItem==1):
        validQuantId = isValidQuantId(itemId, quantId)
        if(validQuantId==1):
            validAddOnInfo = validateAddOn(itemId, addOnInfo)
            if(validAddOnInfo==1):
                validSubsInfo = validateSubsInfo(subsInfo, itemId)
                if(validSubsInfo==1):
                    return 1
                else:
                    return 0
            else:
                return 0
        else:
            return 0
    else:
        return 0

def getAddOnRate(addOnId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT addOnCharge from items_addons where addOnId=%s",[addOnId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getAddOnPrice(addOnInfo):
    if(addOnInfo):
        totalAddOnCost = 0
        for each in addOnInfo:
            addOnId = each['addOnId']
            rate = getAddOnRate(addOnId)
            price = rate * each['quantity']
            totalAddOnCost = totalAddOnCost + price
        return totalAddOnCost
    else:
        return 0

def getItemPrice(quantId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT value from items_quantityinfo where quantId=%s",[quantId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getSubPrice(subsInfo,quantId):
    if(is_empty(subsInfo)):
        subsId = subsInfo['subId']
        with connection.cursor() as cursor:
            row_count = cursor.execute("SELECT subValue,subNoDays from items_subscription where subId=%s",[subsId])
            if(row_count==1):
                row = cursor.fetchone()
                return row
            else:
                return [0,1]
    else:
        return [0,1]

def calculateItemCost(itemId,quantId,addOnInfo,subsInfo,units):
    itemPrice = getItemPrice(quantId)
    addOnPrice = getAddOnPrice(addOnInfo)
    subsPrice = getSubPrice(subsInfo,quantId)
    totalCost = ((itemPrice+ addOnPrice)*subsPrice[1])*units  + subsPrice[0]
    return totalCost

def calculateItemGst(itemId,itemCost):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT gstId_id from items_subitem where subItemId=%s",[itemId])
        if(row_count==1):
            row = cursor.fetchone()
            with connection.cursor() as cursorM:
                row_coutn1 = cursorM.execute("SELECT gstPercentage from items_gstmaster where gstId=%s",[row[0]])
                if(row_coutn1==1):
                    rowM = cursorM.fetchone()
                    itemGst = (itemCost*rowM[0])/(100*1.0)
                    return itemGst
                else:
                    return 0
        else:
            return 0

def calculateAddOnName(addOnInfo):
    addOnName = ""
    for each in addOnInfo:
        for x in each['itemId']:
            with connection.cursor() as cursor:
                addId = int(x['subAddId'])
                row_count = cursor.execute("SELECT addOnItemName from items_addonitems where addOnItemIt=%s",[addId])
                if(row_count==1):
                    row = cursor.fetchone()
                    addOnName = addOnName+","+row[0]
    return addOnName

def calculateDaysRemaining(subsInfo):
    if(is_empty(subsInfo)):
        subsId = subsInfo['subId']
        with connection.cursor() as cursor:
            row_count = cursor.execute("SELECT subNoDays from items_subscription where subId=%s",[subsId])
            if(row_count==1):
                row = cursor.fetchone()
                return row[0]
            else:
                return None
    else:
        return None

def evaluateStartDate(subsInfo):
    if(is_empty(subsInfo)):
        if(subsInfo['startDate']):
            return subsInfo['startDate']
        else:
            return None
    else:
        return None

def evaluateItemName(itemId,quantId,units):
    itemName = ""
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT subItemName from items_subitem where subItemId=%s",[itemId])
        if(row_count==1):
            row = cursor.fetchone()
            itemName = itemName+""+row[0]
        row_count = cursor.execute("SELECT quantity from items_quantityinfo where quantId=%s",[quantId])
        if(row_count==1):
            row = cursor.fetchone()
            itemName = itemName+" @ "+row[0]
    itemName = itemName +" Units: "+str(units)
    return itemName

def evaluateSubsId(subsInfo):
    if(is_empty(subsInfo)):
        if(subsInfo['subId']):
            return subsInfo['subId']
        else:
            return None
    else:
        return None

def insertItemInformation(itemName, units, itemCost, addOnName, subsDaysLeft, subsStartDate, orderStoreId, quantId, itemId, subsId):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO orders_orderitemlist (itemName, quantity, value, addOnName, subsDays, subsDaysLeft, subsSDate, itemStatus, orderStoreId_id, quantId_id, subItemId_id, subsId_id) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",[itemName, units, itemCost, addOnName, subsDaysLeft, subsDaysLeft, subsStartDate, "NEW", orderStoreId, quantId, itemId, subsId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def checkIfDelIncluded(totalShopAmount, shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT minOrderValue, delCharges from login_shopmaster where shopId=%s",[shopId])
        if(row_count==1):
            row = cursor.fetchone()
            if(totalShopAmount==0):
                return 0
            else:
                if(row[0]<=totalShopAmount):
                    return 0
                else:
                    return row[1]
        else:
            return 0

def updatingShopOrInfo(finalTotal, totalShopGst, isDelIncluded, itemCount, orderStoreId):
    with  connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set amount=%s, gstAmount=%s, deliveryIncluded=%s, itemCount=%s, itemLeft=%s where orderStoreId=%s",[finalTotal, totalShopGst, isDelIncluded, itemCount, itemCount, orderStoreId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updateMasterInfo(totalOrderAmount, totalGstAmount, totalShopCount, orderId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_ordermaster set amount=%s, gstAmount=%s, totalShopCount=%s, totalShopCountLeft=%s where orderId=%s",[totalOrderAmount, totalGstAmount, totalShopCount, totalShopCount, orderId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def getShopName(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT shopName from login_shopmaster where shopId=%s",[shopId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return ""

def isOrderReadyForPay(orderId,typeId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where orderId=%s and custId_id=%s and orderStatus=%s",[orderId,typeId,"CALCULATING"])
        if(row_count==1):
            return 1
        else:
            return 0

def updatingUserOrder(orderId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_ordermaster set orderStatus=%s where orderId=%s",["NEW",orderId])
        try:
            connection.commit()
            updateShopOrder = updatingOrderForShop(orderId)
            if(updateShopOrder==1):
                return 1
            else:
                return 0
        except:
            connection.rollback()
            return 0

def updatingOrderForShop(orderId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set orderStatus=%s where orderMasterId_id=%s",["NEW",orderId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def isValidAmount(orderId, amount):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where orderId=%s and amount=%s",[orderId,amount])
        if(row_count==1):
            return 1
        else:
            return 0

def getDmOrderStatus(validOrder,typeId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT itemStatus from orders_orderitemlisthistory where orderItemHistoryId=%s and dmId_id=%s",[validOrder,typeId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def updateItemHistoryStatus(status,validOrder):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderitemlisthistory set itemStatus=%s where orderItemHistoryId=%s",[status,validOrder])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def getOrderDelDate(orderItemId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT subsDays from orders_orderitemlist where orderItemId=%s",[orderItemId])
        if(row_count==1):
            row = cursor.fetchone()
            if(row[0] is None):
                return 1
            else:
                return row[0]
        else:
            return None

def getTotalDelivered(orderItemId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderitemlisthistory where orderItemId_id=%s and itemStatus=%s",[orderItemId,"DELIVER"])
        return row_count

def validateLastItem(orderItemId):
    getTotalDelDays = getOrderDelDate(orderItemId)
    getTotalItemDelivered = getTotalDelivered(orderItemId)
    if(getTotalDelDays==getTotalItemDelivered):
        return 1
    else:
        return 0

def getItemCount(orderStoreId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT itemLeft from orders_orderstoremaster where orderStoreId=%s",[orderStoreId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return None

def deductItemFromStore(orderStoreId):
    itemCount = getItemCount(orderStoreId)
    itemCount = itemCount - 1
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set itemLeft=%s where orderStoreId=%s",[itemCount,orderStoreId])
        try:
            connection.commit()
        except:
            connection.rollback()

def updateOrderItem(orderItemId,status,orderStoreId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderitemlist set itemStatus=%s where orderItemId=%s",[status,orderItemId])
        try:
            connection.commit()
            deductItemFromStore(orderStoreId)
            return 1
        except:
            connection.rollback()
            return 0

def validateLastStoreItem(orderStoreId):
    itemCount = getItemCount(orderStoreId)
    if(itemCount==0):
        return 1
    else:
        return 0

def getMasterOrderID(orderStoreId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT orderMasterId_id from orders_orderstoremaster where orderStoreId=%s",[orderStoreId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def getMasterShopCount(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT totalShopCountLeft from orders_ordermaster where orderId=%s",[orderId])
        if(row_count==1):
            row = cursor.fetchone()
            return row[0]
        else:
            return None

def deductShopFromMaster(orderMasterId):
    shopCount = getMasterShopCount(orderMasterId)
    shopCount = shopCount-1
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_ordermaster set totalShopCountLeft=%s where orderId=%s",[shopCount,orderMasterId])
        try:
            connection.commit()
            return 1
        except:
            return 0

def updatingStoreOrderStatus(orderStoreId,status):
    orderMasterId = getMasterOrderID(orderStoreId)
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set orderStatus=%s where orderStoreId=%s",[status,orderStoreId])
        try:
            connection.commit()
            deductShopFromMaster(orderMasterId)
            return 1
        except:
            connection.rollback()
            return 0

def validateFinalMasterOrder(orderStoreId):
    orderMasterId = getMasterOrderID(orderStoreId)
    shopCount = getMasterShopCount(orderMasterId)
    if(shopCount==0):
        return 1
    else:
        return 0

def updatingMasterOrder(orderStoreId,status):
    orderMasterId = getMasterOrderID(orderStoreId)
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_ordermaster set orderStatus=%s where orderId=%s",[status,orderMasterId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def generateItemList(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s",[orderId])
        if(row_count>0):
            row = cursor.fetchone()
            itemList = []
            count = 0
            while row is not None:
                if((row[6] is None) and (row[8]=="NEW")):
                    itemList.append({
                        "itemId":row[0]
                    })
                    count = count + 1
                elif(row[6]>0):
                    itemList.append({
                        "itemId":row[0]
                    })
                    count = count + 1
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['data']=itemList
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Nothing to display","totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def generateSubShopList(shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s order by orderStoreId desc",[shpId])
        if(row_count>0):
            row = cursor.fetchone()
            count = 0
            data = []
            while row is not None:
                with connection.cursor() as cursorS:
                    row_count1 = cursorS.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s and subsDays!='null' order by orderItemId desc",[row[0]])
                    if(row_count1>0):
                        rowS = cursorS.fetchone()
                        while rowS is not None:
                            count = count + 1
                            data.append({
                                "subscriptIonId":rowS[0],
                                "itemName":rowS[1],
                                "quantity":rowS[2],
                                "cost":rowS[3],
                                "addOnName":rowS[4],
                                "subsDays":rowS[5],
                                "subsDaysLeft":rowS[6],
                                "subsStartDate":rowS[7],
                                "itemStatus":rowS[8]
                            })
                            rowS = cursorS.fetchone()
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['Data']=data
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"totalCount":0,"Data":[]}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def getShopName(shpId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT shopName from login_shopmaster where shopId=%s",[shpId])
        row = cursor.fetchone()
        return row[0]

def generateSubCustList(shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where custId_id=%s order by orderId desc",[shpId])
        if(row_count>0):
            row = cursor.fetchone()
            count = 0
            data = []
            while row is not None:
                with connection.cursor() as cursorT:
                    row_countT = cursorT.execute("SELECT * from orders_orderstoremaster where orderMasterId_id=%s order by orderStoreId desc",[row[0]])
                    if(row_countT>0):
                        rowT = cursorT.fetchone()
                        while rowT is not None:
                            with connection.cursor() as cursorS:
                                row_count1 = cursorS.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s and subsDays!='null' order by orderItemId desc",[rowT[0]])
                                if(row_count1>0):
                                    rowS = cursorS.fetchone()
                                    while rowS is not None:
                                        count = count + 1
                                        data.append({
                                            "subscriptIonId":rowS[0],
                                            "itemName":rowS[1],
                                            "quantity":rowS[2],
                                            "cost":rowS[3],
                                            "addOnName":rowS[4],
                                            "subsDays":rowS[5],
                                            "subsDaysLeft":rowS[6],
                                            "subsStartDate":rowS[7],
                                            "itemStatus":rowS[8],
                                            "shopName":getShopName(rowT[10])
                                        })
                                        rowS = cursorS.fetchone()
                            rowT = cursorT.fetchone()
                row = cursor.fetchone()
            container = {}
            container['totalCount']=count
            container['Data']=data
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"totalCount":0,"Data":[]}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def generateSubAdmList(shpId):
    count = 0
    data = []
    with connection.cursor() as cursorS:
        row_count1 = cursorS.execute("SELECT * from orders_orderitemlist where subsDays!='null' order by orderItemId desc")
        if(row_count1>0):
            rowS = cursorS.fetchone()
            while rowS is not None:
                count = count + 1
                data.append({
                    "subscriptIonId":rowS[0],
                    "itemName":rowS[1],
                    "quantity":rowS[2],
                    "cost":rowS[3],
                    "addOnName":rowS[4],
                    "subsDays":rowS[5],
                    "subsDaysLeft":rowS[6],
                    "subsStartDate":rowS[7],
                    "itemStatus":rowS[8]
                })
                rowS = cursorS.fetchone()
    container = {}
    container['totalCount']=count
    container['Data']=data
    response = JsonResponse(container)
    response.status_code = 200
    return response

def validateShpSub(subsId, shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderStoreId_id from orders_orderitemlist where orderItemId=%s and subsDays!='null'",[subsId])
        if(row_count==1):
            row = cursor.fetchone()
            with connection.cursor() as cursorC:
                cursorC.execute("SELECT shopId_id from orders_orderstoremaster where orderStoreId=%s",[row[0]])
                rowC = cursorC.fetchone()
                if(rowC[0]==shpId):
                    return 1
                else:
                    return 0
        else:
            return 0

def validateCustSub(subsId, shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderStoreId_id from orders_orderitemlist where orderItemId=%s and subsDays!='null'",[subsId])
        if(row_count==1):
            row = cursor.fetchone()
            with connection.cursor() as cursorC:
                cursorC.execute("SELECT orderMasterId_id from orders_orderstoremaster where orderStoreId=%s",[row[0]])
                rowC = cursorC.fetchone()
                with connection.cursor() as cursorM:
                    cursorM.execute("SELECT custId_id from orders_ordermaster where orderId=%s",[rowC[0]])
                    rowM = cursorM.fetchone()
                    if(rowM[0]==shpId):
                        return 1
                    else:
                        return 0
        else:
            return 0

def validateAdmSub(subsId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderStoreId_id from orders_orderitemlist where orderItemId=%s and subsDays!='null'",[subsId])
        if(row_count==1):
            return 1
        else:
            return 0

def generateSubsDetails(subsId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderitemlist where orderItemId=%s and subsDays!='null'",[subsId])
        if(row_count==1):
            container = {}
            count = 0
            row = cursor.fetchone()
            with connection.cursor() as cursorC:
                cursorC.execute("SELECT orderMasterId_id, shopId_id from orders_orderstoremaster where orderStoreId=%s",[row[9]])
                rowC = cursorC.fetchone()
            with connection.cursor() as cursorM:
                cursorM.execute("SELECT custId_id from orders_ordermaster where orderId=%s",[rowC[0]])
                rowM = cursorM.fetchone()
            with connection.cursor() as cursorJ:
                cursorJ.execute("SELECT custName, phoneNumber from login_customermaster where custId=%s",[rowM[0]])
                rowJ = cursorJ.fetchone()
            with connection.cursor() as cursorL:
                cursorL.execute("SELECT shopName, phoneNumber from login_shopmaster where shopId=%s",[rowC[1]])
                rowL = cursorL.fetchone()
            data = []
            with connection.cursor() as cursorI:
                row_countI = cursorI.execute("SELECT * from orders_orderitemlisthistory where orderItemId_id=%s",[subsId])
                if(row_countI>0):
                    rowI = cursorI.fetchone()
                    while rowI is not None:
                        data.append({
                            "dayCount":rowI[3],
                            "dayEta":rowI[2],
                            "dayStatus":rowI[1]
                        })
                        count = count + 1
                        rowI = cursorI.fetchone()
            container['subsId'] = subsId
            container['shopName'] = rowL[0]
            container['shopNumber'] = rowL[1]
            container['userName'] = rowJ[0]
            container['userPhone'] = rowJ[1]
            container['itemName'] = row[1]
            container['value'] = row[3]
            container['itemStatus'] = row[8]
            container['subsDays'] = row[5]
            container['daysLeft'] = row[6]
            container['startDate'] = row[7]
            container['itemTranHistory'] = data
            container['totalCount'] = count
            response = JsonResponse(container)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"Sorry!"}
            response = JsonResponse(x)
            response.status_code = 409
    return response

def sendNotificationOrderCreate(orderId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT shopId_id,amount from orders_orderstoremaster where orderMasterId_id=%s",[orderId])
        row = cursor.fetchone()
        while row is not None:
            shopId = row[0]
            amount = str(row[1])
            shopuserId = getShopUserId(shopId)
            title = "New order placed"
            body = "New order for "+amount+" has been placed"
            with connection.cursor() as cursorM:
                row_count=cursorM.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[shopuserId])
                if(row_count>0):
                    rowM = cursorM.fetchone()
                    while rowM is not None:
                        sendNotification(rowM[0], title, body)
                        rowM = cursorM.fetchone()
            row = cursor.fetchone()
    with connection.cursor() as cursorC:
        cursorC.execute("SELECT amount, totalShopCount from orders_ordermaster where orderId=%s",[orderId])
        rowC = cursorC.fetchone()
        amount = str(rowC[0])
        shpCount = str(rowC[1])
        title = "New order placed"
        body = "New order for "+shpCount+" shops placed for amount "+amount
        sendAdminNotif(body, title)


def sendAdminNotif(body, title):
    with connection.cursor() as cursor:
        cursor.execute("SELECT uId_id from login_adminmaster where isActiveFl=1")
        row = cursor.fetchone()
        while row is not None:
            with connection.cursor() as cursorM:
                row_count=cursorM.execute("SELECT clientToken from login_messagetoke where userId_id=%s and isActive=1",[row[0]])
                if(row_count>0):
                    rowM = cursorM.fetchone()
                    while rowM is not None:
                        sendNotification(rowM[0], title, body)
                        rowM = cursorM.fetchone()
            row = cursor.fetchone()

def getShopUserId(shopId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT uId_id from login_shopmaster where shopId=%s",[shopId])
        row = cursor.fetchone()
        return row[0]

def masterShopOrder(masterId, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s and orderMasterId_id=%s and orderStatus=%s and rating=%s",[orderId, masterId, "COMPLETED", "0.00"])
        if(row_count==1):
            return 1
        else:
            return 0

def rateOrder(rating, orderStoreId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set rating = %s where orderStoreId=%s",[rating, orderStoreId])
        try:
            connection.commit()
            shopId = getStoreOrderShop(orderStoreId)
            rateShop(shopId,rating)
            return 1
        except:
            connection.rollback()
            return 0

def getStoreOrderShop(orderStoreId):
    with connection.cursor() as cursorM:
        cursorM.execute("SELECT shopId_id from orders_orderstoremaster where orderStoreId=%s",[orderStoreId])
        row = cursorM.fetchone()
        return(row[0])

def rateShop(shopId, rating):
    with connection.cursor() as cursor:
        cursor.execute("SELECT rating, totalRater from login_shopmaster where shopId=%s",[shopId])
        row = cursor.fetchone()
        totalRating = row[0]*row[1]
        totalRating = float(totalRating)+rating
        totalRater = row[1]+1
        avgRating = totalRating/totalRater
        rateFinal(avgRating,totalRater,shopId)

def rateFinal(avgRating,totalRater,shopId):
    with connection.cursor() as cursorM:
        cursorM.execute("UPDATE login_shopmaster set rating=%s, totalRater=%s where shopId=%s",[avgRating,totalRater,shopId])
        try:
            connection.commit()
        except:
            connection.rollback()

def markAReview(custId, orderStoreId, review, rating):
    with connection.cursor() as cursor:
        reviewDateTime = datetime.now()
        shopId = getStoreOrderShop(orderStoreId)
        cursor.execute("INSERT INTO login_reviewmaster (date, rating, reviewText, custId_id, shopId_id) values (%s, %s, %s, %s, %s)",[reviewDateTime, rating, review, custId, shopId])
        try:
            connection.commit()
        except:
            connection.rollback()

def validateMasterStoreLink(orderList,masterId):
    with connection.cursor() as cursor:
        l = 0
        for each in orderList:
            row_count = cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s and orderMasterId_id=%s",[each,masterId])
            if row_count==0:
                l = 1
                break
        if l==1:
            return 0
        else:
            return 1

def validateStoreOrderCancel(orderId,shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderMasterId_id,amount from orders_orderstoremaster where orderStoreId=%s and shopId_id=%s and orderStatus=%s",[orderId,shpId,"NEW"])
        if row_count ==1:
            row = cursor.fetchone()
            return row
        else:
            return 0

def validateStoreOrderCancel1(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT orderMasterId_id,amount from orders_orderstoremaster where orderStoreId=%s and orderStatus=%s",[orderId,"NEW"])
        if row_count ==1:
            row = cursor.fetchone()
            return row
        else:
            return 0

def changingDataMaste(row,rowM):
    with connection.cursor() as cursor:
        totalLeftCount = rowM[1]-1
        totalAmountLeft = rowM[0]-row[1]
        cursor.execute("UPDATE orders_ordermaster set amount=%s,totalShopCountLeft=%s where orderId=%s",[totalAmountLeft,totalLeftCount,row[0]])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updatingMasterInfo(row):
    with connection.cursor() as cursor:
        cursor.execute("SELECT amount, totalShopCountLeft from orders_ordermaster where orderId=%s",[row[0]])
        rowM = cursor.fetchone()
        changeDataMaste = changingDataMaste(row,rowM)
        if changeDataMaste==1:
            return 1
        else:
            return 0

def reverseStatusShop(orderId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set orderStatus=%s where orderStoreId=%s",["NEW",orderId])
        try:
            connection.commit()
        except:
            connection.rollback()

def cancelOrderForStore(orderId, row):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set orderStatus=%s where orderStoreId=%s",["CANCELED",orderId])
        try:
            connection.commit()
            updateMasterInfo = updatingMasterInfo(row)
            if updateMasterInfo==1:
                return 1
            else:
                reverseStatusShop(orderId)
                return 0
        except:
            connection.rollback()
            return 0

def finalUserCancel(orderList):
    info = []
    for each in orderList:
        isValidCancel = validateStoreOrderCancel1(each)
        if isValidCancel==0:
            info.append({
                "orderId":each,
                "status":"FAIL"
            })
        else:
            isShopCanceled = cancelOrderForStore(each, isValidCancel)
            if isShopCanceled==1:
                info.append({
                    "orderId":each,
                    "status":"DONE"
                })
            else:
                info.append({
                    "orderId":each,
                    "status":"FAIL"
                })
    x = json
    x = {"Data":info}
    response = JsonResponse(x)
    response.status_code = 200
    return response
