# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from login.models import customerMaster,shopMaster,deliverMedMaster
from address.models import addressMaster
from datetime import datetime,timedelta
from items.models import subItem,mainItem,quantityInfo,subscription
# Create your models here.
class orderMaster(models.Model):
    """docstring for sessionMaster."""
    custId = models.ForeignKey(customerMaster, on_delete=models.PROTECT)
    orderId = models.AutoField(max_length=32, primary_key=True)
    orderDateTime = models.DateTimeField(auto_now_add=True)
    custAddId = models.ForeignKey(addressMaster, on_delete=models.PROTECT,related_name="cust")
    amount = models.FloatField(blank=False)
    gstAmount = models.FloatField(blank=False, default=0)
    paymentMethod = models.CharField(max_length=10, blank=True, unique=False)
    paymentReference = models.CharField(max_length=32, blank=True, unique=True)
    totalShopCount = models.IntegerField(blank=False)
    orderStatus = models.CharField(blank=False, max_length=15, default="CALCULATED")
    totalShopCountLeft = models.IntegerField(blank= False, default=1)
    originAmount = models.FloatField(null=True)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class orderStoreMaster(models.Model):
    orderStoreId = models.AutoField(max_length=32, primary_key=True)
    orderDateTime = models.DateTimeField(auto_now_add=True)
    orderMasterId = models.ForeignKey(orderMaster, on_delete=models.PROTECT)
    orderStatus = models.CharField(blank=False, max_length=10, default="AWAITING")
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    paymentMethod = models.CharField(max_length=10, blank=False, unique=False)
    amount = models.FloatField(blank=False)
    deliveryIncluded = models.BooleanField(default=1)
    gstAmount = models.FloatField(blank=False)
    itemCount = models.IntegerField(blank=False)
    itemLeft = models.IntegerField(blank=False)
    rating = models.DecimalField(max_digits=3, decimal_places=2, blank=True, default=0.00)
    promoId = models.CharField(null=True, max_length=32, unique=False)
    amountOld = models.FloatField(null=True)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class orderItemList(models.Model):
    orderItemId = models.AutoField(max_length=32, primary_key=True, default=1)
    orderStoreId = models.ForeignKey(orderStoreMaster, on_delete=models.PROTECT)
    subItemId = models.ForeignKey(subItem, on_delete=models.PROTECT)
    itemName = models.CharField(max_length=200, blank=False)
    quantity = models.IntegerField(blank=False)
    value = models.IntegerField(blank=False)
    addonName = models.CharField(blank=True, null=True, max_length=300)
    quantId = models.ForeignKey(quantityInfo, on_delete=models.PROTECT)
    subsId = models.ForeignKey(subscription, on_delete=models.PROTECT, blank=True, null=True)
    subsDays = models.IntegerField(blank=True, null=True)
    subsDaysLeft = models.IntegerField(blank=True, null=True)
    subsSDate = models.DateTimeField(blank=True,auto_now_add=False, null=True)
    itemStatus = models.CharField(blank=False, max_length=10, default="NEW")
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class orderItemListHistory(models.Model):
    orderItemHistoryId = models.AutoField(max_length=32, primary_key=True, default=1)
    orderStoreId = models.ForeignKey(orderStoreMaster, on_delete=models.PROTECT)
    orderItemId = models.ForeignKey(orderItemList, on_delete=models.PROTECT, default=1)
    dmId = models.ForeignKey(deliverMedMaster, on_delete=models.PROTECT, blank=True, null=True)
    itemStatus = models.CharField(blank=False, max_length=10, default="NEW")
    orderEta = models.DateTimeField(auto_now_add=False, null=True)
    dayCount = models.IntegerField(blank=False)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg
