# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.shortcuts import render
from django.http import JsonResponse
import logging
from layer1 import createOrderLayer,orderListLayer,orderDetailsLayer,orderStausUpdater,assignOrderDm,payingForOrder,getListForShop,generateSubList,generateSubDetails,registerRatingReview,cancellingOrder

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/orders.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def createorder(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info('Creating order for' + request.body)
                response = createOrderLayer(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def orderlist(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info('Creating order list for' + request.body)
                response = orderListLayer(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def orderdetails(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info('Creating order list for' + request.body)
                response = orderDetailsLayer(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def updateorderstatus(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info('Updating order status for' + request.body)
                response = orderStausUpdater(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def assigndm(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info('Updating order status for' + request.body)
                response = assignOrderDm(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def payfororder(request):
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            #validates if there is a object passed or not, prints the object passed and prints response object, here
            #if request body is present then we further proceed extracting the data and creating an object for
            #validating if the username and password passed is correct or not and responds accordingly
            #else block is for responding back withouth proceeding when there is no data passed
            if (request.body):
                response = payingForOrder(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def vieworderformark(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info(request.body)
                response = getListForShop(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viesublist(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info(request.body)
                response = generateSubList(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viesubdetails(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info(request.body)
                response = generateSubDetails(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def resigsterreview(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info(request.body)
                response = registerRatingReview(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def cancelorder(request):
    #try block begins
    try:
        #Checking if the method is POST or not for proceeding further
        if (request.method=="POST"):
            if (request.body):
                logger.info(request.body)
                response = cancellingOrder(request)
            else :
                x = json
                x = {"Data":"No values passed"}
                response = JsonResponse(x)
                response.status_code = 404
                logger.info(response)
            return response
        #If not POST then wrong method is user responding accordingly
        else :
            response = JsonResponse({"Data":"Wrong method used"})
            response.status_code = 405
            logger.warn('Wrong method')
            return response
    #expecting AttributeError bole to uppar koi method galat hua hai usko check karo aisa hona nai chahiye
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError bole to type mismatch ka case hai koi variable kahin galat use ho raha hai
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
