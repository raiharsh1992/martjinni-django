#space for import
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.http import JsonResponse
import logging
from login.layer2 import validateKey, vlaidatesessiontype, getShpId, evaluatePropertyShop, getAdmId, getCustId
from layer2 import checkIfBillGen,generateBillShop,billListGenerateCust,billListGenerateAdm,billListGenerateShop,finalShopCustomerList,isBillPayable,updatingPayment,generateShopListPromo,generateListPromoAdm,insertShopPromo,doesPromoExist,insertAdminPromo,checkIfUpdatePosShop,updatingSPromo,updatingSPromo,checkIfPromoAppl,applyingPromoCodeMaster,generateApplyResponse,checkIfUpdatePosAdmin,getMaxPromoCount

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/payment.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def generateBillLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        canBillGenerate = evaluatePropertyShop("BILLGENERATE",shpId)
                        if canBillGenerate > 0:
                            custId = rString['custId']
                            isShopGenPos = checkIfBillGen(shpId, custId)
                            if isShopGenPos == 1:
                                billName = rString['billName']
                                resObj = generateBillShop(shpId, billName, custId)
                                x = json
                                x = {"totalOrder":resObj[0],"totalBillValue":resObj[1]}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"totalOrder":0,"totalBillValue":0}
                                response = JsonResponse(x)
                                response.status_code = 200
                        else:
                            x = json
                            x = {"Data":"Not authorized to perform"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def viewBillListLayer(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        canBillGenerate = evaluatePropertyShop("BILLGENERATE",shpId)
                        if canBillGenerate>0:
                            response = billListGenerateShop(shpId)
                        else:
                            x = json
                            x = {"Data":"Not authorized to perform"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif userType=="CUST":
                    custId = getCustId(userId)
                    if custId == rString['typeId']:
                        response = billListGenerateCust(custId)
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif userType=="ADM":
                    admId = getAdmId(userId)
                    if admId == rString['typeId']:
                        response = billListGenerateAdm()
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generatingCustomerList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        canBillGenerate = evaluatePropertyShop("BILLGENERATE",shpId)
                        if canBillGenerate > 0:
                            response = finalShopCustomerList(shpId)
                        else:
                            x = json
                            x = {"Data":"Not authorized to perform"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def payingTheBill(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        canBillGenerate = evaluatePropertyShop("BILLGENERATE",shpId)
                        if canBillGenerate > 0:
                            billId = rString['billId']
                            checkPay = isBillPayable(billId,"SHP",shpId)
                            if(checkPay==1):
                                updatePay = updatingPayment(billId,"SHP")
                                if updatePay == 1 :
                                    x = json
                                    x = {"Data":"Bill payment accepted"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while accepting"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Incorrect bill information passed"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Not authorized to perform"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif userType=="CUST":
                    shpId = getCustId(userId)
                    if shpId == rString['typeId'] :
                        billId = rString['billId']
                        checkPay = isBillPayable(billId,"CUST",shpId)
                        if(checkPay==1):
                            updatePay = updatingPayment(billId,"CUST")
                            if updatePay == 1 :
                                x = json
                                x = {"Data":"Bill payment accepted"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while accepting"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Incorrect bill information passed"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not valid type Id passed"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Incorrect user type selected"}
                response = JsonResponse(x)
                response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def generatePromoList(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        response = generateShopListPromo(shpId)
                    else:
                        x = json
                        x = {"Data":"Not authorized to view"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    admId = getAdmId(userId)
                    if(admId == rString['typeId']):
                        response = generateListPromoAdm()
                    else:
                        x = json
                        x = {"Data":"Not authorized to view"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def insertPromoCode(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        maxPromoCount = int(evaluatePropertyShop("maxPromoCount",shpId))
                        existCountPromo = int(getMaxPromoCount(shpId))
                        if(existCountPromo<maxPromoCount):
                            minValue = rString['minValue']
                            isMulti = rString['isMulti']
                            promoType = rString['promoType']
                            promoAmount = rString['promoAmount']
                            promoCode = rString['promoCode']
                            if((promoType=="P")and(promoAmount<100))or((promoType=="A")and(promoAmount<minValue)):
                                isNewPromoCode = doesPromoExist(promoCode)
                                if isNewPromoCode ==0:
                                    insertedPromoe = insertShopPromo(minValue,isMulti,promoType,promoAmount,promoCode, shpId)
                                    if insertedPromoe ==1:
                                        x = json
                                        x = {"Data":"New promo code created"}
                                        response = JsonResponse(x)
                                        response.status_code = 200
                                    else:
                                        x = json
                                        x = {"Data":"Issue while creating promocede"}
                                        response = JsonResponse(x)
                                        response.status_code = 409
                                else:
                                    x = json
                                    x = {"Data":"Promocode already in use"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Invalid promotype"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Max promo count reached"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not authorized to view"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    admId = getAdmId(userId)
                    if(admId == rString['typeId']):
                        minValue = rString['minValue']
                        isMulti = rString['isMulti']
                        promoType = rString['promoType']
                        promoAmount = rString['promoAmount']
                        promoCode = rString['promoCode']
                        if((promoType=="P")and(promoAmount<100))or((promoType=="A")and(promoAmount<minValue)):
                            isNewPromoCode = doesPromoExist(promoCode)
                            if isNewPromoCode ==0:
                                insertedPromoe = insertAdminPromo(minValue,isMulti,promoType,promoAmount,promoCode, admId)
                                if insertedPromoe ==1:
                                    x = json
                                    x = {"Data":"New promo code created"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while creating promocede"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                            else:
                                x = json
                                x = {"Data":"Promocode already in use"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Invalid promotype selected"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not authorized to view"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def isNewCodePromo(request):
    try:
        rString = json.loads(request.body)
        promoCode = rString['promoCode']
        isNewPromoCode = doesPromoExist(promoCode)
        if isNewPromoCode ==0:
            x = json
            x = {"Data":"Valid Promo-Code"}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":"PromoCode alrady used"}
            response = JsonResponse(x)
            response.status_code = 409
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def updateCodePromo(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="SHP"):
                    shpId = getShpId(userId)
                    if shpId == rString['typeId'] :
                        updateValue = rString['updateValue']
                        promoId = rString['promoId']
                        ifShopPromoUpdate = checkIfUpdatePosShop(promoId,updateValue,shpId)
                        if(ifShopPromoUpdate==1):
                            if updateValue==0:
                                updateShopPromo = updatingSPromo(promoId,updateValue)
                                if(updateShopPromo==1):
                                    x = json
                                    x = {"Data":"Update Success"}
                                    response = JsonResponse(x)
                                    response.status_code = 200
                                else:
                                    x = json
                                    x = {"Data":"Issue while updating"}
                                    response = JsonResponse(x)
                                    response.status_code = 500
                            else:
                                maxPromoCount = int(evaluatePropertyShop("maxPromoCount",shpId))
                                existCountPromo = int(getMaxPromoCount(shpId))
                                if(existCountPromo<maxPromoCount):
                                    updateShopPromo = updatingSPromo(promoId,updateValue)
                                    if(updateShopPromo==1):
                                        x = json
                                        x = {"Data":"Update Success"}
                                        response = JsonResponse(x)
                                        response.status_code = 200
                                    else:
                                        x = json
                                        x = {"Data":"Issue while updating"}
                                        response = JsonResponse(x)
                                        response.status_code = 500
                                else:
                                    x = json
                                    x = {"Data":"Max promo active"}
                                    response = JsonResponse(x)
                                    response.status_code = 409
                        else:
                            x = json
                            x = {"Data":"Cannot update value"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not authorized to view"}
                        response = JsonResponse(x)
                        response.status_code = 401
                elif(userType=="ADM"):
                    admId = getAdmId(userId)
                    if(admId == rString['typeId']):
                        updateValue = rString['updateValue']
                        promoId = rString['promoId']
                        ifShopPromoUpdate = checkIfUpdatePosAdmin(promoId,updateValue)
                        if(ifShopPromoUpdate==1):
                            updateShopPromo = updatingSPromo(promoId,updateValue)
                            if(updateShopPromo==1):
                                x = json
                                x = {"Data":"Update Success"}
                                response = JsonResponse(x)
                                response.status_code = 200
                            else:
                                x = json
                                x = {"Data":"Issue while updating"}
                                response = JsonResponse(x)
                                response.status_code = 500
                        else:
                            x = json
                            x = {"Data":"Cannot update value"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not authorized to view"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
        else:
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response

def applyingPromoCode(request):
    try:
        rString = json.loads(request.body)
        key = request.META['HTTP_BASICAUTHENTICATE']
        uId = validateKey(key)
        userId = rString['userId']
        if(uId==0):
            x = json
            x = {"Data":"Invalid session"}
            response = JsonResponse(x)
            response.status_code = 401
        elif(uId==userId):
            userType1 = vlaidatesessiontype(uId)
            userType = rString['userType']
            if(userType==userType1):
                if(userType=="CUST"):
                    custId = getCustId(userId)
                    if(custId == rString['typeId']):
                        promoCode = rString['promoCode']
                        orderMasterId = rString['orderMasterId']
                        orderStoreId = rString['orderStoreId']
                        isPromoAppl = checkIfPromoAppl(promoCode,orderMasterId,orderStoreId, custId)
                        if isPromoAppl==1:
                            applyPromoCode = applyingPromoCodeMaster(promoCode, custId, orderMasterId, orderStoreId)
                            if(applyPromoCode==1):
                                response = generateApplyResponse(orderMasterId,orderStoreId)
                            else:
                                x = json
                                x = {"Success":"FALSE","Reason":"Not able to apply"}
                                response = JsonResponse(x)
                                response.status_code = 409
                        else:
                            x = json
                            x = {"Success":"FALSE","Reason":"PromoCode not applicable"}
                            response = JsonResponse(x)
                            response.status_code = 409
                    else:
                        x = json
                        x = {"Data":"Not authorized"}
                        response = JsonResponse(x)
                        response.status_code = 401
                else:
                    x = json
                    x = {"Data":"Not authorized"}
                    response = JsonResponse(x)
                    response.status_code = 401
            else:
                x = json
                x = {"Data":"Not authorized"}
                response = JsonResponse(x)
                response.status_code = 401
        else:
            x = json
            x = {"Data":"Not authorized"}
            response = JsonResponse(x)
            response.status_code = 401
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(request)
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(request)
    return response
