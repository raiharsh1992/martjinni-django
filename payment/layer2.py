#space for import
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import sys
from django.shortcuts import render
from django.db import connection
from django.http import JsonResponse
import logging
from haversine import haversine
from datetime import datetime,timedelta
import random
import string
from orders.layer2 import validateCustomerOrder

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/payment.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

def checkIfBillGen(shopId, custId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_paymentoverview where shopId_id=%s and custId_id=%s order by billId desc;",[shopId, custId])
        if(row_count>0):
            row = cursor.fetchone()
            lastOrderId = row[8]
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT * from orders_orderstoremaster where shopId_id=%s and orderStoreId>%s order by orderStoreId desc;",[shopId,lastOrderId])
                if(row_countM>0):
                    rowM = cursorM.fetchone()
                    l = 0
                    while rowM is not None:
                        isUserorder = validateCustomerOrder(custId, rowM[9])
                        if isUserorder == 1:
                            l = 1
                            break
                        rowM = cursorM.fetchone()
                    return l
                else:
                    return 0
        else:
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT * from orders_orderstoremaster where shopId_id=%s order by orderStoreId desc;",[shopId])
                if(row_countM>0):
                    rowM = cursorM.fetchone()
                    l = 0
                    while rowM is not None:
                        isUserorder = validateCustomerOrder(custId, rowM[9])
                        if isUserorder == 1:
                            l = 1
                            break
                        rowM = cursorM.fetchone()
                    return l
                else:
                    return 0

def generateBillShop(shopId, billName, custId):
    billDate = datetime.now()
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_paymentoverview where shopId_id=%s and custId_id=%s order by billId desc;",[shopId, custId])
        if(row_count>0):
            row = cursor.fetchone()
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT * from orders_orderstoremaster where shopId_id=%s and orderStoreId>%s order by orderStoreId desc;",[shopId,row[8]])
                endOrder = 0
                startOrder = 0
                orderList = ""
                count = 0
                totalBill = 0
                rowM = cursorM.fetchone()
                while rowM is not None:
                    isUserorder = validateCustomerOrder(custId, rowM[9])
                    if isUserorder == 1:
                        if rowM[2]!="CREATING":
                            if(endOrder == 0):
                                endOrder = rowM[0]
                                startOrder = rowM[0]
                                orderList = str(rowM[0])
                                count = count + 1
                                totalBill = totalBill + rowM[4]
                            else:
                                startOrder = rowM[0]
                                orderList = orderList+","+str(rowM[0])
                                count = count + 1
                                totalBill = totalBill + rowM[4]
                    rowM = cursorM.fetchone()
                with connection.cursor() as cursorE:
                    if endOrder > 0 :
                        cursorE.execute("INSERT INTO payment_paymentoverview (startOrderId, billName, orderList, totalBill, billDate, isPaid, custId_id, endOrderId_id, shopId_id) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)",[startOrder, billName, orderList, totalBill, billDate, 0, custId, endOrder, shopId])
                        try:
                            connection.commit()
                            return (count, totalBill)
                        except:
                            connection.rollback()
                            return (0,0)
                    else:
                        return (0,0)
        else:
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT * from orders_orderstoremaster where shopId_id=%s order by orderStoreId desc;",[shopId])
                endOrder = 0
                startOrder = 0
                orderList = ""
                count = 0
                totalBill = 0
                rowM = cursorM.fetchone()
                while rowM is not None:
                    isUserorder = validateCustomerOrder(custId, rowM[9])
                    if isUserorder == 1:
                        if rowM[2]!="CREATING":
                            if(endOrder == 0):
                                endOrder = rowM[0]
                                startOrder = rowM[0]
                                orderList = str(rowM[0])
                                count = count + 1
                                totalBill = totalBill + rowM[4]
                            else:
                                startOrder = rowM[0]
                                orderList = orderList+","+str(rowM[0])
                                count = count + 1
                                totalBill = totalBill + rowM[4]
                    rowM = cursorM.fetchone()
                with connection.cursor() as cursorE:
                    if endOrder > 0 :
                        cursorE.execute("INSERT INTO payment_paymentoverview (startOrderId, billName, orderList, totalBill, billDate, isPaid, custId_id, endOrderId_id, shopId_id) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)",[startOrder, billName, orderList, totalBill, billDate, 0, custId, endOrder, shopId])
                        try:
                            connection.commit()
                            return (count, totalBill)
                        except:
                            connection.rollback()
                            return (0,0)
                    else:
                        return (0,0)

def billListGenerateAdm():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_paymentoverview order by billId desc;")
        if row_count>0:
            row = cursor.fetchone()
            myObj = []
            count = 0
            while row is not None:
                myObj.append({
                    "billId":row[1],
                    "billName":row[2],
                    "custName":getCustName(row[7]),
                    "shopName":getShopName(row[9]),
                    "totalBill":row[4],
                    "orderCount":getOrderCount(row[3]),
                    "isPaid" :row[6],
                    "orders":getOrders(row[3])
                })
                count = count + 1
                row = cursor.fetchone()
            x = json
            x = {"Data":myObj, "totalCount":count}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def billListGenerateCust(custId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_paymentoverview where custId_id=%s order by billId desc;",[custId])
        if row_count>0:
            row = cursor.fetchone()
            myObj = []
            count = 0
            while row is not None:
                myObj.append({
                    "billId":row[1],
                    "billName":row[2],
                    "shopName":getShopName(row[9]),
                    "totalBill":row[4],
                    "orderCount":getOrderCount(row[3]),
                    "isPaid" :row[6],
                    "orders":getOrders(row[3])
                })
                count = count + 1
                row = cursor.fetchone()
            x = json
            x = {"Data":myObj, "totalCount":count}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def billListGenerateShop(shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_paymentoverview where shopId_id=%s order by billId desc;",[shpId])
        if row_count>0:
            row = cursor.fetchone()
            myObj = []
            count = 0
            while row is not None:
                myObj.append({
                    "billId":row[1],
                    "billName":row[2],
                    "custName":getCustName(row[7]),
                    "totalBill":row[4],
                    "orderCount":getOrderCount(row[3]),
                    "isPaid" :row[6],
                    "orders":getOrders(row[3])
                })
                count = count + 1
                row = cursor.fetchone()
            x = json
            x = {"Data":myObj, "totalCount":count}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def getShopName(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from login_shopmaster where shopId=%s",[shopId])
        if row_count == 1:
            row = cursor.fetchone()
            return row[1]
        else:
            return ""

def getCustName(custId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT custName from login_customermaster where  custId=%s",[custId])
        if(row_count>0):
            row = cursor.fetchone()
            return row[0]
        else:
            return ""

def getOrderCount(orderList):
    orderList = orderList.split(",")
    orderCount = len(orderList)
    return orderCount

def getOrders(orderList):
    orderList = orderList.split(",")
    orders = []
    with connection.cursor() as cursor:
        for each in orderList:
            cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s",[each])
            row = cursor.fetchone()
            orders.append({
                "orderId":row[0],
                "orderDate":row[1],
                "orderStatus":row[2],
                "totalAmount":row[4],
                "totalItemCount":row[7],
                "items":getItems(each)
            })
    return orders

def getItems(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderitemlist where orderStoreId_id=%s",[orderId])
        items = []
        if(row_count>0):
            row = cursor.fetchone()
            while row is not None:
                items.append({
                    "itemName":row[1]
                })
                row = cursor.fetchone()
            return items
        else:
            return items

def finalShopCustomerList(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_orderstoremaster where shopId_id=%s",[shopId])
        if row_count > 0:
            userList = []
            row = cursor.fetchone()
            while row is not None:
                if row[2]!="CREATING":
                    custId = getOrderCust(row[9])
                    if custId not in userList and custId != 0:
                        userList.append(custId)
                row = cursor.fetchone()
            response = finalCustList(userList)
        else:
            x = json
            x = {"Data":[],"totalCount":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def getOrderCust(orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT custId_id from orders_ordermaster where orderId=%s",[orderId])
        if row_count == 1:
            row = cursor.fetchone()
            return row[0]
        else:
            return 0

def finalCustList(userList):
    with connection.cursor() as cursor:
        custList = []
        count = 0
        for each in userList:
            cursor.execute("SELECT custName from login_customermaster where custId=%s",[each])
            row = cursor.fetchone()
            custList.append({
                "custName":row[0],
                "custId":each
            })
            count = count + 1
        x = json
        x = {"Data":custList,"totalCount":count}
        response = JsonResponse(x)
        response.status_code = 200
    return response

def isBillPayable(billId,uType,shpId):
    with connection.cursor() as cursor:
        if uType == "CUST":
            row_count = cursor.execute("SELECT isPaid from payment_paymentoverview where custId_id=%s and billId=%s",[shpId,billId])
            if(row_count==1):
                row = cursor.fetchone()
                if row[0] == 0:
                    return 1
                else:
                    return 0
            else:
                return 0
        elif uType == "SHP":
            row_count = cursor.execute("SELECT isPaid from payment_paymentoverview where shopId_id=%s and billId=%s",[shpId,billId])
            if(row_count==1):
                row = cursor.fetchone()
                if row[0] == 1:
                    return 1
                else:
                    return 0
            else:
                return 0

def updatingPayment(billId,uType):
    with connection.cursor() as cursor:
        try:
            if uType == "CUST":
                cursor.execute("UPDATE payment_paymentoverview set isPaid=%s where billId=%s",[1,billId])
                connection.commit()
                return 1
            elif uType=="SHP":
                cursor.execute("UPDATE payment_paymentoverview set isPaid=%s where billId=%s",[2,billId])
                connection.commit()
                return 1
            else:
                return 0
        except:
            connection.rollback()
            return 0

def generateShopListPromo(shpId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_promocodemaster where shopId_id=%s",[shpId])
        if(row_count>0):
            row = cursor.fetchone()
            data = []
            count = 0
            while row is not None:
                count = count+1
                data.append({
                    "promoId":row[0],
                    "promoCode":row[1],
                    "createdOn":row[2],
                    "minAmount":row[3],
                    "promoType":row[4],
                    "disAmount":row[5],
                    "isActive":row[6],
                    "isMulti":row[11]
                })
                row = cursor.fetchone()
            x = {}
            x = {"Data":data,"count":count}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"count":0,"Data":[]}
            response = JsonResponse(x)
            response.status_code = 200
        return response

def generateListPromoAdm():
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from payment_promocodemaster where addedBy=%s",["ADM"])
        if(row_count>0):
            row = cursor.fetchone()
            data = []
            count = 0
            while row is not None:
                count = count+1
                data.append({
                    "promoId":row[0],
                    "promoCode":row[1],
                    "createdOn":row[2],
                    "minAmount":row[3],
                    "promoType":row[4],
                    "disAmount":row[5],
                    "isActive":row[6],
                    "isMulti":row[11]
                })
                row = cursor.fetchone()
            x = {}
            x = {"Data":data,"count":count}
            response = JsonResponse(x)
            response.status_code = 200
        else:
            x = json
            x = {"Data":[],"count":0}
            response = JsonResponse(x)
            response.status_code = 200
    return response

def doesPromoExist(promoCode):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * from payment_promocodemaster where promoCode=%s",[promoCode])
        if(row_count==0):
            return 0
        else:
            return 1

def insertShopPromo(minValue,isMulti,promoType,promoAmount,promoCode, shpId):
    with connection.cursor() as cursor:
        creationDate = datetime.now()
        cursor.execute("INSERT INTO payment_promocodemaster (promoCode, createdOn, minAmount, promoType, disAmount, isActive, addedBy, shopId_id, isMulti) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)",[promoCode, creationDate, minValue, promoType, promoAmount, 1, "SHP", shpId, isMulti])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def insertAdminPromo(minValue,isMulti,promoType,promoAmount,promoCode, admId):
    with connection.cursor() as cursor:
        creationDate = datetime.now()
        cursor.execute("INSERT INTO payment_promocodemaster (promoCode, createdOn, minAmount, promoType, disAmount, isActive, addedBy, shopId_id, isMulti) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)",[promoCode, creationDate, minValue, promoType, promoAmount, 1, "ADM", admId, isMulti])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def checkIfUpdatePosShop(promoId,updateValue,shpId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT isActive from payment_promocodemaster where promoId=%s and shopId_id=%s",[promoId, shpId])
        if(row_count==1):
            row = cursor.fetchone()
            isActive = row[0]
            if isActive==updateValue:
                return 0
            elif isActive!=updateValue and (updateValue==1) or (updateValue==0):
                return 1
            else:
                return 0
        else:
            return 0

def checkIfUpdatePosAdmin(promoId,updateValue):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT isActive from payment_promocodemaster where promoId=%s and addedBy=%s",[promoId,"ADM"])
        if(row_count==1):
            row = cursor.fetchone()
            isActive = row[0]
            if isActive==updateValue:
                return 0
            elif isActive!=updateValue and (updateValue==1) or (updateValue==0):
                return 1
            else:
                return 0
        else:
            return 0

def updatingSPromo(promoId,updateValue):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE payment_promocodemaster set isActive=%s where promoId=%s",[updateValue,promoId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def validateClientOrder(orderMasterId, custId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where orderId =%s and custId_id=%s",[orderMasterId,custId])
        if(row_count==1):
            return 1
        else:
            return 0

def checkIfPromoAppl(promoCode,orderMasterId,orderStoreId,custId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * from payment_promocodemaster where promoCode=%s",[promoCode])
        if row_count == 1:
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT shopId_id, promoId, amount from orders_orderstoremaster where orderStatus=%s and orderStoreId=%s and orderMasterId_id=%s and promoId is NULL",["CREATING",orderStoreId,orderMasterId])
                if row_countM ==1:
                    isClientOrder = validateClientOrder(orderMasterId, custId)
                    if isClientOrder==1:
                        rowM = cursorM.fetchone()
                        row = cursor.fetchone()
                        if(row[10]==rowM[0]):
                            print "here"
                            if(float(row[3])<=float(rowM[2])):
                                usedCust = row[8].split(",")
                                isMulti = row[11]
                                if isMulti==1:
                                    return 1
                                else:
                                    if custId in usedCust:
                                        return 0
                                    else:
                                        return 1
                            else:
                                return 0
                        else:
                            return 0
                    else:
                        return 0
                else:
                    return 0
        else:
            return 0

def updatinStoreInfo(deductAmount,oldAmount,newAmount,orderStoreId,promoCode):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set promoId=%s, amountOld=%s, amount=%s where orderStoreId=%s",[promoCode,oldAmount,newAmount,orderStoreId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updateMasterPromo(orderId, oldAmount, newAmount):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_ordermaster set amount=%s,originAmount=%s where orderId=%s",[newAmount,oldAmount,orderId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updateMasterPromoNull(orderId,  newAmount):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_ordermaster set amount=%s,originAmount=NULL where orderId=%s",[newAmount,orderId])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def updatingMasterInfo(deductAmount, orderId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT * from orders_ordermaster where orderId=%s",[orderId])
        if(row_count==1):
            row = cursor.fetchone()
            if row[11] is None:
                oldAmount = row[2]
                newAmount = oldAmount - deductAmount
                uifUpdatedMaster = updateMasterPromo(orderId,oldAmount,newAmount)
                if uifUpdatedMaster==1:
                    return 1
                else:
                    return 0
            else:
                originAmount = row[11]
                oldAmount = row[2]
                newAmount = oldAmount - deductAmount
                uifUpdatedMaster = updateMasterPromo(orderId,originAmount,newAmount)
                if uifUpdatedMaster==1:
                    return 1
                else:
                    return 0
        else:
            return 0

def updatingPromoInfo(promoCode,usedCust):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE payment_promocodemaster set customerList=%s where promoCode=%s",[usedCust,promoCode])
        try:
            connection.commit()
            return 1
        except:
            connection.rollback()
            return 0

def reveseMasterChange(orderMasterId, deductAmount):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from orders_ordermaster where orderId=%s",[orderMasterId])
        row = cursor.fetchone()
        currAmount = row[2]
        originAmount = row[11]
        newCurr = currAmount+deductAmount
        if newCurr==originAmount:
            updateMasterPromoNull(orderMasterId, newCurr)
        else:
            updateMasterPromo(orderMasterId, originAmount, newCurr)

def deletePromoShop(amountOld, orderStoreId):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE orders_orderstoremaster set amount=%s,promoId=NULL,amountOld=NULL where orderStoreId=%s",[amountOld,orderStoreId])
        try:
            connection.commit()
        except:
            connection.rollback()

def reveseStoreChange(orderStoreId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT amountOld from orders_orderstoremaster where orderStoreId=%s",[orderStoreId])
        row = cursor.fetchone()
        deletePromoShop(row[0], orderStoreId)

def applyingPromoCodeMaster(promoCode, custId, orderMasterId, orderStoreId):
    with connection.cursor() as cursor:
        row_count=cursor.execute("SELECT * from orders_orderstoremaster where orderStoreId=%s and orderMasterId_id=%s and promoId is NULL ",[orderStoreId,orderMasterId])
        if(row_count==1):
            with connection.cursor() as cursorM:
                row_countM = cursorM.execute("SELECT * from payment_promocodemaster where promoCode=%s",[promoCode])
                if row_countM == 1:
                    rowM = cursorM.fetchone()
                    row = cursor.fetchone()
                    if(rowM[7] is None):
                        usedCust = str(custId)
                    else:
                        usedCust = str(rowM[7])+","+str(custId)
                    originalAmount = row[4]
                    if(rowM[4]=="A"):
                        newAmount = originalAmount - rowM[5]
                        deductAmount = rowM[5]
                    elif(rowM[4]=="P"):
                        newAmount = originalAmount - ((originalAmount*rowM[5])/100)
                        deductAmount = (originalAmount*rowM[5])/100
                    updateStoreInfo = updatinStoreInfo(deductAmount,originalAmount,newAmount,orderStoreId,promoCode)
                    if updateStoreInfo==1:
                        updateMasterInfo =updatingMasterInfo(deductAmount,orderMasterId)
                        if updateMasterInfo==1:
                            updatePromoInfo = updatingPromoInfo(promoCode,usedCust)
                            if updatePromoInfo == 1:
                                return 1
                            else:
                                reveseMasterChange(orderMasterId, deductAmount)
                                reveseStoreChange(orderStoreId)
                                return 0
                        else:
                            reveseStoreChange(orderStoreId)
                            return 0
                    else:
                        return 0
                else:
                    return 0
        else:
            return 0

def generateApplyResponse(orderMasterId,orderStoreId):
    with connection.cursor() as cursor:
        cursor.execute("SELECT amount, amountOld from orders_orderstoremaster where orderStoreId=%s",[orderStoreId])
        row = cursor.fetchone()
        with connection.cursor() as cursorM:
            cursorM.execute("SELECT amount, originAmount from orders_ordermaster where orderId=%s",[orderMasterId])
            rowM = cursorM.fetchone()
            x = {}
            x = {
                "Success":"TRUE",
                "oldStoreAmount":row[1],
                "newStoreAmount":row[0],
                "originTotal":rowM[1],
                "newTotal":rowM[0],
                "orderMasterId":orderMasterId,
                "orderStoreId":orderStoreId
            }
            response = JsonResponse(x)
            response.status_code = 200
            return response

def getMaxPromoCount(shopId):
    with connection.cursor() as cursor:
        row_count = cursor.execute("SELECT COUNT(*) FROM payment_promocodemaster where shopId_id=%s and isActive=1",[shopId])
        if row_count>0:
            row = cursor.fetchone()
            return row[0]
        else:
            return 0
