# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from login.models import shopMaster,customerMaster,adminMaster
from orders.models import orderStoreMaster
# Create your models here.
class paymentOverview(models.Model):
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT)
    custId = models.ForeignKey(customerMaster, on_delete=models.PROTECT)
    startOrderId = models.IntegerField(blank=False, unique=True)
    endOrderId = models.ForeignKey(orderStoreMaster, on_delete=models.PROTECT)
    billId = models.AutoField(max_length=32, primary_key=True)
    billName = models.CharField(max_length=32, blank=True, unique=False)
    orderList = models.CharField(max_length=100,unique=False)
    totalBill =  models.DecimalField(max_digits=13, decimal_places=2, blank=False)
    billDate = models.DateField(auto_now_add=False)
    isPaid = models.BooleanField(default="1")
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg

class promocodeMaster(models.Model):
    promoId = models.AutoField(primary_key=True)
    shopId = models.ForeignKey(shopMaster, on_delete=models.PROTECT, null=True)
    promoCode = models.CharField(max_length=32, unique=True, blank=False)
    createdOn = models.DateField(auto_now_add=False)
    minAmount = models.IntegerField(blank=False, unique=False)
    promoType = models.CharField(max_length=1, unique=False, blank=False)
    disAmount = models.IntegerField(blank=False, unique=False)
    isActive = models.BooleanField()
    customerList = models.CharField(null=True, unique=False, max_length=500)
    adminId = models.ForeignKey(adminMaster, on_delete=models.PROTECT, null=True)
    addedBy = models.CharField(max_length=5, blank=False, unique=False)
    isMulti = models.BooleanField(default=0)
    def __init__(self, arg):
        super(sessionMaster, self).__init__()
        self.arg = arg
